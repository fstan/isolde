with(CodeTools):
with(ISOLDE):


###
Test(x,x);


##Test the output of formal reduce
##expected_res is the list of all polynomials from the result 
#
test_formal_reduce := proc(M, x, lambda, phi, delta, ct, expected_res)
local res, i;

res:= formal_reduce(M, x, lambda, phi, delta, ct);
print(`res= `, res );

if type(res, list(list)) then

Test(nops(expected_res)-nops(res), 0);
for i to nops(res) do
print(i);
print(res[i,1], expected_res);
Test(evalb(res[i,1] in expected_res), true)
end do;

else Test(res[1]-expected_res, 0)
end if;

end proc;


###
###
A := linalg[matrix](4, 4, [1, 1/x, x, 1/x^2, x, x+1, 1/x^5, 0, x, 1/x, 1/x^2, 0, x, 0, 0, 0]);
phi := proc (f, x) f end proc;
delta := proc (f, x) diff(f, x) end proc;

Test(GlobalSuperReduce(A, x, x, lambda, phi, delta, -1)[3], [[0, -lambda - lambda^2 ], [1, -1], [2, lambda^2  - 1]]);

M := mat_convert(A, x, 0, 'differential');
Test(l_super_reduce(M, x, lambda, x, delta, -1, {step = 1})[2], [[0, (1 + lambda)^2*lambda^2]]);
test_formal_reduce(M, x, lambda, phi, delta, -1, [-1/x^3+1/2/x^2-1/8/x, 0, 1/x^3+1/2/x^2+1/8/x]);

##result ISOLDE in Step 5 -> [-1/x^3+1/(2*x^2)-1/(8*x), 1/x^3+1/(2*x^2)+1/(8*x), -1/x]

N := mat_convert(evalm(A/x^10), x, 0, 'differential');
test_formal_reduce(N, x, lambda, phi, delta, -1, {1/x^13+1/(2*x^12)+1/(8*x^11)+1/x^10+95/(128*x^9)-5/(16*x^8)-31/(1024*x^7)+43/(256*x^6)+8379/(32768*x^5)+655/(2048*x^4)-57657/(262144*x^3)+2099/(65536*x^2)-1891253/(4194304*x),1/x^21+1/(2*x^20)+1/(8*x^19)-1/(128*x^17)+1/(1024*x^15)-5/(32768*x^13)+7/(262144*x^11)-21/(4194304*x^9)+33/(33554432*x^7)-429/(2147483648*x^5)+715/(17179869184*x^3),-1/x^13+1/(2*x^12)-1/(8*x^11)+1/x^10+161/(128*x^9)+3/(16*x^8)-97/(1024*x^7)+27/(256*x^6)+8773/(32768*x^5)+679/(2048*x^4)+7225/(262144*x^3)-6253/(65536*x^2)+1355061/(4194304*x)});

###
###
B := linalg[matrix](4, 4, [1, 1/x, x, (1+x)/x^2, x, x+1, (1+x)/x^5, 0, x, 1/(x+1), 1/x^2, 0, x, 0, 0, 0]);
phi := proc (f, x) f end proc;
delta := proc (f, x) diff(f, x) end proc;

Test(GlobalSuperReduce(B, x, x, lambda, phi, delta, -1)[3], [[0, -lambda-lambda^2], [1, -1], [2, 1]]);

P := mat_convert(B, x, 0, 'differential');

Test(l_super_reduce(P, x, lambda, x, delta, -1, {step = 1})[2], [[0, (1+lambda)^2*lambda^2]]);

test_formal_reduce(P, x, lambda, phi, delta, -1, [1/x^5+1/2/x^4-7/8/x^3-4/x^2, 0]);

N := mat_convert(evalm(B/x^10), x, 0, 'differential');
test_formal_reduce(N, x, lambda, phi, delta, -1,
[1/x^25+1/2/x^24-7/8/x^23-1/x^22+79/128/x^21+9/8/x^20-1495/1024/x^19+
221/128/x^18+54299/32768/x^17+1125/1024/x^16+280431/262144/x^15+44317/32768/x^
14+11976347/4194304/x^13+1685247/262144/x^12+57332905/33554432/x^11+42622777/
4194304/x^10+14244367507/2147483648/x^9+241573453/33554432/x^8+50127018675/
17179869184/x^7+30391843677/2147483648/x^6-4483773162767/274877906944/x^5+
547207066875/17179869184/x^4-59225898572353/2199023255552/x^3+4376121015987/
274877906944/x^2, 1/x^21+1/x^19-1/x^15+2/x^13+1/x^11+1/x^9+10/x^7+16/x^
5+20/x^3-54/x^2-37/x^4-14/x^6-3/x^8-5/x^10-4/x^12-2/x^18]);

###
###
C := linalg[matrix](4, 4, [1, (1-x)/x, x, (1+x)/x^3, x, x+5, (5+x)^2/x^5, 0, x, 1/(x-2)^3, 1/(x^2-1), 0, x, 0, 0, 0]);
phi := proc (f, x) f end proc;
delta := proc (f, x) diff(f, x) end proc;

Test(GlobalSuperReduce(C, x, x, lambda, phi, delta, -1)[3], [[0, lambda^2+2*lambda-1], [1, 1], [2, 1]]);

P := mat_convert(C, x, 0, 'differential');

Test(l_super_reduce(P, x, lambda, x, delta, -1, {step = 1})[2], [[0, (1+lambda)^2*lambda^2]]);

test_formal_reduce(P, x, lambda, phi, delta, -1, [64/625/x^5-112/625/x^3+4/5/x^2, 0]);

N := mat_convert(evalm(B/x^9), x, 0, 'differential');
test_formal_reduce(N, x, lambda, phi, delta, -1, [1/x^23+1/2/x^22-7/8/x^21-1/x^20+79/128/x^19+9/8/x^18-1495/1024/x^17+
221/128/x^16+54299/32768/x^15+1125/1024/x^14+280431/262144/x^13+44317/32768/x^
12+11976347/4194304/x^11+1685247/262144/x^10+57332905/33554432/x^9+42622777/
4194304/x^8+14244367507/2147483648/x^7+241573453/33554432/x^6+50127018675/
17179869184/x^5+30391843677/2147483648/x^4-4483773162767/274877906944/x^3+
495667459323/17179869184/x^2, 1/x^19+1/x^17-1/x^13+2/x^11+1/x^9+1/x^7+
10/x^5+16/x^3-37/x^2-14/x^4-3/x^6-5/x^8-4/x^10-2/x^16]);