# ************************************************************************* #
#                                                                           #
#             Primitives for computing with matrix power series             #
#                                                                           #
#                              Differential case                            #
#                                                                           #
#                                                                           #
#                            Last update:  $Date$ by $Author$          	    #
#                                                                           #
# ************************************************************************* #

### NEW changes from 04/01/2012

##NOTE: that mat_eval is called with 2 sets of parameters
## (K, x, i, j) or (K, x, i, j, phi1, delta1). In the last case the 2
# additional parametres are never used since omega is missing! (See
# procedure code for mat_eval)

##NOTE: in mat_pseudo_transform we use phi as expresion and not as procedure



# new_system()
#
# This function returns a new key which is not a member of the global
# variable Sys_Keys
#
new_system := proc()
    local N;
    global Sys_Keys;

    if type(Sys_Keys, 'name') then Sys_Keys := {} fi;
    N := L||(nops(Sys_Keys)+1);
    Sys_Keys := Sys_Keys union {N};
    N
end:

# new_matrix()
#
# This function returns a new key which is not a member of the global
# variable Mat_Keys
#
new_matrix := proc()
    local N;
    global Mat_Keys;

    if type(Mat_Keys, 'name') then Mat_Keys := {} fi;
    N := A||(nops(Mat_Keys)+1);
    Mat_Keys := Mat_Keys union {N};
	userinfo(5, 'MatTools', `in new matrix N=`, N);
    N
end:

# mat_subs(s, K, x)
#  INPUT:  s - an equation
#          K - a key of a matrix series M in internal representation
#          x - name
#  OUTPUT: do the substitution s in M
#
mat_subs := proc(s, K, x, phi, delta, omega)
    local N, KK;
    global Mat_Description;

    N := array(1..10);
    N[1] := 'substituted';
    N[2] := mat_valuation(K);
    N[3] := map(collect, subs(s, mat_get_terms(K, x)), x, evala);
    N[4] := mat_get_order(K, x);
    N[5] := s;
    N[6] := mat_get_ext(K) union indets(s, 'RootOf');
    N[7] := K;
	N[8] := phi;
	N[9] := delta;
	N[10] := omega;
    KK := new_matrix();
    Mat_Description[KK] := N;
    KK
end:

# mat_to_simple(K, x, invars, phi, delta, omega)
#   INPUT:  K      - a key of the representation of a matrix series M
#           x      - a name
#           invars - a list of the ni-invars of a k-simple system
#           phi    - authomorphism as procedure
#           delta  - a pseudo-derivative
#           omega  - the degree of delta
#
#   OUTPUT: this procedure supposes that the pseudo-linear system
#           delta(Y)= M phi(Y)
#           can be written as D x^(k+1) delta(Y) = N phi(Y), where one has
#           P(lambda)=det(D_0 -lambda N_0) is a polynomial in lambda not identically
#           zero. The output is then an array DD with 9 elements
#           representing the simple operators. The entries of the array DD
#           are:
#
#      DD[1]  the name 'simple'
#      DD[2]  the value k (for a k-simple system)
#      DD[3]  the matrix D
#      DD[4]  the matrix N up to order h
#      DD[5]  the order h. This value depends on the number of terms already
#             computed in M.
#      DD[6]  K
#      DD[7]  omega
#      DD[8]  phi
#      DD[9]  delta
# TODO check if mat_to simple is called without phi delta omega
mat_to_simple := proc(K, x, invars, phi, delta, omega)
    local DD, q, k, n, m, i, j, alpha, A, D, N, KK, omega1;
    global Mat_Description;

    if nargs = 6 then
    	omega1 := omega;
    else
    	omega1 := -1;
    fi;

    userinfo(5, 'MatTools', `in mat_to_simple: phi, delta, omega`, phi, delta, omega);

    DD := array(1..9);

    # q is the pole order
    q := invars[1];
    if q <= -omega1 and nops(invars) > 1 then ERROR(`wrong number of invariants`) fi;
    k := max(0, q-nops(invars)+omega1+1);
    n := mat_get_dim(K);
    userinfo(5, 'MatTools', `converting to k-simple system, k = `, k);
    userinfo(5, 'MatTools', `invariants are`, invars);
    if q <= -omega1 then
        alpha := [seq(0, i=1..n)]
    else
        m := n - convert(subsop(1 = NULL, invars), `+`);
        alpha := [seq(seq(i-nops(invars), j=1..invars[i+1]),
                      i=1..nops(invars)-1), seq(0, i=1..m)]
    fi;
    D := linalg['diag'](seq(x^(-i), i = alpha));
    userinfo(5, 'MatTools', `diagonal matrix is`, [seq(x^(-i), i = alpha)]);
    userinfo(5, 'MatTools', `evaluating system up to order`, omega1-k);
    A := mat_eval(K, x, -q, omega1-k);

    if nargs > 3 then
    	N := normalm(x^(k-omega1)*evalm(A &* map(phi, evalm(D), x) - map(delta, evalm(D), x)));
    #else
    #	N := normalm(x^(k-omega1)*evalm(A &* D - map(diff, D, x)));
    # ##TODO check
    fi;
    DD[1] := 'simple';
    DD[2] := k;
    DD[3] := op(D);
    DD[4] := op(N);
    DD[5] := 0;
    DD[6] := K;
    DD[7] := omega1;
    DD[8] := phi;
    DD[9] := delta;
    KK := new_matrix();
    Mat_Description[KK] := DD;
    KK;
end:

# eval_block_reduced(M, x, kk)
#  INPUT:  M  - a matrix series which is the block reduced matrix series
#          x  - a name
#          kk - an integer
#  OUTPUT: the updated block reduction up to order kk
#
eval_block_reduced := proc(M, x, kk)
    local n, m, h, k, l, ext, q, T, S, iT,
          A, A_21, Xh, Yh, D, D_21, N, N_21, Qh, Rh, tmp, c;
    global Mat_Description;

    userinfo(5, 'MatTools', `M[4], k are `, M[4], kk);
    if kk <= M[4] then RETURN(M) fi;
    h := M[4];
    q := M[6][1];
    m := M[5];

    if nops(M[6]) = 1 then
        # classical block-reduction
        userinfo(4, 'MatTools', `block reducing system up to order`, kk);
        userinfo(5, 'MatTools', `order is already`, h);

        n := mat_get_dim(M[14]);
        ext := mat_get_ext(M[14]);
        if not type(M[2], 'integer') then M[2] := -q fi;
        c := M[15];
        T := linalg[copyinto](M[11], Id(n), 1, n-m+1);
        iT := linalg[copyinto](evalm(-M[11]), Id(n), 1, n-m+1);

        userinfo(4, 'MatTools', `updating terms before splitting:`, h+1..kk);
        #print(M[14], x, h+1, kk);
        A := Evalm(mult(mult(iT, mat_eval(M[14], x, h+1, kk)), map(M[16],eval(T),x)), x);
        userinfo(5, 'MatTools', `New terms A=`, A);

        M[7] := evalm(M[7]+linalg['submatrix'](A, 1..n-m, 1..n-m));
        A_21 := linalg['submatrix'](mat_eval(M[14], x, -q, kk),
                                    n-m+1..n, 1..n-m);
        M[8] := evalm(M[8]+map(truncate, linalg['submatrix'](matrix_series(A,x,kk), 1..n-m,
                                         n-m+1..n), x, h+1));   # A_12
        M[3] := evalm(M[3]+linalg['submatrix'](A, n-m+1..n, n-m+1..n)); # A_22

        userinfo(5, 'MatTools', `New terms for M[8]`,map(truncate, linalg['submatrix'](matrix_series(A,x,kk), 1..n-m,
                                         n-m+1..n), x, h+1));
        # lifting block-reduction of leading matrix
        do
            h := min(op(convert(map(val, M[8], x, 0), 'set')));

            if h > kk then break fi;

            userinfo(4, 'MatTools', `block reducing, order is`, h, evalm(M[8]));
            Qh := map(coeff, matrix_series(M[8],x,h), x, h);

            # update transformation matrix
            userinfo(4, 'MatTools', `solving sylvester equation`);
            userinfo(5, 'MatTools', `h, M[20], M[9], M[10], -Qh are `, h, M[20], evalm(M[9]), evalm(M[10]), evalm(-Qh));
            #print(evalm(M[20]^(h+q)*M[9]), evalm(M[10]), evalm(-Qh));
            Xh := evalm(x^(h+q)*
	               sylvester(evalm(M[20]^(h+q)*M[9]), M[10], evalm(-Qh)));
            #print(evalm(Xh));

            userinfo(4, 'MatTools', `transforming blocks`);
            M[11] := evalm(M[11]+Xh);
            M[3] := map(lc_evala, Evalm(M[3]+mult(A_21, map(M[16],eval(Xh),x)), x), x, ext);

            userinfo(4, 'MatTools', `reducing algebraic numbers -> M[16]`, Evalm(M[16],x));
            M[8] := map(evala, Evalm(M[8]+mult(M[7], map(M[16],eval(Xh),x))-mult(Xh,M[3]) -
                                        map(eval(M[17]), Xh, x), x));
            userinfo(4, 'MatTools', `transforming blocks`);
            M[7] := Evalm(M[7] - mult(Xh,A_21), x);
        od;

        # updating infos
        M[4] := kk;
        M[3] := map(seq_evala, M[3], x, ext, M[2], M[4]);
        M
    else
        # generalized block-reduction
        n := mat_get_dim(M[24]);
        ext := mat_get_ext(M[24]);
        if not type(M[2], 'integer') then
            M[2] := M[4];
            if M[2] < -q then M[2] := -q fi
        fi;
        k := M[30]+q-nops(M[6])+1;
        c := M[27];
        if M[2] = -q then l := q else l := k fi;
        userinfo(4, 'MatTools',
                 `block reducing k-simple operator up to order`, l+kk+1); # TODO to be checked
        tmp := simple_eval(M[21], x, 0, l+kk+1);
        D := mult(M[22], mult(tmp[1], M[23]));
        N := mult(M[22], mult(tmp[2], M[23]));

        S := linalg[copyinto](M[17], Id(n), 1, n-m+1);
        iT := linalg[copyinto](evalm(-M[18]), Id(n), 1, n-m+1);

        userinfo(4, 'MatTools',
                 `updating terms before generalized splitting`);
        D := map(lc_evala, mult(iT, mult(D, S)), x, ext);
        N := map(lc_evala, mult(iT, evalm(mult(N, map(M[28],eval(S),x))-
                           		mult(D, evalm(x^(k-M[30])*map(eval(M[29]), eval(S), x))))), x, ext);

        # get block-matrices of D
        M[7] := linalg['submatrix'](D, 1..n-m, 1..n-m);     # D_11
        D_21 := linalg['submatrix'](D, n-m+1..n, 1..n-m);   # D_21
        M[8] := linalg['submatrix'](D, 1..n-m, n-m+1..n);   # D_12
        M[9] := linalg['submatrix'](D, n-m+1..n, n-m+1..n); # D_22

        # get block-matrices of N
        M[10] := linalg['submatrix'](N, 1..n-m, 1..n-m);     # N_11
        N_21  := linalg['submatrix'](N, n-m+1..n, 1..n-m);   # N_21
        M[11] := linalg['submatrix'](N, 1..n-m, n-m+1..n);   # N_12
        M[12] := linalg['submatrix'](N, n-m+1..n, n-m+1..n); # N_22

        # "lifting block-reduction of leading matrix
        do
            h := min(mat_val(M[8], x, 0), mat_val(M[11], x, 0));
            if h > l+kk+1 then break fi;
            userinfo(4, 'MatTools', `attention! block reducing, order is`, h);
            Qh := map(coeff, matrix_series(M[8],x,h), x, h);
            Rh := map(coeff, matrix_series(M[11],x,h), x, h);

            # update transformation matrix
            userinfo(4, 'MatTools', `solving sylvester equation`);
            tmp := sim_sylvester(evalm(M[32]^h*M[15]), M[13], M[16], M[14],
                        evalm(-Rh), evalm(-Qh)); ##, M[19], M[20], c, h); ##TODO fix the cache

            userinfo(5, 'MatTools', `M[32]^h*M[15], M[13], M[16], M[14], -Rh, -Qh, M[19], M[20] are `,
                     evalm(M[32]^h*M[15]), M[13], M[16], M[14], evalm(-Rh), evalm(-Qh), M[19], M[20]);
            userinfo(4, 'MatTools', `transforming blocks`);
            Xh := evalm(x^h*tmp[1]);
            Yh := evalm(x^h*tmp[2]);
            M[17] := evalm(M[17]+Xh);
            M[18] := evalm(M[18]+Yh);

            # update D_22 and D_12 =Q
            M[9] := Evalm(M[9]+mult(D_21,Xh), x);
            userinfo(4, 'MatTools', `reducing algebraic numbers in D`);
            M[8] := map(lc_evala,
                        evalm(M[8]+mult(M[7],Xh)-mult(Yh,M[9])), x, ext);

            # update N_22 and N_12 (=R)
            M[12] := Evalm(M[12]+mult(N_21,map(M[28],eval(Xh),x))-
                           x^(k-M[30])*mult(D_21, map(eval(M[29]), eval(Xh), x)), x);
            userinfo(4, 'MatTools', `reducing algebraic numbers in N`);
            M[11] := map(lc_evala, normalm(M[11]+mult(M[10],map(M[28],eval(Xh),x))-mult(Yh,M[12])-
                              x^(k-M[30])*mult(M[7], map(eval(M[29]), eval(Xh), x))), x, ext);

            # update D_11 and N_11
            M[7] := evalm(M[7]-mult(Yh,D_21));
            M[10] := evalm(M[10] - mult(map(M[28],eval(Yh),x),N_21));
        od;

        # updating infos
        M[3] := map(expand,
                    evalm(convert_back(M[9], M[12], x, l+kk+1)/x^(k-M[30])));
        M[4] := kk;
        M
    fi
end:

# convert_back(D, N, x, kk)
#  INPUT:  D  - a matrix polynomial
#          N  - a matrix polynomial
#          x  - a name
#          kk - an integer
#  OUTPUT: the truncated series expansion of D^(-1)N up to order o(x^k)
#          Here, one should use Pade-approximation, but at the moment the
#          inverting and calling series seems to be efficient.
#
convert_back := proc(D, N, x, kk)
    local iD, v, A;

    iD := linalg['inverse'](D);
    v := mat_val(iD, x, 0);
    A:=evalm(x^v*map(convert,
                  map(taylor, normalm((iD &* N)/x^v), x, kk+1),
                  'polynom'));
    map(collect, A, x, evala)
end:

# simple_eval(K, x, i, j)
#  INPUT:  K - a key of a simple operator S in internal representation
#          x  - a name
#          i  - an integer
#          j  - an integer
#  OUTPUT: the truncated series expansion of D and N from x^i to x^j where
#          S = Dx^(k+1)d/dx-N is a k-simple operator
#
simple_eval := proc(K, x, i, j)
    local n, A, N, S;
    global Mat_Description;

    S := Mat_Description[K];
    n := mat_get_dim(S[6]);
    if j < 0 or i > j then
        n := mat_get_dim(K);
        evalm(linalg['matrix'](n, n, 0))
    elif S[1] <> 'simple'
        then ERROR(`argument has inproper format`)
    else
        if j > S[5] then
            userinfo(5, 'MatTools',
                     `computing more terms up to order`, j-S[2]);
            A := mat_eval(S[6], x, -S[2]+S[7]+S[5]+1, j-S[2]+S[7]);
            N := normalm(x^(S[2]-S[7])*(A &* map(S[8],S[3],x)));
            S[4] := Evalm(S[4] + N, x);
            S[5] := j;
        fi;
    fi;
    [map(truncate, S[3], x, i, j), map(truncate, matrix_series(S[4],x,j), x, i, j)]
end:

# mat_get_terms(M, x)
#  INPUT:  M - a key of a matrix series M in internal representation
#          x - a name
#  OUTPUT: the already computed accurate terms of the series expansion of M
#
mat_get_terms := proc(K, x)
    local M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[2] = 'NN' then
        mat_valuation(K,x);
    fi;
    if M[1] = 'transformed' then
        map(evala, map(truncate, M[3], x, M[2], M[7]))
    elif M[1] = 'pseudo_transformed' then
        map(evala, map(truncate, matrix_series(M[3],x,M[7]), x, M[2], M[7]))
    elif member(M[1], {'changed_exp_part', 'term_transformed', 'ramified', 'block_reduced',
                       'substituted'}) then
        map(evala, map(truncate, matrix_series(M[3],x,M[4]), x, M[2], M[4]))
    elif M[1] = 'copied' then
        mat_get_terms(M[2], x)
    elif M[1] = 'localized' then
        evalm(M[3]*x^M[2])
    elif M[1] = 'simple' then
        ERROR(`not yet implemented`)
    elif M[1] = 'elementary_trans' then
    	map(evala, map(truncate, M[3], x, M[2], M[4]))
    elif M[1] = 'swapped' then
    	map(evala, map(truncate, M[3], x, M[2], M[4]))
    elif M[1] = 'direct_block_reduced' then
    	map(evala, map(truncate, M[3], x, M[2], M[4]))
    elif M[1] = 'multiplied' then
        map(evala, map(truncate, M[3], x, M[2], M[4]))
	else
		ERROR(`unknown data structure type`);
    fi
end:

# mat_get_order(K, x)
#  INPUT:  K - a key of a matrix series M in internal representation
#          x - a name
#  OUTPUT: the order up to which (included) terms of M are already computed
#
mat_get_order := proc(K, x)
    local M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[2] = 'NN' then mat_valuation(M, x) fi;

    if member(M[1], {'pseudo_transformed','transformed'}) then M[7]
    elif member(M[1], {'changed_exp_part', 'ramified', 'block_reduced', 'localized', 'substituted',
                       'sum', 'difference', 'product', 'swapped', 'direct_block_reduced',
                       'term_transformed', 'elementary_trans', 'multiplied'}) then M[4]
    elif M[1] = 'copied' then mat_get_order(M[2], x)
    elif M[1] = 'simple' then ERROR(`not yet implemented`)
    fi
end:

# mat_get_indicial_polynomial(K, u)
#  INPUT:  K - a key of a simple system in internal representation
#          u - a name
#  OUTPUT: the exponential part of this series
#
mat_get_indicial_polynomial := proc(K, u)
    local tmp;

    tmp := simple_eval(K, x, 0, 0);
    linalg['det'](evalm(tmp[2]-u*tmp[1]))
end:

# mat_copy(K)
#  INPUT:  K - a key of a matrix series in internal representation
#  OUTPUT: a copy of the matrix series
#
mat_copy := proc(K)
    local M, N, KK;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'copied' then
        K
    else
        N := array(1..2);
        N[1] := 'copied';
        N[2] := K;
        KK := new_matrix();
        Mat_Description[KK] := N;
        KK
    fi
end:

# mat_colvaluation(A, x, i)
#
# computes the valuation of a column of the matrix series A
#
mat_colvaluation := proc(A, x, i)

    ERROR(`should not be called anymore`);

end:

# mat_lead_mon(M, x)
#  INPUT:  M - a matrix series in internal representation
#          x - a name
#  OUTPUT: the leading monomial of the matrix series M
#
mat_lead_mon := proc(M, x)
    local v;

    v := mat_valuation(M, x);
    mat_eval(M, x, v, v)
end:

# mat_lc(M, x)
#  INPUT:  M - a matrix series in internal representation
#          x - a name
#  OUTPUT: the leading coefficient of the matrix series M
#
mat_lc := proc(M, x)
    local v;

    v := mat_valuation(M, x);
    mat_coeff(M, x, v)
end:

# mat_coeff(M, x, i)
#  INPUT:  M - a matrix series in internal representation
#          x - a name
#  OUTPUT: the coefficient of x^i of the matrix series M
#
mat_coeff := proc(M, x, i)
        map(coeff, mat_eval(M, x, i, i), x, i)
end:

# mat_check(M, x)
#  INPUT:  M - a matrix series in internal representation
#  OUTPUT: true or false
#
mat_check := proc(MM, x)
    local A, B, C, D, N, R, v, q, T, n, m, M;
    global Mat_Description, EE;

    M:= Mat_Description[MM]; ##fix from 21/03/2012

    if M[1] = 'localized' then
        A := evalm(M[6]);
        userinfo(5, 'debug', `enter check localization`);
        if M[4]-M[2] < 0 then
            userinfo(5, 'debug', `no terms computed, localization OK`);
            RETURN(evalm(A*x^M[2]))
        fi;
        userinfo(5, 'debug', `testing series expansion, computed terms are`,
                 M[2]..M[4]);
        R := map(evala, evalm(
              M[3]-map(proc(f, x, k) convert(taylor(f, x=0, k+1), 'polynom')
                       end, M[6], x, M[4]-M[2])));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in localization`)
        else
            userinfo(5, 'debug', `localization OK`);
            RETURN(evalm(A*x^M[2]))
        fi
    fi;

    if M[1] = 'transformed' then
        userinfo(5, 'debug', `enter check transformation`);
        userinfo(5, 'debug', `testing series expansion, computed terms are`,
                 M[2]..M[7]);
        userinfo(5, 'debug', `span is`, M[6]);
        A := mat_check(M[9], x);
        if not(type(M[2], 'integer')) then
            userinfo(5, 'debug', `valuation not computed, transformation OK`);
            RETURN(transform(A, x, M[4]))
        fi;
        v := mat_valuation(M[9], x);
        userinfo(5, 'debug', `testing transformed terms:`, v..M[7]+M[6]);

        B := map(evala,
                 evalm(x^v*map(proc(f, x, k)
                                   convert(taylor(f, x=0, k+1), 'polynom')
                               end,
                               map(normal, evalm(A/x^v)), x, M[7]+M[6]-v)));
        ###print(`gotten recursively`, B);
        B := transform(B, x, M[4]);
        R := map(evala@normal, evalm(M[3]-B));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in transformation`)
        else
            userinfo(5, 'debug', `transformation OK`);
            RETURN(transform(A, x, M[4]))
        fi
    fi;


    if M[1] = 'pseudo_transformed' then
        userinfo(5, 'debug', `enter check transformation`);
        userinfo(5, 'debug', `testing series expansion, computed terms are`,
                 M[2]..M[7]);
        userinfo(5, 'debug', `span is`, M[6]);
        A := mat_check(M[9], x);
        if not(type(M[2], 'integer')) then
            userinfo(5, 'debug', `valuation not computed, transformation OK`);
            RETURN(pseudo_transform(A, x, M[4], M[10], M[11]))
        fi;
        v := mat_valuation(M[9], x);
        userinfo(5, 'debug', `testing transformed terms:`, v..M[7]+M[6]);

        B := map(evala,
                 evalm(x^v*map(proc(f, x, k)
                                   convert(taylor(f, x=0, k+1), 'polynom')
                               end,
                               map(normal, evalm(A/x^v)), x, M[7]+M[6]-v)));
        ###print(`gotten recursively`, B);
        B := pseudo_transform(B, x, M[4], M[10], M[11]);
        R := map(evala@normal, evalm(M[3]-B));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in transformation`)
        else
            userinfo(5, 'debug', `transformation OK`);
            RETURN(pseudo_transform(A, x, M[4], M[10], M[11]))
        fi
    fi;

    if M[1] = 'changed_exp_part' then
        userinfo(5, 'debug', `enter check exponential part`);
        if M[4]-M[2] < 0 or not(type(M[2], 'integer')) then
            A := evalm(-M[5]+mat_check(M[7], x));
            userinfo(5, 'debug', `no terms computed, exponential part OK`);
            RETURN(evalm(A))
        fi;
        v := mat_valuation(M[7], x);
        A := mat_check(M[7], x);
        B := map(normal, evalm((A-M[5])/x^M[2]));
        userinfo(5, 'debug', `testing exponential part, terms are`,
                 M[2]..M[4]);
        R := map(evala, evalm(M[3]/x^M[2]-
                 map(proc(f, x, k) convert(taylor(f, x=0, k+1), 'polynom')
                     end, B, x, M[4]-M[2])   ));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in change exp. part`)
        else
            userinfo(5, 'debug', `exponential part OK`);
            RETURN(evalm(A-M[5]))
        fi
    fi;

    if M[1] = 'copied' then
        userinfo(5, 'debug', `enter check copy`);
        RETURN(mat_check(M[2], x))
    fi;

    if M[1] = 'substituted' then
        userinfo(5, 'debug', `enter check substitution`);
        A := mat_check(M[7], x);
        RETURN(subs(M[5], eval(A)))
    fi;

    if M[1] = 'ramified' then
        userinfo(5, 'debug', `enter check ramification`);
        if M[4]-M[2] < 0 or not(type(M[2], 'integer')) then
            userinfo(5, 'debug', `no terms computed, ramification OK`);
            A := mat_check(M[7], x);
            B := Evalm(M[5]*M[6]*x^(M[6]-1)*subs(x = M[5]*x^M[6], eval(A)), x);
            RETURN(evalm(B))
        fi;
        v := mat_valuation(M[7], x);
        A := mat_check(M[7], x);
        B := Evalm(M[5]*M[6]*x^(M[6]-1)*subs(x = M[5]*x^M[6], eval(A)), x);
        C := normalm(B/x^M[2]);
        userinfo(5, 'debug', `testing ramification, terms are`,
                 M[2]..M[4]);
        R := map(evala, evalm(M[3]/x^M[2]-
                 map(proc(f, x, k) convert(taylor(f, x=0, k+1), 'polynom')
                     end, C, x, M[4]-M[2])   ));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in ramification`)
        else
            userinfo(5, 'debug', `ramification OK`);
            RETURN(evalm(B))
        fi
    fi;

    if M[1] = 'simple' then
        userinfo(5, 'debug', `enter check simple operator`);
        v := mat_valuation(M[6], x);
        userinfo(5, 'debug', `getting terms, order is`, -(M[2]+1)+M[5]);
        A := mat_eval(M[6], x, v, -(M[2]+1)+M[5]);D := evalm(M[3]);
        mat_check(M[6], x);
        D := evalm(M[3]);
        N := map(truncate, Evalm(x^(M[2]+1)*(A &* D - map(diff, D, x)), x), x,
                           0, M[5]);
        R := map(evala, map(truncate, normalm(N-M[4]), x, 0, M[5]));
        if not linalg['iszero'](R) then
            EE := M;
            ERROR(`in simple operator`)
        else
            userinfo(5, 'debug', `simple operator OK`);
            RETURN(true)
        fi
    fi;

    if M[1] = 'block_reduced' then
        userinfo(5, 'debug', `enter check block-reduced operator`);
        if nops(M[6]) = 1 then
            userinfo(5, 'debug', `classical block-reduction`);
            B := mat_check(M[14], x);
            q := M[6][1];
            userinfo(5, 'debug', `getting terms:`, -q..M[4]);
            A := mat_eval(M[14], x, -q, M[4]);
            n := mat_get_dim(M[14]);
            m := M[5];
            T := linalg['copyinto'](M[11], Id(mat_get_dim(M[14])), 1, n-m+1);
            A := map(collect, map(evala, transform(A, x, T)), x);
            R := map(truncate,
                     normalm(linalg['submatrix'](A, n-m+1..n, n-m+1..n)-M[3]),
                     x, -q, M[4]);
            if not linalg['iszero'](R) then
                EE := M;
                ERROR(`in block--reduced operator: wrong reduced system`)
            elif not mat_val(linalg['submatrix'](A, 1..n-m, n-m+1..n), x, 0)
                     > M[4] then
                EE := M;
                ERROR(`in block--reduced operator: order not high enough`)
            else
                userinfo(5, 'debug', `order is`, M[4]);
                userinfo(5, 'debug', `block--reduced operator OK`);
                RETURN(linalg['submatrix'](transform(B, x, T), n-m+1..n, n-m+1..n))
            fi
        else
            userinfo(5, 'debug', `generalized block-reduction`);
            B := mat_check(M[24], x);
            q := M[6][1];
            userinfo(5, 'debug', `getting terms:`, -q..M[4]);
            A := mat_eval(M[24], x, -q, M[4]);
            n := mat_get_dim(M[24]);
            m := M[5];
            T := linalg['copyinto'](M[17], Id(n), 1, n-m+1);
            A := map(collect, map(evala, transform(A, x, T)), x);
            R := map(truncate,
                     normalm(linalg['submatrix'](A, n-m+1..n, n-m+1..n)-M[3]),
                     x, -q, M[4]);
            if not linalg['iszero'](R) then
                EE := M;
                ERROR(`in block--reduced operator: wrong reduced system`)
            #elif not mat_val(linalg['submatrix'](A, 1..n-m, n-m+1..n), x, 0)
            #         > M[4] then
            #    EE := evalm(A);
            #    ERROR(`in block--reduced operator: order not high enough`)
            else
                userinfo(5, 'debug', `order is`, M[4]);
                userinfo(5, 'debug', `block--reduced operator OK`);
                RETURN(linalg['submatrix'](transform(B, x, T), n-m+1..n, n-m+1..n))
            fi
        fi
    fi;

    ERROR(`in mat_check: data type not supported`)
end:

# mat_sum(A, B, x)
#  INPUT:  A - a matrix series in internal representation
#	   B - a matrix series in internal representation
#          x - a name
#  OUTPUT: The sum of the matrices A and B in internal representation
#
mat_sum := proc(A, B, x)
	global Mat_Description, Valuation_Bound;
	local C, va, vb, h, Ct, CC;

	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;

	C := array(1..7);
	C[1] := 'sum';
	va := mat_valuation(A,x);
	vb := mat_valuation(B,x);
	h := min(mat_get_order(A,x), mat_get_order(B,x));
	Ct := Evalm(mat_eval(A,x,va,h) + mat_eval(B, x, vb,h),x);

	C[2] := 'NN';
	C[3] := Ct;
	C[4] := h;
	C[5] := NULL;
	C[6] := A;
	C[7] := B;

	CC := new_matrix();
	Mat_Description[CC]:= C;

	RETURN(CC);
end:

# mat_difference(A, B, x)
#  INPUT:  A - a matrix series in internal representation
#	   B - a matrix series in internal representation
#          x - a name
#  OUTPUT: The difference of the matrices A and B in internal representation
#
mat_difference := proc(A, B, x)
	global Mat_Description, Valuation_Bound;
	local C, va, vb, h, Ct, CC;

	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;

	C := array(1..7);
	C[1] := 'difference';
	va := mat_valuation(A,x);
	vb := mat_valuation(B,x);
	h := min(mat_get_order(A,x), mat_get_order(B,x));
	Ct := Evalm(mat_eval(A,x,va,h) - mat_eval(B, x, vb,h),x);

	C[2] := 'NN';
	C[3] := Ct;
	C[4] := h;
	C[5] := NULL;
	C[6] := A;
	C[7] := B;

	CC := new_matrix();
	Mat_Description[CC]:= C;

	RETURN(CC);
end:

# mat_product(A, B, x)
#  INPUT:  A - a matrix series in internal representation
#	   B - a matrix series in internal representation
#          x - a name
#  OUTPUT: The product of the matrices A and B in internal representation
#	   C[1] is the symbol 'product'
#	   C[2] is the valuation of A&*B or the symbol 'NN'
#	   C[3] is the product of the first C[5] coefficients of A and B
# 	   C[4] is an integer, the order up to which coefficients in C[3] are correct
#	   C[5] is an integer, the number of terms that were used in both A and B to calculate C[3]
#	   C[6] is the local matrix series A
# 	   C[7] is the local matrix series B
#
mat_product := proc(A, B, x)
	global Mat_Description, Valuation_Bound;
	local C, va, vb, ht, h, Ct, CC, lc;

	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;

	C := array(1..7);
	C[1] := 'product';
	va := mat_valuation(A,x);
	vb := mat_valuation(B,x);
	lc := min(mat_get_order(A,x)-va+1, mat_get_order(B,x)-vb+1);
    Ct := map(normal,evalm(mat_eval(A,x,va,va+lc-1) &* mat_eval(B, x, vb,vb+lc-1)));

	C[2] := 'NN';
	C[3] := Ct;
	C[4] := va + vb + lc - 1;
	C[5] := lc;
	C[6] := A;
	C[7] := B;

	CC := new_matrix();
	Mat_Description[CC]:= C;

	RETURN(CC);
end:

# mat_apply(A, Operator, p, x)
#  INPUT:  A - a matrix
#	   Operator - Either automorphism Phi or Pseudo-derivative Delta
#	   p - an irreducible polynomial or infinity
#          x - a name
#  OUTPUT: The resulting matrix after application of Phi or Delta
#	   C[1] is the symbol 'apply'
#	   C[2] is the valuation of the resulting matrix or the symbol 'NN'
#	   C[3] is the first C[6] coefficients of the resulting matrix
#	   C[4] is the operator
#	   C[5] is the order to which the operator is computed
#	   C[6] is the matrix A
#
#mat_apply := proc(A, Operator, x)
#	global Mat_Description, Valuation_Bound;
#	local Ct, Op, h, CC, C, va;
#
#	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;
#
#	C := array(1..6);
#	C[1] := 'apply';
#	va := mat_valuation(A,x);
#	h := mat_get_order(A,x);
#	Op :=
#	Ct := map(Op,A,x);
#	##print("Ct=",Ct);
#
#	C[2] := 'NN';
#	C[3] := Ct;
#	C[4] := Op;
#	C[5] := h;
#	C[6] := A;
#	##print("h=",h);
#	CC := new_matrix();
#	Mat_Description[CC]:= C;
#
#	RETURN(CC);
#end:

# mat_block_reduce(K, x, invars, P, lambda)
#   INPUT:  K      - the key of a representation of a matrix series M
#           x      - a name
#           invars - a list of non negative integers
#           P      - a polynomial in lambda
#           lambda - a name
#
#   OUTPUT: Let invars = [q, n1,..,nj] and set k = q-j-1. This procedure
#           supposes that the differential system Y'= MY can be written as
#
#                            D x^(k+1) Y' = NY,
#
#           where one has P1(lambda) := det(D_0 -lambda N_0) is a polynomial
#           in lambda not vanishing identically. This system is called
#           k-simple. Furthermore it holds P1 = P*P2 and P and P2 are
#           pairwise prime.
#
#           The output if this fucntion is the representation of a "block-
#           reduced" system which represents a matrix series F of dimension
#           m = deg(P) such that:
#
#                    - there exist a formal power series transformation T
#                      such that
#                                      [ D  0 ]
#                               T[M] = [      ]
#                                      [ E  F ],
#
#                    - if P is not a constant, the pole order of F is at most
#                      k+1. Write F = F_0 x^{k+1} + ... The characteristic
#                      polynomial of F_0 is
#
#                            det(F_0-lambda I) = P(lambda).
#
#                    - if P is a constant, let d = dim(M)-degree(P1). The
#                      dimension of F will be d and its pole order of F will
#                      be the same of M (i.e. q), F is k-simple with an
#                      associated polynomial which is constant.
#
#           If j = 0, this is a variant of the classical splitting lemma. In
#           this case, we store F in an array with 14 elements where
#
#   F[1]  is the symbol 'block_reduced'
#   F[2]  is k, the pole order of F
#   F[3]  is the matrix A_22, it coincides with the series expansion of F up
#         to order o(x^j) where j = F[4]
#   F[4]  is the above order j
#   F[5]  is the integer m
#   F[6]  is a list containing the pole order q
#   F[7]  is the block A_11
#   F[8]  is the block A_12 = Q
#   F[9]  is the upper block A0_11
#   F[10] is the lower block A0_22
#   F[11] is the matrix X
#   F[12] intermediate data for solving the sylvester equations
#   F[13] idem
#   F[14] the matrix series M
#   F[15]
#   F[16] phi as an proceduce
#   F[17] delta
#   F[18] omega
#   F[19] c, one of the local characteristics of phi and delta
#   F[20] q, one of the local characteristics of phi and delta
#
#
#           If j > 0, the use method is a generalization based on the theory
#           of regular matrix pencils. We store F in an array with elements
#
#   F[1]  is the symbol 'block_reduced'
#   F[2]  is k, the pole order of F
#   F[3]  is the series expansion of F up to order o(x^j) where j = F[4]
#   F[4]  is the above order j
#   F[5]  is the integer m     k-simple operator Dx^(k+1)d/dx-N
#   F[6]  is the list invars
#   F[7]  the upper block D_11
#   F[8]  the upper block D_12 (Q)
#   F[9]  the lower block D_22
#   F[10] the upper block N_11
#   F[11] the upper block N_12 (R)
#   F[12] the lower block N_22
#   F[13] the upper diagonal block D0_11
#   F[14] the lower diagonal block D0_22
#   F[15] the upper diagonal block N0_11
#   F[16] the lower diagonal block N0_22
#   F[17] the matrix X
#   F[18] the matrix Y
#   F[19] help data for solving the sylvester equation: K
#   F[20] the same: L
#   F[21] the matrix series M converted to a k-simple system
#   F[22] the constant transformation S and
#   F[23] the constant transformation T such that S(N_0-lambda D_0)T
#         is block-reduced
#   F[24] the matrix series M
#   F[25] the matrix D^{-1}
#   F[26]
#   F[27]
#   F[28] automorphism phi given as a proceduce
#   F[29] automorphism delta
#   F[30] omega
#   F[31] c, one of the local characteristics of phi and delta
#   F[32] q, one of the local characteristics of phi and delta
#
mat_block_reduce := proc(K, x, invars, P, lambda, phi1, delta1, omega1, cc, qq)
    local  F, m, n, q, k, tmp, B0, D0, N0, H, S, T, B, c, KK, r, phi, delta, omega;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_block_reduce_algebraic(args))
    fi;

    if nargs <=7 then
        phi:= Default_phi;
        delta:= Default_delta;
        omega := Default_omega;
    else
        phi:= phi1;
        delta:= delta1;
        omega := omega1;
    fi;

    if nops(invars) = 1 then
        # classical splitting lemma
        F := array(1..20);
        F[1] := 'block_reduced';
        F[16] := phi; ### Eckhard: this is better!
        F[17] := delta;
        F[18] := omega;

#to fix the calls from FormalSuperReduction (these calls do not have a
# the last 2 parameters)
        if nargs=7 or nargs=10 then
		# Actually only needed for direct block-reduction
        F[19] := args[-2]; #=cc
        F[20] := args[-1]; #=qq
       fi;

        q := invars[1];
        F[4] := -q;

        # init new valuation. Not known if leading matrix of reduced
        # system is nilpotent
        if ldegree(P, lambda) = degree(P, lambda) then
            F[2] := 'NN'
        else
            F[2] := -q
        fi;

        n := mat_get_dim(K);
        m := degree(P, lambda);
        B := mat_copy(K);
        B0 := map(coeff, mat_eval(B, x, -q, -q), x, -q);
        #print( mat_eval(B, x, -q, -q), evalm(B0));
        # block-reduce the leading matrix
        userinfo(4, 'MatTools',
                 `computing block-reduced form of leading matrix`, B0);
        H := pencil_block_reduce(B0, Id(n), P, lambda, 'S', 'T');
		userinfo(5, 'MatTools', `T, S, B0 = `, S,T, map(denom, B0));

        # retrieve block reduced leading matrix
        B0  := map(coeff, H, lambda, 0);
        userinfo(5, 'MatTools', `B0 = `, evalm(B0));
        F[5] := m;
        F[6] := [q];
        F[7] := evalm(linalg['submatrix'](B0, 1..n-m, 1..n-m)/x^q);
        F[8] := linalg['submatrix'](B0, 1..n-m, n-m+1..n);
        F[9] := linalg['submatrix'](B0, 1..n-m, 1..n-m);
        F[10] := linalg['submatrix'](B0, n-m+1..n, n-m+1..n);
        F[3] := evalm(F[10]/x^q);
        ###print(F[3],m,n);
        #F[9] := linalg['matrix'](n-m, m, 0);
        #F[10] := linalg['matrix'](n-m, m, 0);
        F[11] := linalg['matrix'](n-m, m, 0); # X
        B := mat_pseudo_transform(B, x, T, evalm(1/T), 0, phi, delta, omega);
        F[14] := B;
		# TODO Flavia: to discuss
        if q <> 1 then
            F[15] := c
        else
            F[15] := NULL
        fi
    else
        # generalized splitting lemma
        F := array(1..32);
        F[1] := 'block_reduced';
        F[2] := 'NN';
        F[28] := phi;
        F[29] := delta;
        F[30] := omega;

        if nargs=7 or nargs=10 then
		# Actually only needed for eval_ block_reduce when solving the
        # Sylvester equation
        F[31] := args[-2]; #=cc
        F[32] := args[-1]; #=qq
        fi;

        q := invars[1];
        r := q+omega;
        k := r-nops(invars)+1;

        # init new valuation. Not known if leading matrix of reduced
        # system is nilpotent
        if degree(P, lambda) = 0 then
            F[2] := -q;
            F[4] := -q-1;
        elif ldegree(P, lambda) = degree(P, lambda) then
            F[2] := 'NN';
            F[4] := -k+omega;
        else
            F[2] := -k+omega;
            F[4] := -k+omega;
        fi;
        n := mat_get_dim(K);
        F[6] := invars;
        B := mat_copy(K);

        # convert into k-simple operator and store this
        F[21] := mat_to_simple(K, x, invars, phi, delta, omega);
        tmp := simple_eval(F[21], x, 0, 0);
        D0 := tmp[1];
        N0 := tmp[2];

        # block-reduce the leading pencil and store coefficients
        userinfo(4, 'MatTools',
                 `computing block-reduced form of leading pencil`);
        H := pencil_block_reduce(N0, D0, P, lambda, 'S', 'T');
        # retrieve block reduced pencil
        N0   := map(coeff, H, lambda, 0);
        D0   := evalm(-map(coeff, H, lambda, 1));

        # compute dimension of reduced block -> optimize this !!!
        m := degree(P, lambda);
        if m = 0 then
            m := n - degree(linalg['det'](evalm(N0-lambda*D0)), lambda)
        fi;
        F[5] := m;

        # store blocks of block reduced pencil
        F[13] := linalg['submatrix'](D0, 1..n-m, 1..n-m);      # store D0_11
        F[14] := linalg['submatrix'](D0, n-m+1..n, n-m+1..n);  # store D0_22
        F[15] := linalg['submatrix'](N0, 1..n-m, 1..n-m);      # store N0_11
        F[16] := linalg['submatrix'](N0, n-m+1..n, n-m+1..n);  # store N0_22

        F[17] := linalg['matrix'](n-m, m, 0); # the matrix X
        F[18] := linalg['matrix'](n-m, m, 0); # the matrix Y

        if F[2] = F[4] then F[3] := evalm(F[16]/x^(k-omega)) fi;

        # do diag times constant transformation on system, save S, T and B
        ##print("span",r-k);
        B := mat_pseudo_transform(B, x, evalm(Mat_Description[F[21]][3]&*T),
                                  linalg['inverse'](evalm(Mat_Description[F[21]][3]&*T)), r-k, phi, delta, omega);

        F[22] := S;
        F[23] := T;
        F[24] := B;
        ##print("S,T,B",S,T,B);
        #iD := linalg['inverse'](evalm(S&*F[21][3]&*T));
        #F[25] := linalg['submatrix'](iD, n-m+1..n, 1..n-m);
        #F[26] := linalg['submatrix'](iD, n-m+1..n, n-m+1..n);
        if k = 0 then F[27] := c else F[27] := NULL fi
    fi;
    KK := new_matrix();
    Mat_Description[KK] := F;
    KK
end:

# readlib('mat_change_exp_part')(K, x, w)
#   INPUT:  K - the key of a representation of a matrix series M
#           x - a name
#           w - an expression polynomial in x and x^(-1)
#   OUTPUT: N - the matrix series which results from the change of exp. part
#           w. We represent this in an array with 6 elements:
#
#      N[1] is the symbol 'changed_exp_part',
#      N[2] is a integer: the valuation of the matrix series,
#      N[3] contains terms of the new matrix series up to order N[4]
#           included
#      N[4] is the order up to which terms in N[3] are computed
#      N[5] is the exponential part w
#      N[6] is the algebraic extension needed for the coefficients of the
#           matrix series
#      N[7] is the matrix series M
#
mat_change_exp_part := proc(K, x, w)
    local N, KK, ext;
    global Mat_Description;

    #if readlib('mat_get_type')(K) = 'algebraic' then
    if mat_get_type(K) = 'algebraic' then
        RETURN(readlib(`mat_change_eigenvalue`)(K, x, w))
    fi;
    #readlib(`utils`);
    #readlib(`mat_tools`);
    if evala(w) = 0 then RETURN(K) fi;
    N := array(1..7);
    N[1] := 'changed_exp_part';
    N[2] := 'NN';
    ext := readlib('mat_get_ext')(K);
    N[3] := map(lc_evala, evalm(mat_get_terms(K, x)-w), x, ext);
    N[4] := mat_get_order(K, x);
    N[5] := w;
    N[6] := ext union indets(w, 'RootOf');
    N[7] := K;
    KK := new_matrix();
    Mat_Description[KK] := N;
    KK
end:

# mat_term_transform(K, x, mu, r, phi, delta, omega)
#   INPUT:  K - the key of a representation of a matrix series M
#           x - a name
#           w - an expression polynomial in x and x^(-1)
#   OUTPUT: N - the matrix series which results from the change of exp. part
#           w. We represent this in an array with 6 elements:
#
#      N[1] is the symbol 'term_transformed',
#      N[2] is a integer: the valuation of the matrix series,
#      N[3] contains terms of the new matrix series up to order N[4]
#           included
#      N[4] is the order up to which terms in N[3] are computed
#      N[5] is the parameter mu
#      N[6] is the algebraic extension needed for the coefficients of the
#           matrix series
#      N[7] is the matrix series M
#	   N[8] is the list [alpha, t] specific to the term transformation
#	   N[9] is the list [phi, delta, omega]
#
mat_term_transform := proc(K, x, mu, r, phi, delta, omega)
    local N, KK, ext, alpha, t;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        #print("here");
        RETURN(mat_change_eigenvalue(K, x, mu))
    fi;

    if evala(mu) = 0 then RETURN(K) fi;

	if phi(x,x) = x then
		alpha := 1;
		t := 0;
	elif r > omega then
    	alpha := 1/mu;
    	t := r - omega;
    elif r = omega then
        alpha := 1/(mu+1);
    	t := 0;
    else
    	alpha := 1;
    	t := 0;
    fi;

	userinfo(4, 'MatTools',
                 `term transformation with alpha =`, alpha, ` and t = `, t, ` and mu = `, mu);

    N := array(1..9);
    N[1] := 'term_transformed';
    N[2] := 'NN';
    ext := mat_get_ext(K);
	userinfo(5, 'MatTools', `take away:`, mu*x^mat_valuation(K, x), ` from `, mat_get_terms(K, x));
    #print("ext");
    N[3] := map(radnormal,evalm(alpha*x^t*(mat_get_terms(K, x)-mu*x^mat_valuation(K, x))));
    N[4] := mat_get_order(K, x);
    N[5] := mu/x^(r-omega);
    N[6] := ext union indets(mu, 'RootOf');
    N[7] := K;

#to fix the calls from FormalSuperReduction (these calls do not have a
# the last 2 parameters)
#    if nargs > 3 then
    N[8] := [alpha, t];
	N[9] := [phi, delta, omega];
#   N[9] := mu/x^mat_valuation(K, x);
#    fi;

    KK := new_matrix();
    Mat_Description[KK] := N;
    KK
end:

# mat_convert(A, x, x0 [,which])
#   INPUT:  A  - a rational function matrix
#           x  - a name
#           x0 - a rational, an algebraic number or infinity
#           which - a name (either 'differential' or 'algebraic')
#   OUTPUT: The internal representation of a local matrix series at the
#           point x0
#
#   We represent this in an array M with 8 elements:
#      M[1] is the symbol 'localized',
#      M[2] is a integer: the valuation of the matrix series,
#      M[3] contains a truncated series expansion of A at the point x0 up to
#           x^M[4] (including). Terms are divided by x^valuation(M),
#      M[4] is the order k up to which terms have already been computed,
#      M[5] is the point x0,
#      M[6] is the matrix A where x0 has been shifted to the origin divided
#           by x^valuation(A, x0),
#      M[7] is the algebraic extension needed for the coefficients of the
#           matrix series.
#      M[8] is the "type" of the problem considered (differential or algebraic)
#
mat_convert := proc(A, x, x0, which)
    local n, M, v, N;
    global Mat_Description;

    userinfo(5, 'MatTools', `enter mat_convert`);
    M := array(1..8);
    n := linalg['rowdim'](A);
    M[1] := 'localized';
    M[3] := linalg['matrix'](n, n, 0);
    M[5] := x0;
    M[8] := 'differential';
    if nargs = 4 and which = 'algebraic' then M[8] := 'algebraic' fi;

    # move the point x0 to the origin by a substitution
    userinfo(5, 'MatTools', `localizing at`, x0);
    if x0 <> infinity then
        if has(x0, 'RootOf') then
            # reduce algebraic numbers of the denominator up to
            # a sufficient high order
            M[6] := map(proc(f, x)
                            normal(numer(f)/lc_evala(denom(f), x, {x0}))
                        end,
                        subs(x = x+x0, eval(A)), x);
        else
            M[6] := map(lc_evala, normalm(subs(x = x+x0, eval(A))), x)
        fi
    else
        #M[6] := map(evala, normalm(-1/x^2*subs(x = 1/x, eval(A))));
        M[6] := map(evala, normalm(subs(x = 1/x, eval(A))));
    fi;
    v := min(op(map(val, convert(M[6], 'set'), x, 0)));
    M[6] := normalm(M[6]/x^v);
    M[2] := v;
    M[4] := v-1;
    # should test, whether RootOf's are independent
    M[7] := indets(x0, 'RootOf') union indets(convert(A, 'set'), 'RootOf');
    N := new_matrix();
    Mat_Description[N] := M;
    N
end:

# mat_block_reduce(K, x, invars, P, lambda)
#   INPUT:  K      - the key of a representation of a matrix series M
#           x      - a name
#           invars - a list of non negative integers
#           P      - a polynomial in lambda
#           lambda - a name
#
#   OUTPUT: Let invars = [q, n1,..,nj] and set k = q-j-1. This procedure
#           supposes that the differential system Y'= MY can be written as
#
#                            D x^(k+1) Y' = NY,
#
#           where one has P1(lambda) := det(D_0 -lambda N_0) is a polynomial
#           in lambda not vanishing identically. This system is called
#           k-simple. Furthermore it holds P1 = P*P2 and P and P2 are
#           pairwise prime.
#
#           The output if this fucntion is the representation of a "block-
#           reduced" system which represents a matrix series F of dimension
#           m = deg(P) such that:
#
#                    - there exist a formal power series transformation T
#                      such that
#                                      [ D  0 ]
#                               T[M] = [      ]
#                                      [ E  F ],
#
#                    - if P is not a constant, the pole order of F is at most
#                      k+1. Write F = F_0 x^{k+1} + ... The characteristic
#                      polynomial of F_0 is
#
#                            det(F_0-lambda I) = P(lambda).
#
#                    - if P is a constant, let d = dim(M)-degree(P1). The
#                      dimension of F will be d and its pole order of F will
#                      be the same of M (i.e. q), F is k-simple with an
#                      associated polynomial which is constant.
#
#           If j = 0, this is a variant of the classical splitting lemma. In
#           this case, we store F in an array with 14 elements where
#
#   M[1]  is the symbol 'direct_block_reduced'
#   M[2]  is k, the valuation of A
#   M[3]  is the series expansion of A up to order o(x^j) where j = M[4]
#   M[4]  is the above order j
#   M[5]  is r0, the rank of the leading matrix of A
#   M[6]  is S
#   M[7]  q, the dimension of the lower block
#   M[8]  is the "Dietrich" matrix
#   M[9]  is the invertable submatrix of the L-Matrix
#   M[10]  is the current transformation (product of
#   M[11] is the matrix A
#
#
mat_direct_block_reduce := proc(A, x, invars, s, q, qq, phi1, delta1)
local  B, M, N, B0, MM, n, phi, delta;
global Mat_Description;

    userinfo(4, 'MatTools', `mat_direct_block_reduce with phi, delta`, phi, delta);
     if nargs < 8 then
         phi := Default_phi;
         delta := Default_delta;
     else
        phi:= phi1;
        delta:= delta1;
     fi;

	n:= mat_get_dim(A);
    M := array(1..12);
	M[1] := 'direct_block_reduced';
	M[2] := 'NN'; #-invars[1];
	M[3] := linalg['matrix'](q,q,0);
	M[4] := -invars[1];
	M[5] := invars[2];;
	M[6] := s;
	M[7] := q;
    userinfo('MatTools', `direct block reduce A,x,-invars[1],phi,delta `, A,x,-invars[1],phi,delta);
    M[8] := linalg['inverse'](linalg['submatrix'](mat_coeff(A,x,-invars[1],phi,delta),1..M[5],n-s-M[5]+1..n-s));
	userinfo(5, 'MatTools', 'direct_block_reduced',  `generate M[8] ->`, M[8]);

	if n-s-M[5]>0 then
        M[9] := linalg['inverse'](linalg['submatrix'](mat_coeff(A,x,-invars[1]+1, phi, delta),M[5]+1..n-q,[seq(i, i=1..n-s-M[5]), seq(i, i=n-s+1..n-q)]));
	else
		M[9] := NULL;
	fi;
	M[10]:= mat_copy(A);
	M[11]:= A;
	M[12] := qq;
	MM := new_matrix();
	Mat_Description[MM] := M;
	MM
end;

# mat_elementary_transformation(A, x, i, j, alpha)
#  INPUT:  A - a matrix series in internal representation
#	   x - a name
#          i - an integer
#          j - an integer
#	   alpha - a polynomial in x
#	   beta - a polynomial in x
#  OUTPUT: The elementary transformation E_ij(alpha) in internal representation
#	   B[1] is the symbol 'elementary_trans'
#	   B[2] is the valuation of B or the symbol 'NN'
#	   B[3] is the truncated power series
# 	   B[4] is the order of B
#	   B[5] is a list of lists, each entry representing a pair (i, j) of columns
#      B[6] is a 2-dimensional array, storing alpha in position [i,j] that belongs to (i,j)
#	   B[7] is the local matrix series A
#
mat_elementary_transformation := proc(A, x, i, j, alpha, phi1, delta1)
global Mat_Description, Valuation_Bound;
local M, B, v, h, Bt, BB, n, phi, delta;

    #print(`mat_elementary_transform should not be called anymore`);
    if nargs < 7 then
        phi := Default_phi;
        delta := Default_delta;
     else
        phi:= phi1;
        delta:= delta1;
     fi;

	n := mat_get_dim(A);
	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;
	if Mat_Description[A][1]<>'elementary_trans' then
		B:=array(1..7);
		B[1] := 'elementary_trans';
		v := mat_valuation(A,x,phi,delta);
		h := mat_get_order(A,x);
		Bt := mat_eval(A,x,v,h,phi,delta);

		B[2] := v;
		B[3] := Bt;
		B[4] := h;
		B[5] := [[i,j]];
		B[6] := linalg['matrix'](n, n, 0);
		B[6][i, j] := subs(x=phi,alpha);
		B[7] := A;

		BB := new_matrix();
		Mat_Description[BB]:= B;
	else
		B:=array(1..7);
		M:=Mat_Description[A];
		B[1] := 'elementary_trans';
		B[2] := M[2];
		B[3] := M[3];
		B[4] := M[4];
		if not [i,j] in M[5] then B[5] := [op(M[5]),[i,j]] else B[5] := M[5] fi;
		B[6] := evalm(M[6]);
		B[6][i,j] := B[6][i,j] + (subs(x=phi,alpha));
		B[7] := M[7];
		Bt := B[3];
		BB := new_matrix();
		Mat_Description[BB]:= B;
	fi;

	Bt := linalg[addcol](Bt,i,j,subs(x=phi,alpha));
	Bt := linalg[addrow](Bt,j,i,-(subs(x=phi,alpha)));
	Bt[i,j] := Bt[i,j]-map(delta,alpha,x);
	B[3] := Evalm(Bt, x);

	RETURN(BB);
end:

# mat_eval(K, x, i, j)
#  INPUT:  K - a key for a matrix series M in internal representation
#          x - a name
#          i - an integer
#          j - an integer
#  OUTPUT: the truncated series expansion of M from x^i to x^j
#
mat_eval := proc(K, x, i, j)
    local M, ext, n, C, Ch, jj, l, ii, U, X, h, v, nn, clist, d;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_eval_algebraic(args))
    fi;

    M := Mat_Description[K];
    n := mat_get_dim(K);

    if i > j then
        evalm(linalg['matrix'](n, n, 0))
    elif M[1] = 'transformed' then
        # M results from a transformation of M[9]
        userinfo(5, 'MatTools', `evaluating terms:`, i..j);
        userinfo(5, 'MatTools', `lookahead is`, M[6]);
        userinfo(5, 'MatTools', `computed order is`, M[7]);
        if j > M[7] then
            # desired order j exceeds already computed order M[7]
            C := mat_eval(M[9], x, M[7]+M[6]+1, M[6]+j);
            userinfo(5, 'MatTools', `updating transformed matrix`);
            C := mult(M[5], mult(C, M[4]));
            userinfo(5, 'MatTools', `reducing algebraic numbers`);
            C := map(lc_evala, C, x, M[9]);
            M[3] := Evalm(M[3]+C, x);
            M[7] := j
        fi;
        map(truncate, M[3], x, i, j)

    elif M[1] = 'pseudo_transformed' then
            # M results from a transformation of M[9]
            userinfo(5, 'MatTools', `evaluating terms:`, i..j);
            userinfo(5, 'MatTools', `lookahead is`, M[6]);
            userinfo(5, 'MatTools', `computed order is`, M[7]);
            if j > M[7] then
                # desired order j exceeds already computed order M[7]
                C := mat_eval(M[9], x, M[7]+M[6]+1, M[6]+j);
                userinfo(5, 'MatTools', `updating transformed matrix`);
                C := mult(M[5], mult(C, M[4]));
                userinfo(5, 'MatTools', `reducing algebraic numbers`);
                C := map(lc_evala, C, x, M[9]);
                M[3] := Evalm(M[3]+C, x);
                M[7] := j
            fi;
        map(truncate, matrix_series(M[3],x,j), x, i, j)


    elif M[1] = 'localized' then
        # M is the meromorphic series expansion of a rational function matrix
        ext := M[7];
        if j > M[4] then
            # We need to compute new terms of the series expansion
            userinfo(5, 'MatTools',
                     `computing new terms of series expansion`);
            M[3] := map(proc(f, x, k, ext)
                            lc_evala(convert(taylor(f, x, k+1), 'polynom'),
                                     x, ext)
                        end, M[6], x, j-M[2], ext);
            # Update info
            M[4] := j;
            userinfo(5, 'MatTools', `order is`, j);
        fi;
		# Note that only in this case, we have to take into account
        # the 'shift' by x^valuation
        # This is what we return in this case
        Evalm(x^M[2]*map(truncate, matrix_series(M[3],x,j-M[2]), x, i-M[2], j-M[2]), x)

    elif M[1] = 'changed_exp_part' then
        # M results from the matrix series M[7] by a change of exponential
        if j > M[4] then
            # We need to update stored terms
            userinfo(5, 'MatTools',
                     `updating terms after change of exponential`);
            userinfo(5, 'MatTools', `reducing algebraic numbers`);
            M[3] := map(lc_evala,
                        evalm(M[3]+M[8]*mat_eval(M[7], x, M[4]+1, j)), x, M[6]);
            M[4] := j
        fi;
        map(truncate, matrix_series(M[3],x,j), x, i, j)

	elif M[1] = 'term_transformed' then
        # M results from the matrix series M[7] by a term transformation
        if j > M[4] then
            # We need to update stored terms
            userinfo(5, 'isolde',
                     `updating terms after change of exponential`);
            userinfo(5, 'mat_eval', `reducing algebraic numbers`);
            M[3] := map(lc_evala,
                        evalm(M[3]+M[8][1]*x^(M[8][2])*mat_eval(M[7], x, M[4]+1, j)), x, M[6]);
            M[4] := j
        fi;
		userinfo(5, 'MatTools', `j-M[4] =`, j-M[4]);
        map(truncate, matrix_series(M[3],x,j), x, i, j)

    elif M[1] = 'copied' then
        mat_eval(M[2], x, i, j);

    elif M[1] = 'ramified' then
        # M results from the matrix series M[7] by a ramification
        if j > M[4] then
            # We need to take more terms
            userinfo(5, 'MatTools', `updating ramified terms. Need:`,
                     M[4]+1..j);
            userinfo(5, 'MatTools', `take:`, iquo(M[4]+1, M[6])..
                                           iquo(j, M[6]));
            M[3] :=  Evalm(M[3]+ subs(x = M[5]*x^M[6],
                           mat_eval(M[7], x, iquo(M[4]+1, M[6]), iquo(j, M[6]))), x);
            # updating order. It will be always congruent 0 (modulo r).
            M[4] := iquo(j, M[6])*M[6]+M[6]-1;
        fi;
        map(truncate, M[3], x, i, j);

    elif M[1] = 'substituted' then
        # M results from the matrix series M[7] by a substitution
        if j > M[4] then
            userinfo(5, 'MatTools', `updating terms after substitution`);
            M[3] := evalm(M[3] + map(collect,
                                     subs(M[5], mat_eval(M[7], x, M[4]+1, j)), #label
                                     x, evala));
            M[4] := j;
        fi;
        map(truncate, M[3], x, i, j)

    elif M[1] = 'block_reduced' then
       userinfo(5, 'MatTools',`lets see 1 ->j, i =`, j, i);
        # M results from the matrix series M[19] by a block reduction
        eval_block_reduced(M, x, j);
        userinfo(5, 'MatTools',`lets see 1 ->M[3]=`, M[3]);
        map(evala,map(truncate, matrix_series(M[3],x,j), x, i, j));

    elif M[1] = 'swapped' then
    	# M results from swapping row and column i with j of A
	            userinfo(5, 'MatTools', `evaluating terms:`, i..j);
	            userinfo(5, 'MatTools', `computed order is`, M[4]);
	            if j > M[4] then
	                # desired order j exceeds already computed order M[4]
	                C := mat_eval(M[6], x, M[4]+1, j);
	                userinfo(5, 'MatTools', `updating swapped matrix`);
                    C := linalg['swapcol'](C,M[5][1],M[5][2]);
                    C := linalg['swaprow'](C,M[5][1],M[5][2]);
                    M[3] := Evalm(M[3]+C, x);
	                M[4] := j
	            fi;
        map(truncate, M[3], x, i, j);#, x, i, j)

    elif M[1] = 'multiplied' then
    	d:=val(M[5],x,0);
    	 # M results from the matrix series M[6] by multiplication
	        if j > M[4] then
	            # We need to take more terms
	            userinfo(5, 'MatTools', `updating multiplied terms. Need:`,
	                     M[4]+1..j);
	            userinfo(5, 'MatTools', `take:`, M[4]+1-d..
	                                           j-d);
	            M[3] :=  Evalm(M[3]+ evalm(M[5]*mat_eval(M[6], x, M[4]+1-d,j-d)), x);
	            M[4] := j;
	        fi;
        map(truncate, M[3], x, i, j);
    fi;
end:

# mat_get_dim(K)
#  INPUT:  K - a key of a matrix series M in internal representation
#  OUTPUT: the dimension of the represented square matrix series
#
mat_get_dim := proc(K)
	local M;
	global Mat_Description;

	M := Mat_Description[K];
	if M[1] = 'transformed' then
		linalg['rowdim'](M[4])
	elif M[1] = 'pseudo_transformed' then
		linalg['rowdim'](M[4])
	elif M[1] = 'changed_exp_part' then
		mat_get_dim(M[7])
	elif M[1] = 'term_transformed' then
		mat_get_dim(M[7])
	elif M[1] = 'copied' then
		mat_get_dim(M[2])
	elif M[1] = 'substituted' then
		mat_get_dim(M[7])
	elif M[1] = 'ramified' then
		mat_get_dim(M[7])
	elif M[1] = 'splitted' then
		linalg['rowdim'](M[3])
	elif M[1] = 'localized' then
		linalg['rowdim'](M[6])
	elif M[1] = 'block_reduced' then
        M[5]
	elif M[1] = 'direct_block_reduced' then
        M[7]
	elif M[1] = 'swapped' then
        mat_get_dim(M[6])
	elif M[1] = 'elementary_trans' then
        mat_get_dim(M[7])
	elif M[1] = 'multiplied' then
        mat_get_dim(M[6])
	else
		ERROR("Unknown symbol:", M[1])
	fi
end:

# mat_get_exp_part(K, x)
#  INPUT:  K - a key of a matrix series M in internal representation
#          x - a name
#  OUTPUT: the exponential part of this series
#
mat_get_exp_part := proc(K, x)
    local M;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_get_eigenvalue(args))
    fi;
    M := Mat_Description[K];
    if M[1] = 'transformed' then
        mat_get_exp_part(M[9], x)

    elif M[1] = 'pseudo_transformed' then
        mat_get_exp_part(M[9], x)

    elif M[1] = 'changed_exp_part' then
        collect(expand(mat_get_exp_part(M[7], x)+
                       M[5]/diff(mat_get_ramification(K, x), x)), x, evala)

    elif M[1] = 'term_transformed' then
        collect(expand(mat_get_exp_part(M[7], x))+ M[5], x, evala)

    elif M[1] = 'copied' then
        mat_get_exp_part(M[2], x)

    elif M[1] = 'substituted' then
        collect(subs(M[5], mat_get_exp_part(M[7], x)), x, evala)

    elif M[1] = 'ramified' then
        subs(x = M[5]*x^M[6], mat_get_exp_part(M[7], x))

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            mat_get_exp_part(M[14], x)
        else
            mat_get_exp_part(M[24], x)
        fi

    elif M[1] = 'localized' then 0

    elif M[1] = 'simple' then
        mat_get_exp_part(M[6], x)
	elif M[1] = 'swapped' then
		mat_get_exp_part(M[6], x);
    elif M[1] = 'multiplied' then
    	mat_get_exp_part(M[6], x);
    fi
end:

# mat_get_ext(M)
#  INPUT:  M - a matrix series in internal representation
#  OUTPUT: the algebraic extension over the rationals necessary for the series
#
mat_get_ext := proc(K)
    local M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'transformed' then
        mat_get_ext(M[9])

    elif M[1] = 'pseudo_transformed' then
        mat_get_ext(M[9])

    elif M[1] = 'changed_exp_part' then
        M[6]

    elif M[1] = 'term_transformed' then
        M[6]

    elif M[1] = 'copied' then
        mat_get_ext(M[2])

    elif M[1] = 'substituted' then
        M[6]

    elif M[1] = 'ramified' then
        M[8]

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            mat_get_ext(M[14])
        else
            mat_get_ext(M[24])
        fi

    elif M[1] = 'localized' then
        M[7]

    elif M[1] = 'simple' then
        mat_get_ext(M[6])
    elif M[1] = 'swapped' then
    	mat_get_ext(M[6]);
    elif M[1] = 'elementary_trans' then
    	mat_get_ext(M[6]);
    elif M[1] = 'multiplied' then
    	mat_get_ext(M[6]);;
    fi
end:

# mat_get_inv_transformation(K, x)
#  INPUT:  K - a key of a matrix series M in internal representation
#          x - name
#  OUTPUT: the inverse of the overall transformation matrix
#
mat_get_inv_transformation := proc(K, x)
    local n, m, T, M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'transformed' then
        T := mat_get_inv_transformation(M[9], x);
        n := linalg['rowdim'](T);
        mult(linalg['diag'](Id(n-linalg['rowdim'](M[5])), M[5]), T)

    elif M[1] = 'pseudo_transformed' then
            T := mat_get_inv_transformation(M[9], x);
            n := linalg['rowdim'](T);
            mult(linalg['diag'](Id(n-linalg['rowdim'](M[5])), M[5]), T)

    elif M[1] = 'changed_exp_part' then
        mat_get_inv_transformation(M[7], x)

    elif M[1] = 'copied' then
        mat_get_inv_transformation(M[2], x)

    elif M[1] = 'substituted' then
        map(collect, subs(M[5], mat_get_inv_transformation(M[7], x)), x)

    elif M[1] = 'ramified' then
        subs(x = M[5]*x^M[6], eval(mat_get_inv_transformation(M[7], x)))

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            T := mat_get_inv_transformation(M[14], x);
            n := mat_get_dim(M[14]);
            m := M[5];
            map(collect, mult(linalg['diag'](Id(linalg['rowdim'](T)-n),
                linalg['copyinto'](evalm(-M[11]), Id(n), 1, n-m+1)), T), x)
        else
            T := mat_get_inv_transformation(M[24], x);
            n := mat_get_dim(M[24]);
            m := M[5];
            map(collect, mult(linalg['diag'](Id(linalg['rowdim'](T)-n),
                linalg['copyinto'](evalm(-M[17]), Id(n), 1, n-m+1)), T), x)
        fi

    elif M[1] = 'localized' then
        Id(mat_get_dim(K))

    elif M[1] = 'simple' then
        mult(mat_get_inv_transformation(M[6], x), M[3])

    elif M[1] = 'swapped' then
    	mat_get_inv_transformation(M[6], x)
    fi
end:

# mat_get_point(K)
#  INPUT:  K - a key of a matrix series M in internal representation
#  OUTPUT: the point of localisation
#
mat_get_point := proc(K)
    local M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'transformed' then
        mat_get_point(M[9])

    elif M[1] = 'pseudo_transformed' then
        mat_get_point(M[9])

    elif M[1] = 'changed_exp_part' then
        mat_get_point(M[7])

    elif M[1] = 'copied' then
        mat_get_point(M[2])

    elif M[1] = 'substituted' then
        mat_get_point(M[7])

    elif M[1] = 'ramified' then
        mat_get_point(M[7])

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            mat_get_point(M[14])
        else
            mat_get_point(M[24])
        fi

    elif M[1] = 'localized' then
        M[5]

    elif M[1] = 'simple' then
        mat_get_point(M[6])
    elif M[1] = 'multiplied' then
        mat_get_point(M[6])
    elif M[1] = 'swapped' then
    	mat_get_point(M[6])
    fi;
end:

# mat_get_ramification(K, x)
#  INPUT:  K - a key of a matrix series M in internal representation
#          x - a name
#  OUTPUT: the ramification of M in the form c*x^r
#
mat_get_ramification := proc(K, x)
    local M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'transformed' then
        mat_get_ramification(M[9], x)

    elif M[1] = 'pseudo_transformed' then
        mat_get_ramification(M[9], x)

    elif M[1] = 'changed_exp_part' then
        mat_get_ramification(M[7], x)

    elif M[1] = 'term_transformed' then
        mat_get_ramification(M[7], x)

    elif M[1] = 'copied' then
        mat_get_ramification(M[2], x)

    elif M[1] = 'substituted' then
        evala(subs(M[5], mat_get_ramification(M[7], x)))

    elif M[1] = 'ramified' then
        subs(x = M[5]*x^M[6], mat_get_ramification(M[7], x))

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            mat_get_ramification(M[14], x)
        else
            mat_get_ramification(M[24], x)
        fi

    elif M[1] = 'localized' then x

    elif M[1] = 'simple' then
        mat_get_ramification(M[6], x)
    elif M[1] = 'swapped' then
		mat_get_ramification(M[6], x)
    elif M[1] = 'multiplied' then
        mat_get_ramification(M[6], x)
    fi

end:

# mat_get_transformation(K, x)
#  INPUT:  K - a key of a matrix series M in internal representation
#          x - name
#  OUTPUT: the overall transformation matrix
#
mat_get_transformation := proc(K, x)
    local n, m, T, M;
    global Mat_Description;

    M := Mat_Description[K];
    if M[1] = 'transformed' then
        T := mat_get_transformation(M[9], x);
        n := linalg['rowdim'](T);
        mult(T, linalg['diag'](Id(n-linalg['rowdim'](M[4])), M[4]))

    elif M[1] = 'pseudo_transformed' then
            T := mat_get_transformation(M[9], x);
            n := linalg['rowdim'](T);
            mult(T, linalg['diag'](Id(n-linalg['rowdim'](M[4])), M[4]))

    elif M[1] = 'changed_exp_part' then
        mat_get_transformation(M[7], x)

    elif M[1] = 'copied' then
        mat_get_transformation(M[2], x)

    elif M[1] = 'substituted' then
        map(collect, subs(M[5], mat_get_transformation(M[7], x)), x)

    elif M[1] = 'ramified' then
        subs(x = M[5]*x^M[6], eval(mat_get_transformation(M[7], x)))

    elif M[1] = 'block_reduced' then
        if nops(M[6]) = 1 then
            T := mat_get_transformation(M[14], x);
            n := mat_get_dim(M[14]);
            m := M[5];
            map(collect, mult(T, linalg['diag'](Id(linalg['rowdim'](T)-n),
                         linalg['copyinto'](M[11], Id(n), 1, n-m+1))), x)
        else
            T := mat_get_transformation(M[24], x);
            n := mat_get_dim(M[24]);
            m := M[5];
            map(collect, mult(T, linalg['diag'](Id(linalg['rowdim'](T)-n),
                         linalg['copyinto'](M[17], Id(n), 1, n-m+1))), x)
        fi

    elif M[1] = 'localized' then
        Id(mat_get_dim(K))

    elif M[1] = 'simple' then
        mult(mat_get_transformation(M[6], x), M[3])

    elif M[1] = 'swapped' then
    	mat_get_transformation(M[6], x)
    fi
end:


# mat_pseudo_transform(K, x, T, invT, sigma, phi, delta, omega)
#  INPUT:  K     - the key of a matrix series M in internal representation
#          x     - a name
#          T     - a polynomial matrix
#          invT  - the inverse of T which must be polynomial in x and 1/x
#          sigma - an integer (the span of T)
#          phi   - the automorhphism represented as a proceduce
#  OUTPUT: M will be assigned to the internal representation of the series
#          T[M]. This procedure is destructing the old value of M, if M
#          has already the type 'transformed'.
#          We represent the result in an array N with 9 elements:
#
#      N[1] is the symbol 'transformed',
#      N[2] is a integer: the valuation of the matrix series,
#      N[3] contains terms of the transformation of M, where terms of M
#           have been transformed up to order M[7] (included).
#      N[4] is the transformation matrix T
#      N[5] is the inverse of T
#      N[6] is the span of T
#      N[7] is the order up to which terms in M[3] are exact terms of the
#           series expansion of T[M]
#      N[8] is the algebraic extension needed for the coefficients of the
#           matrix series
#      N[9] is the matrix series M
#
#
mat_pseudo_transform := proc(K, x, T, invT1, sigma1, phi1, delta1, omega1)
    local M, N, C, ext, phi, delta, omega, invT, sigma, phiproc;
    global Mat_Description, Default_delta, Default_phi, Default_omega;
    #print(args);
    if nargs < 6 then
        phiproc:=Default_phi;
        phi:= Default_phi(x,x);
        delta:= Default_delta;
        omega := Default_omega;
    else
        phiproc:=args[-3];
        phi:= args[-3](x,x);
        delta:= args[-2];
        omega := args[-1];
    fi;

    if nargs < 5 or nargs = 6 then
            invT := evalm(T^(-1));
            sigma := -(mat_val(T, x, 0)+ mat_val(invT, x,0));
    fi;

    if nargs = 5 or nargs = 8 then
            invT := invT1;
            sigma := sigma1;
    fi;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_transform_algebraic(K,x,T, invT, simga, phiproc, delta, omega))
    fi;

    if nargs = 5 then
        return mat_transform(K, x, T, invT, sigma, Default_phi, Default_delta, Default_omega)
    fi;

    userinfo(5, 'MatTools', `pseudo transforming system with phi, delta, omega`, phi, delta, omega);

    M := Mat_Description[K];
    ext := mat_get_ext(K) union
           indets(convert(T, 'set'), 'RootOf') union
           indets(convert(invT, 'set'), 'RootOf');

    if M[1] <> 'pseudo_transformed' then
        M := array(1..12);
        M[1] := 'pseudo_transformed';
        M[2] := 'NN';
        userinfo(5, 'MatTools', `mat_get_terms`);
        M[3] := mat_get_terms(K, x); # transform stored terms
        userinfo(5, 'MatTools', `pseudo transforming system`);
        C := map(lc_evala,mult(invT, evalm(mult(M[3], subs(x=phi,evalm(T))) - map(delta,T,x))),x,ext);

        userinfo(5, 'MatTools', ` ->reducing algebraic numbers`);
        M[3] := map(radnormal, evalm(C));
        M[4] := evalm(T);
        M[5] := evalm(1/T);
        M[6] := sigma;
        M[7] := mat_get_order(K, x)-sigma;
        M[8] := ext;
        M[9] := K;
       # if nargs = 8 then
        M[10] := phiproc;
        M[11] := delta;
        M[12] := omega;
        #fi;
        N := new_matrix();
        #print(ext,N,M);
        Mat_Description[N] := M;
        N
    else
		# Flavia: attention, we should also check whether phi and delta are the same, otherwise this will be incorrect
        # new valuation not known
        M[2] := 'NN';

        # update span of transformation matrix, and store algebraic extension
        M[6] := M[6] + sigma;
        if sigma > 0 then
             userinfo(5, 'MatTools', `updating span, is now`, M[6])
        fi;
        M[8] := ext;

        # update order info
        M[7] := M[7]-sigma;

        # update transformation matrices
        userinfo(5, 'MatTools', `update transformation matrices`);
        M[4] := map(lc_evala, mult(M[4], T), x, ext);
        M[5] := map(lc_evala, mult(invT, M[5]), x, ext);

        # transform stored terms
        userinfo(5, 'MatTools', `transforming system`);
        C := mult(invT, evalm(mult(M[3], subs(x=phi,evalm(T))) - map(delta,T,x)));
        userinfo(5, 'MatTools', `reducing algebraic numbers`);
        M[3] := map(lc_evala, C, x, ext);
        K
    fi;
end:

# mat_ramification(K, x, c, r)
#   INPUT:  K - the key of a representation of a matrix series M
#           x - a name
#           c - an integer or a name
#           r - a positive integer
#   OUTPUT: N - the matrix series which results from doing the ramification
#           x -> cx^r in the system Y'=MY.
#
#      N[1] is the symbol 'ramified',
#      N[2] is a integer: the valuation of the resulting matrix series,
#      N[3] contains terms of the new matrix series up to order N[4]
#           included
#      N[4] is the order up to which terms in N[3] are computed
#      N[5] is c
#      N[6] is the ramification r
#      N[7] is the matrix series M
#      N[8] is the algebraic extension
#      N[9] is the automorphism phi
#      N[10] is the pseudo derivative delta
#      N[11] is an integer, omega
#
mat_ramification := proc(K, x, c, r)
    local N, ext, k, KK, p, LL; # phi1, delta1, omega1, phiproc;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_ramification_algebraic(args))
    fi;
    N := array(1..8);
    N[1] := 'ramified';
    N[5] := c;
    N[6] := r;
    N[7] := K;
    ext := mat_get_ext(K);
    N[8] := ext union indets(c, 'RootOf');
    k := mat_get_order(K, x);

    N[2] := r*mat_valuation(K, x)+r-1;
    N[3] := Evalm(c*r*x^(r-1)*subs(x = c*x^r, mat_get_terms(K, x)), x);
    N[4] := (k+2)*r-2;

	## This will be the new version
    #N[2] := r*mat_valuation(K, x);
    ##print("mat_ramification new", Mat_Description[K]);
    #N[3] := Evalm(subs(x = c*x^r, mat_get_terms(K, x)),x);
    #N[4] := k*r+r-1;

    KK := new_matrix();
    userinfo(4, 'MatTools', `leaving ramification`);
    Mat_Description[KK] := N;
    KK
end:

# mat_scalar_mult(A, x, f)
#   INPUT:  A - the key of a representation of a matrix series M
#           x - a name
#	    f - a polynomial in x
#   OUTPUT: N - the matrix series which results from the scalar multiplication
#		of the matrix A with the polynomial f.
#
#      N[1] is the symbol 'multiplied',
#      N[2] is a integer: the valuation of the resulting matrix series,
#      N[3] contains terms of the new matrix series up to order N[4]
#           included
#      N[4] is the order up to which terms in N[3] are computed
#      N[5] the polynomial f
#      N[5] is the matrix series M
#
mat_scalar_mult := proc(K, x, f)
    local N, ext, k, KK, p, LL, d; #phi1, delta1, omega1, phiproc;
    global Mat_Description;

    N := array(1..6);

    d:=val(f,x,0);

    N[5] := f;

    N[1] := 'multiplied';
    N[6] := K;

    k := mat_get_order(K, x);

    N[2] := mat_valuation(K, x)+d;

    N[3] := map(expand,evalm(f*mat_get_terms(K, x)));
    N[4] := k+d;

    KK := new_matrix();
    Mat_Description[KK] := N;
    KK
end:

# mat_swap(A, x, i, j)
#
# exchanges the ith and jth row and column of the matrix series A
#  INPUT:  A - a matrix series in internal representation
#	   x - a name
#          i - an integer
#          j - an integer
#  OUTPUT: The swapped matrix with the in internal representation
#	   B[1] is the symbol 'swapped'
#	   B[2] is the valuation of B or the symbol 'NN'
#	   B[3] is the truncated power series
# 	   B[4] is the order of B
#	   B[5] is a list containing i, j
#	   B[6] is the local matrix series A
#
mat_swap := proc(A, x, i, j)
	global Mat_Description, Valuation_Bound;
	local M, B, v, h, Bt, BB;

	if type(Valuation_Bound, 'name') then Valuation_Bound := 100 fi;
	M:=array(1..6);
	B[1] := 'swapped';
	v := mat_valuation(A,x);
	h := mat_get_order(A,x);
	Bt := mat_eval(A,x,v,h);

	B[2] := v;
	B[3] := Bt;
	B[4] := h;
	B[5] := [i,j];
	B[6] := A;

	BB := new_matrix();
	Mat_Description[BB]:= B;


	Bt := linalg['swapcol'](Bt,i,j);
	Bt := linalg['swaprow'](Bt,i,j);
	M[3] := Bt;

	RETURN(BB);
end:

# mat_transform(K, x, T, invT, sigma)
#  INPUT:  K     - the key of a matrix series M in internal representation
#          x     - a name
#          T     - a polynomial matrix
#          invT  - the inverse of T which must be polynomial in x and 1/x
#          sigma - an integer (the span of T)
#  OUTPUT: M will be assigned to the internal representation of the series
#          T[M]. This procedure is destructing the old value of M, if M
#          has already the type 'transformed'.
#          We represent the result in an array N with 9 elements:
#
#      N[1] is the symbol 'transformed',
#      N[2] is a integer: the valuation of the matrix series,
#      N[3] contains terms of the transformation of M, where terms of M
#           have been transformed up to order M[7] (included).
#      N[4] is the transformation matrix T
#      N[5] is the inverse of T
#      N[6] is the span of T
#      N[7] is the order up to which terms in M[3] are exact terms of the
#           series expansion of T[M]
#      N[8] is the algebraic extension needed for the coefficients of the
#           matrix series
#      N[9] is the matrix series M
#	   N[10] phi
#	   N[11] delta
#	   N[12] omega
#
mat_transform := proc(K, x, T, invT, sigma, phi, delta, omega)
    local M, N, C, ext;
    global Mat_Description;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_transform_algebraic(args))
    fi;
    M := Mat_Description[K];
    ext := mat_get_ext(M) union
           indets(convert(T, 'set'), 'RootOf') union
           indets(convert(invT, 'set'), 'RootOf');
    if M[1] <> 'transformed' then
        M := array(1..12);
        M[1] := 'transformed';
        M[2] := 'NN';
        M[3] := mat_get_terms(K, x);
        # transform stored terms
        userinfo(5, 'MatTools', `transforming system`);
        C := mult(invT, evalm(mult(M[3], T) - map(diff, T, x)));
        userinfo(5, 'MatTools', `reducing algebraic numbers`);
        M[3] := map(lc_evala, C, x, ext);
        M[4] := evalm(T);
        M[5] := evalm(1/T);
        M[6] := sigma;
        M[7] := mat_get_order(K, x)-sigma;
        M[8] := ext;
        M[9] := K;
        if nargs = 8 then
        	M[10] := phi;
        	M[11] := delta;
			M[12] := omega;
        fi;

        N := new_matrix();
        Mat_Description[N] := M;
        N
    else
        # new valuation not known
        M[2] := 'NN';

        # update span of transformation matrix, and store algebraic extension
        M[6] := M[6] + sigma;
        if sigma > 0 then
             userinfo(5, 'MatTools', `updating span, is now`, M[6])
        fi;
        M[8] := ext;

        # update order info
        M[7] := M[7]-sigma;

        # update transformation matrices
        userinfo(5, 'MatTools', `update transformation matrices`);
        M[4] := map(lc_evala, mult(M[4], T), x, ext);
        M[5] := map(lc_evala, mult(invT, M[5]), x, ext);

        # transform stored terms
        userinfo(5, 'MatTools', `transforming system`);
        C := mult(invT, evalm(mult(M[3], T) - map(diff, T, x)));
        userinfo(5, 'MatTools', `reducing algebraic numbers`);
        M[3] := map(lc_evala, C, x, ext);
        K
    fi;
end:

# mat_valuation(K, x, bound)
#  INPUT:  K     - a key for a matrix series M in internal representation
#          x     - a name
#          bound - (optional) an integer
#  OUTPUT: the inimum of bound and the valuation of the matrix series M
#
mat_valuation := proc(K, x, bound)
    local M, v, C, sigma, ext, b, va, vb;
    global Mat_Description, Valuation_Bound;

    if mat_get_type(K) = 'algebraic' then
        RETURN(mat_valuation_algebraic(args))
    fi;

    if not type(Valuation_Bound, 'integer') then Valuation_Bound := 20 fi;
    if nargs = 3 then b := bound else b := Valuation_Bound fi;
    M := Mat_Description[K];
    if type(M[2], 'integer') or M[2] = infinity then M[2]
    else
        ext := mat_get_ext(K, x);
        if M[1] = 'transformed' then
            # compute valuation of so far computed terms
            userinfo(5, 'MatTools', `recomputing valuation`);
            sigma := M[6];
            v := min(op(map(val, convert(M[3], 'set'), x, 0)));
            # if v is greater than actual order, we have to increase it
            while v > M[7] and v < b do
                # transform successively new terms until precision is ok
                C := mat_eval(M[9], x, M[7]+sigma+1, v+sigma);
                M[3] := map(lc_evala, evalm(M[3]+mult(M[5], mult(C, M[4]))),
                                      x, M[8]);
                # update new order
                M[7] := v;
                v := min(op(map(val_ldegree, convert(M[3], 'set'), x)));
            od;
            # store valuation
            if nargs = 3 and v >= b then M[2] := 'NN' else M[2] := v fi;
            v

	elif M[1] = 'sum' then
		#compute valuation of currently computed terms
		userinfo(5, 'MatTools', `recomputing valuation`);
		if not linalg['iszero'](M[3]) then
			v := min(op(map(val_ldegree, convert(M[3], 'set'), x)));
		else
			v := M[4];
			while (v <= Valuation_Bound) and (linalg['iszero'](M[3])) do
				v := v +1;
				M[3] := Evalm(M[3] + mat_eval(M[6],x,v,v) + mat_eval(M[7],x,v,v),x);
				userinfo(5, 'MatTools', `checking valuation`,v);
			od;
		fi;
		M[2] := v;
		M[4] := v;
		v

	elif M[1] = 'difference' then
		#compute valuation of currently computed terms
		userinfo(5, 'MatTools', `recomputing valuation`);
		if not linalg['iszero'](M[3]) then
			v := min(op(map(val_ldegree, convert(M[3], 'set'), x)));
		else
			v := M[4];
			while (v <= Valuation_Bound) and (linalg['iszero'](M[3])) do
				v := v +1;
				M[3] := Evalm(M[3] + mat_eval(M[6],x,v,v) - mat_eval(M[7],x,v,v),x);
				userinfo(5, 'MatTools', `checking valuation`,v);
			od;
		fi;
		M[2] := v;
		M[4] := v;
		v

	elif M[1] = 'product' then
			#compute valuation of currently computed terms
			userinfo(5, 'MatTools', `recomputing valuation`);
			va := mat_valuation(M[6],x);
			vb := mat_valuation(M[7],x);
			if not linalg['iszero'](map(truncate, M[3], x, va+vb, M[4])) then
				v := min(op(map(val_ldegree, convert(M[3], 'set'), x)));
			else
				v := M[4];
				while (v <= Valuation_Bound) and (linalg['iszero'](map(truncate, M[3], x, va+vb, v))) do
						v := v + 1;
						M[5] := M[5] + 1; #increment lc
						M[3] := Evalm(M[3] +
							mult(mat_eval(M[6],x,v-vb,v-vb), mat_eval(M[7],x,vb,v-1-va)) +
							mult(mat_eval(M[6],x,va,v-1-vb), mat_eval(M[7],x,v-va,v-va)) +
							mult(mat_eval(M[6],x,v-vb,v-vb), mat_eval(M[7],x,v-va,v-va)),x) ;
						userinfo(5, 'MatTools', `M[3]=`,M[3]);
						userinfo(5, 'MatTools', `checking valuation`,v);
				od;
				M[4] := v;
			fi;
			M[2] := v;
		v

	elif M[1] = 'apply' then
			#compute valuation of currently computed terms
			userinfo(5, 'MatTools', `recomputing valuation`);
			if not linalg['iszero'](M[3]) then
				v := min(op(map(val_ldegree, convert(M[3], 'set'), x)));
			else
				v := M[4];
				while (v <= Valuation_Bound) and (linalg['iszero'](M[3])) do
					v := v +1;
					M[3] := Evalm(M[3] + mat_eval(M[5],x,v,v) + mat_eval(M[6],x,v,v),x);
					userinfo(5, 'MatTools', `checking valuation`,v);
				od;
			fi;
			M[2] := v;
			M[4] := v;
		v

	elif M[1] = 'pseudo_transformed' then
	            # compute valuation of so far computed terms
	            userinfo(5, 'MatTools', `recomputing valuation`);
	            sigma := M[6];
	            v := min(op(map(val, convert(M[3], 'set'), x, 0)));
	            # if v is greater than actual order, we have to increase it
	            while v > M[7] and v < b do
	                # transform successively new terms until precision is ok
                    C := mat_eval(M[9], x, M[7]+sigma+1, v+sigma);
	                M[3] := map(lc_evala,
                                evalm(M[3]+mult(M[5], mult(C, M[4]))), x, M[8]);
                    # update new order
	                M[7] := v;
	                v := min(op(map(val, convert(M[3], 'set'), x, 0)));
	            od;
	            # store valuation
	            if nargs = 3 and v >= b then M[2] := 'NN' else M[2] := v fi;
            v

        elif M[1] = 'localized' then
            ERROR(`undetermined valuation of localized matrix`)

        elif M[1] = 'changed_exp_part' then
            # compute valuation of so far computed terms
            userinfo(5, 'MatTools', `recomputing valuation`);
            v := min(op(map(generic_val, convert(M[3], 'set'), x, 0, mat_valuation(M[7],x))));
            # if v is greater than actual order, we have to increase it
            while v > M[4] do
                # get successively new terms until precision is ok
                if v = infinity then v := M[4]+1 fi;
                M[3] := map(lc_evala, evalm(M[3]+
                                mat_eval(M[7], x, M[4]+1, v)),
                                      x, ext);
                # update new order
                M[4] := v;
                v := min(op(map(generic_val, convert(M[3], 'set'), x, 0 ,mat_valuation(M[7],x))));
                if M[4] > b then break fi;
            od;
            # store valuation
            if nargs = 3 and v >= b then M[2] := 'NN' else M[2] := v fi;
            min(v, b)

		elif M[1] = 'term_transformed' then
            # compute valuation of so far computed terms
            userinfo(5, 'isolde', `recomputing valuation`);
            v := min(op(map(generic_val, convert(M[3], 'set'), x, 0, mat_valuation(M[7],x))));
            # if v is greater than actual order, we have to increase it
            while v > M[4] do
                # get successively new terms until precision is ok
                if v = infinity then v := M[4]+1 fi;
                M[3] := map(lc_evala, evalm(M[3]+
                                M[8][1]*x^M[8][2]*mat_eval(M[7], x, M[4]+1, v)),
                                      x, ext);
				userinfo(5, 'MatTools', `new terms`, M[8][1]*x^M[8][2]*mat_eval(M[7], x, M[4]+1, v));
                # update new order
                M[4] := v;
                v := min(op(map(generic_val, convert(M[3], 'set'), x, 0 ,mat_valuation(M[7],x))));
                if M[4] > b then break fi;
            od;
            # store valuation
            if nargs = 3 and v >= b then M[2] := 'NN' else M[2] := v fi;
            min(v, b)

        elif M[1] = 'copied' then
           mat_valuation(M[2], x, b);#, phi, delta)

        elif M[1] = 'ramified' then
            ERROR(`undefined valuation for ramified matrix`)

        elif M[1] = 'substituted' then
             ERROR(`undefined valuation for substituted matrix`)

        elif M[1] = 'block_reduced' then
            # compute valuation of so far computed terms
            userinfo(5, 'MatTools', `recomputing valuation`);
            v := M[4];
            do
                if v < 0 then v := iquo(v, 2) else v := v+1 fi;
                userinfo(5, 'MatTools',`lets see 2`);
                eval_block_reduced(M, x, v);
                if not linalg['iszero'](M[3]) or v >= b then break fi;
            od;
            v := min(b, op(map(val, convert(M[3], 'set'), x, 0)));
            # store valuation
            if nargs = 3 and v >= b then M[2] := 'NN' else M[2] := v fi;
            v
        else
			ERROR("valuation of unknown data structure");
		fi
    fi
end:
