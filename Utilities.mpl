# lc_evala(f, x, ext, b)
# Goal: compute the valuation and leading coeff of f. (This might be an
# algebraic number). Then we need to symplify this leading coeff,
# using evala. If after symplification we obtain zero, we need to get
# the next coefficient (of higher order)
#   INPUT:  f   - an expression polynomial in x
#           x   - a name
#           ext - a set of RootOfs describing an algebraic extension
#           b   - (optional) an integer
#   OUTPUT: the expression f with normalized leading coefficient
#           if the integer b is passed as argument, all terms of lower degree
#           than b are supposed to reduce to zero.
#
lc_evala := proc(f, x, ext, b)
    local g, d, dd, lc, i;

    if (nargs > 2) and (ext = {}) then return f fi;
    RETURN(evala(f));## TODO temporary solution

    g := collect(expand(f), x, normal);
    if nargs = 2 or ext = {} or f = 0 then
        if nargs < 4 then
            RETURN(g)
        else
            RETURN(truncate(g, x, b, degree(g, x)))
        fi
    fi;
    if nargs = 4 then d := b else d := ldegree(g, x) fi;
    dd := degree(g, x);
    do
        lc := evala(Expand(coeff(g, x, d)));
        if lc <> 0 or d > dd or (nargs = 4 and d >= b) then
            #print(g, d);
            RETURN(lc*x^d+convert([seq(coeff(g, x, i)*x^i,
                               i = d+1..degree(g, x))], `+`))
        fi;
       	d := d+1;
    od
end:

# lc_series(f, x, ext, b)
#   INPUT:  f   - an expression polynomial in x
#           x   - a name
#           ext - a set of RootOfs describing an algebraic extension
#           b   - (optional) an integer
#   OUTPUT: the expression f with normalized leading coefficient
#           if the integer b is passed as argument, all terms of lower degree
#           than b are supposed to reduce to zero.
#
lc_series := proc(f, x, ext, b)
    local g, d, dd, lc, i;

    if (nargs > 2) and (ext = {}) then return f fi;
    g := collect(expand(f), x, normal);
    if nargs = 2 or ext = {} or f = 0 then
        if nargs < 4 then
            RETURN(g)
        else
            RETURN(truncate(g, x, b, degree(g, x)))
        fi
    fi;
    if nargs = 4 then d := b else d := ldegree(g, x) fi;
    dd := degree(g, x);
    do
       	lc:= evala(limit(evala(g/x),x=0));
       	if lc <> 0 or d > dd or (nargs = 4 and d >= b) then
		RETURN(lc*x^d+convert([seq(coeff(g, x, i)*x^i, i = d+1..degree(g, x))], `+`))
        fi;
       	ERROR('RootOf');
       	d := d+1;
    od
end:

# seq_evala(f, x, ext, i, j)
#   INPUT:  f   - an expression polynomial in x
#           x   - a name
#           ext - a set of RootOfs describing an algebraic extension
#           i   - an integer
#           j   - an integer
#   OUTPUT: the expression f with evala applied to terms of order i to j
#
seq_evala := proc(f, x, ext, i, j)
    local g;

    if indets(ext, 'RootOf') = {} then RETURN(f) fi;
    RETURN(evala(f)); #TODO temporary solution
    g := collect(expand(f), x);
    convert([seq(coeff(g, x, h)*x^h, h = ldegree(g, x)..i-1),
             seq(evala(coeff(g, x, h))*x^h, h = i..j),
             seq(coeff(g, x, h)*x^h, h = j+1..degree(g, x))], `+`)
end:


# truncate(f, x, i, j)
#   INPUT:  f - an expression polynomial in x or 1/x
#           x - a name
#           i - an integer
#           j - (optional) an integer
#   OUTPUT: the truncated expression f mod x^(j+1) - f mod x^(i-1).
#           if j is not passed as argument, truncation goes up to degree(f,x).
#
truncate := proc(f, x, i, j)
    local k;

    if f = 0 then 0
    else
        if nargs = 4 then
             convert([seq(coeff(map(normal,f), x, k)*x^k, k = i..j)], `+`);
        else
            convert([seq(coeff(map(normal,f), x, k)*x^k, k = i..degree(f, x))], `+`)
        fi
    fi
end:

# matrix_series(M, x, j)
#   INPUT:  M  - a rational function matrix
#           x  - a name
#           j - (optional) an integer
#   OUTPUT: the series
#
matrix_series := proc(M, x, j)
    local N;

    if linalg['iszero'](M) then
        evalm(M);
    else
        N := map(proc(a, x, jj)
                 local v;
                     v := val(a,x,0);
                     if jj-v+1 <= 0 then 0
                     else
                         expand(x^v * convert(series(normal(a*x^(-v)), x, jj-v+1), 'polynom'))
                     fi;
                 end, M, x, j);
    	evalm(N);
    fi;
end:

# mult(A, B)
#   INPUT:  A - a matrix
#           B - a matrix
#   OUTPUT: the matrix product of A and B. This procedure seems to be more
#           efficient than Maple's evalm(A &* B) if A or B contain RootOfs.
#
mult := proc(A, B)
   local i, j, l, C, m, n, k;

   n := linalg['rowdim'](A);
   m := linalg['coldim'](A);
   k := linalg['coldim'](B);
   if linalg['rowdim'](B) <> m then
       ERROR(`wrong dimensions in matrix product`)
   fi;
   C := linalg[matrix](n, k);
   for i from 1 to n do
       for j from 1 to k do
           C[i, j] := convert([seq(A[i, l]*B[l, j], l=1..m)], `+`);
       od
   od;
   evalm(C)
end:

# my_linsolve(A, b, c, clist)
#   INPUT:  A     - a matrix
#           b     - a vector or a list
#           c     - a name
#           clist - a list of indexed names of the form c[i]
#   OUTPUT: The parametrized solution space of the linear system AX = b.
#           New occuring parameters are replaced by c[Index+1],..where
#           Index is global
#
my_linsolve := proc(A, b, c, clist)
    local res, dummy, C, dim, i;
    global Index;

    res := linalg['linsolve'](A, b, 'dummy', C);
    if res = NULL then RETURN(NULL) fi;
    res := convert(res, 'list');
    dim := nops(select(has, indets(res), C));
    res := subs(seq(C[i]=c[Index+i], i=1..dim), res);
    Index := Index+dim;
    [evalm(res), [op(clist), seq(c[i], i=Index-dim+1..Index)]]
end:

# ************************************************************************* #
#               functions computing valuations                              #
# ************************************************************************* #

# val_ldegree(f, x)
#   INPUT:  f  - a polynomial in x and 1/x
#           x  - a name
#   OUTPUT: if f = 0, then infinity, otherwise ldegree(f, x)
#
val_ldegree := proc(f, x)

    if f = 0 then infinity else ldegree(f, x) fi
end:

# generic_val(f, x, x0, mv [, lc])
#   INPUT:  f  - a general expression
#           x  - a name
#           x0 - an algebraic number or infinity
#           mv - an integer
#           lc - (optional) a name
#   OUTPUT: The valuation of f at x0 (it is assumed that it is geq to mv).
#           If lc is passed as argument, it is
#           assigned to the leading coefficient of f at x0.
#
generic_val := proc(f, x, x0, mv, lc)
    local i, lcv;

    if type(f, 'ratpoly') then
    	RETURN(val(args[1],args[2],args[3], seq(args[i],i=5..nargs)));
    else
        userinfo(5, 'Utilities', `f=`,f);
    	i := mv;
    	lcv := 0;
    	while lcv = 0 do
    		lcv := evala(limit(f/x^i,x=0,right));
            userinfo(5, 'Utilities', `lcv=`,lcv);
    		i := i+1;
    	od;
    	if nargs = 5 then lc := lcv fi;
    	RETURN(i-1);
    fi;
end:

# generic_series(f, x, x0)
#   INPUT:  f  - a general expression
#           x  - a name
#           x0 - an algebraic number or infinity
#   OUTPUT: The series expansion of f at 0
#
generic_series := proc(f, x, k)
    local i, lc, lcv, s, r;

    if type(f, 'ratpoly') then
    	RETURN(series(f,x=0,k));
    else
    	i:=0;
    	lcv := 0;
    	s := f;
    	r := 0;
    	lc:= evala(limit(f,x=0,right));
    	while i < k do
    		lcv := evala(limit(s/x^i,x=0,right));
    		s := s - lcv*x^i;
    		r := r + lcv*x^i;
    		i := i+1;
    	od;
    	RETURN(r);
    fi;
end:

# val(f, x, x0 [, lc])
#   INPUT:  f  - a a rational function
#           x  - a name
#           x0 - an algebraic number or infinity
#           lc - (optional) a name
#   OUTPUT: the valuation of f at x0. If lc is passed as argument, it is
#           assigned to the leading coefficient of f at x0.
#
val := proc(f, x, x0, lc)
    local v, l1, l2;

    if not type(f,'ratpoly') then
        userinfo(4, 'Utilities', `enter generic_val`);
    	v:=generic_val(f,x,x0,0);
    else
    	if nargs = 3 then
    	    RETURN(poly_val(numer(f), x, x0) - poly_val(denom(f), x, x0))
    	fi;
    	v := poly_val(numer(f), x, x0, 'l1') - poly_val(denom(f), x, x0, 'l2');
    	if nargs = 4 then lc := evala(l1/l2) fi;
    fi;
    v
end:


# pval(f, x, p [, lc])
#   INPUT:  f  - a a rational function
#           x  - a name
#           p  - a polynomial or 1/x
#           lc - (optional) a name
#   OUTPUT: the order of f at p. If lc is passed as argument, it is
#           assigned to the leading coefficient of f at x0.
#
pval := proc(f, x, p, lc)
    local v, l1, l2, U, V;

    #option remember;
    if nargs = 4 then
        v := poly_pval(expand(numer(f)), x, p, 'l1')-
             poly_pval(expand(denom(f)), x, p, 'l2');
    else
        v := poly_pval(expand(numer(f)), x, p)-
             poly_pval(expand(denom(f)), x, p)
    fi;
    if (p = x) or (p = 1/x) or (p = infinity) then
        if nargs = 4 then lc := l1/l2 fi;
        RETURN(v)
    fi;
    if nargs = 4 then
        gcdex(l2, p, x, 'U', 'V');
        lc := rem(l1*U, p, x)
    fi;

    v
end:

# poly_val(a, x, x0 [,lc ])
#   INPUT:  a  - a polynomial
#           x  - a name
#           x0 - an algebraic number or infinity
#           lc - (optional) a name
#   OUTPUT: the valuation of f at x0. If lc is passed as argument, it is
#           assigned to the leading coefficient of f at x0.
#
poly_val := proc(a, x, x0, lc)
    local b, lc1, v;

    if not type(a, 'polynom') then ERROR(`polynomial expected:`, a) fi;
    if a = 0 then
        if nargs = 4 then lc := 0 fi;
        RETURN(infinity)
    elif (x0 = infinity) then
        if nargs = 4 then lc := lcoeff(collect(a, x), x) fi;
        RETURN(-degree(a, x))
    elif x0 = 0 then
        if nargs = 4 then lc := tcoeff(collect(a, x), x) fi;
        RETURN(ldegree(a, x))
    else
        b := subs(x = x+x0, a);
        v := 0;
        lc1 := evala(subs(x = 0, b));
        while lc1 = 0 do
            v := v+1;
            b := diff(b, x)/v;
            lc1 := evala(subs(x = 0, b));
        od;
        if nargs = 4 then lc := lc1 fi;
        v
    fi
end:

# poly_pval(a, x, p [,lc ])
#   INPUT:  a  - a polynomial
#           x  - a name
#           p  - a polynomial or 1/x
#           lc - (optional) a name
#   OUTPUT: the order of a at p
#
poly_pval := proc(a, x, p, lc)
    local a1, ord, temp, q;

    #option remember;
    a1 := collect(a, x);
    if not type(a, 'polynom') then ERROR(`polynomial expected:`, a) fi;
    if a = 0 then
        if nargs = 4 then lc := 0 fi;
        RETURN(infinity)
    fi;
    if (p = 1/x) or (p = infinity) then
        if nargs = 4 then lc := lcoeff(a1, x) fi;
        RETURN(-degree(a, x))
    fi;
    if p = x then
        if nargs = 4 then lc := tcoeff(a1, x) fi;
        RETURN(ldegree(a, x))
    fi;
    ord := 0;
    temp := a;
    if indets(a, 'RootOf') union indets(p, 'RootOf') = {} then
        while divide(temp, p, 'q') do
            temp := diff(temp, x);
            ord := ord+1
        od
    else
        while evala(Divide(temp, p, 'q')) do
            temp := q;
            ord := ord+1
        od
    fi;
    if nargs = 4 then
        if ord = 0 then lc := evala(Rem(a, p, x))
            else lc := evala(Rem(q, p, x)) fi
    fi;
    ord
end:

# col_pval(v, x, p [, lcv])
#   INPUT:  v   - a rational function vector
#           x   - a name
#           p   - a polynomial or infinity
#           lcv - (optional) a name
#   OUTPUT: the order of v at p and the leading vector of v at p
#
col_pval := proc(v, x, p, lcv)
    local n, lc, i, tmp, v1, s;

    v1 := infinity;
    n := linalg[vectdim](v);
    if nargs = 3 then
        RETURN(min(op(map(val, convert(v, 'set'), x, p))))
    fi;
    lcv := linalg[vector](n, 0);
    for i from 1 to n do
        if v[i] <> 0 then
            s := pval(v[i], x, p, 'lc');
            tmp[i] := [s, lc];
            v1 := min(v1, s)
        fi
    od;
    if v1 <> infinity then
        for i from 1 to n do
            if tmp[i][1] = v1 then lcv[i] := tmp[i][2] fi
        od
    fi;
    RETURN(v1)
end:

# mat_val(M, x, x0 [, lcM])
#   INPUT:  M   - a rational function matrix
#           x   - a name
#           x0  - a rational, a RootOf or infinity
#           lcM - (optional) a name
#   OUTPUT: the order of M at p. If lcM is passed as argument, it will be
#           assigned to the leading matrix lcM of M at x0
#
mat_val := proc(M, x, x0, lcM)
    local v, c, cc, n, m, d, i, j, a;

    if nargs = 3 then
        RETURN(min(op(map(generic_val, convert(M, 'set'), x, x0,0))))
    fi;
    m := linalg['rowdim'](M);
    n := linalg['coldim'](M);
    v := infinity;
    cc := NULL;
    for i from 1 to m do
        for j from 1 to n do
            d := generic_val(M[i, j], x, x0);
            if d < v then
                v := d; cc := [i, j];
            elif d = v then
                cc := cc, [i, j]
            fi
        od;
    od;
    lcM := linalg['matrix'](m, n, 0);
    for c in [cc] do
        a := normal((x-x0)^(-v)*M[c[1], c[2]]);
        lcM[c[1], c[2]] := subs(x = 0, a)
    od;
    v
end:

# mat_pval(M, x, p [, lcM])
#   INPUT:  f  - a a rational function
#           x  - a name
#           p  - a polynomial or 1/x
#           lc - (optional) a name
#   OUTPUT: the order of f at p.  If lcM is passed as argument, it will be
#           assigned to the leading matrix lcM of M at p
#
mat_pval := proc(M, x, p, lcM)
    local v, c, cc, d, i, j, n, m, U, V, a;

    if nargs = 3 then
        RETURN(min(op(map(pval, convert(M, 'set'), x, p))))
    fi;
    m := linalg['rowdim'](M);
    n := linalg['coldim'](M);
    v := infinity;
    cc := NULL;
    for i from 1 to m do
        for j from 1 to n do
            d := pval(M[i, j], x, p);
            if d < v then
                v := d; cc := [i, j];
            elif d = v then
                cc := cc, [i, j]
            fi
        od;
    od;
    lcM := linalg[matrix](m, n, 0);
    for c in [cc] do
        a := normal(p^(-v)*M[c[1], c[2]]);
        gcdex(denom(a), p, x, 'U', 'V');
        lcM[c[1], c[2]] := rem(numer(a)*U, p, x)
    od;
    v
end:

# ************************************************************************* #
#                         some matrix tools                                 #
# ************************************************************************* #

# Id(n)
#   INPUT:  n - an integer
#   OUTPUT: The n x n identity matrix
#
Id := proc(n)
    local i;

    if n = 0 then NULL else linalg['diag'](seq(1, i = 1..n)) fi
end:

# Evalm(M, x)
#  INPUT:  M  - a matrix
#          x  - a name
#  OUTPUT: same as evalm, but entries of M are collected w.r.t. x
#
Evalm := proc(M, x) map(collect, evalm(M), x) end:

# normalm(M)
#  INPUT:  M  - a matrix
#  OUTPUT: same as evalm, but entries of M are normalized
#
normalm := proc(M) map(normal, evalm(M)) end:

# evalam(M, x)
#  INPUT:  M  - a matrix
#          x  - a name
#  OUTPUT: same as evalm, but numerator and denominator of the entries of M
#          are reduced w.r.t. to algebraic numbers and collected w.r.t. x
#
evalam := proc(M, x)

    map(proc(f)
            collect(numer(f), x, evala)/
            collect(denom(f), x, evala)
        end, evalm(M))
end:

# lc_evalam(M, x, ext, b)
#  INPUT:  M  - a matrix
#          x  - a name
#          ext - a set of RootOf's
#          b   - (optional) an integer
#  OUTPUT: same as evalm, but lc_evala is mapped to entries
#
lc_evalam := proc(M, x)

    map(lc_evala, evalm(M), x, seq(args[i], i=3..nargs))
end:

# localize(M, x, x0)
#  INPUT:  M  - a matrix
#          x  - a name
#          x0 - a rational or algebraic number or infinity
#  OUTPUT: move the point given by x0 to the origin
#
localize := proc(M, x, x0)

    if x0 <> infinity then
        if has(x0, 'RootOf') then
            # reduce algebraic numbers of the denominator up to
            # a sufficient high order
            map(proc(f, x, x0)
                    normal(numer(f)/lc_evala(expand(denom(f)), x, {x0}))
                end,
                subs(x = x+x0, eval(M)), x, x0);
        else
            evalm(subs(x = x+x0, eval(M)))
        fi
    else
    	evalm(subs(x = 1/x, eval(M)))
    fi
end:

# transform(A, x, T)
#  INPUT:  A - a rational function square matrix
#          x - a name
#          T - a rational function square matrix
#  OUTPUT: for a system Y'=AY define the matrix T[A] := T^{-1}(AT-T'). The
#          system Z'=T[A]Z is gotten from the transformationwhere Y = TZ.
#
transform := proc(A, x, T)

    map(normal, evalm(1/T &* ((A &* T) - map(diff, T, x))));
end:

# pseudo_transform(A, x, T, phi, delta)
#  INPUT:  A - a rational function square matrix
#          x - a name
#          T - a rational function square matrix
#  OUTPUT: for a system Y'=AY define the matrix T[A] := T^{-1}(AT-T'). The
#          system Z'=T[A]Z is gotten from the transformationwhere Y = TZ.
#
pseudo_transform := proc(A, x, T, phi, delta)

    map(normal, evalm(1/T &* ((A &* (map(phi,T,x))) - map(delta, T, x))));
end:

# tensor_prod(A, B)
#   INPUT:  A - a matrix
#           B - a matrix
#   OUTPUT: The tensor product matrix of A and B
#
tensor_prod := proc(A, B)
    local i, j, C, ma, mb, na, nb;

    ma := linalg['rowdim'](A);
    mb := linalg['rowdim'](B);
    na := linalg['coldim'](A);
    nb := linalg['coldim'](B);
    C := linalg['matrix'](ma*mb, na*nb, 0);
    for i from 0 to ma-1 do
        for j from 0 to na-1 do
            C := linalg['copyinto'](linalg['scalarmul'](B, A[i+1, j+1]), C,
                                    1+i*mb, 1+j*nb)
        od
    od;
    evalm(C)
end:

# colechelon(A, [T, ind])
#    INPUT:  A   - a matrix containing rationals and RootOfs
#            T   - (optional) a name
#            ind - (optional) a name
#    OUTPUT: the column echelon form of A. T is assigned to the right
#            transformation matrix. If ind is passed as argument, it is
#            assigned to the row numbers of the "steps" of the echelon form.
#
colechelon := proc(A, T, ind)
    local ext, B, ind0, r, c, b, i, ii, j, jj, h, TT;

    c := linalg['coldim'](A);
    r := linalg['rowdim'](A);
    ext := indets(convert(A, 'set'), 'RootOf');
    B := copy(A);
    TT := Id(c);
    ind0 := [];
    j := 1;
    if ext = {} then
        # faster version if no algebraic extension
        for i from r by -1 to 1 while j<=c do
            jj := j;
            while jj <= c and B[i, jj] = 0 do jj := jj+1 od;
            if jj = c+1 then next fi;
            ind0 := [i, op(ind0)];
            if jj > j then
                B := linalg['swapcol'](B, jj, j);
                TT := linalg['swapcol'](TT, jj, j)
            fi;
            for h from jj+1 to c do
                b := B[i, h]/B[i, j];
                for ii to i do
                    B[ii, h] := normal(B[ii, h]-b*B[ii, j])
                od;
                for ii to c do
                    TT[ii, h] := normal(TT[ii, h]-b*TT[ii, j])
                od
            od;
            j := j+1
        od;
    else
        # version with evala
        for i from r by -1 to 1 while j<=c do
            jj := j;
            while jj <= c and B[i, jj] = 0 do jj := jj+1 od;
            if jj = c+1 then next fi;
            ind0 := [i, op(ind0)];
            if jj > j then
                B := linalg['swapcol'](B, jj, j);
                TT := linalg['swapcol'](TT, jj, j)
            fi;
            for h from jj+1 to c do
                b := evala(Normal(B[i, h]/B[i, j]));
                for ii to i do
                    B[ii, h] := evala(Normal(B[ii, h]-b*B[ii, j]))
                od;
                for ii to c do
                    TT[ii, h] := evala(Normal(TT[ii, h]-b*TT[ii, j]))
                od
            od;
            j := j+1
        od
    fi;
    if nargs = 3 then ind := ind0 fi;
    if nargs >= 2 then assign(T, TT) fi;
    op(B)
end:

# rowechelon(A [, T, ind])
#    INPUT:  A   - a matrix containing rationals or RootOfs
#            T   - (optional) a name
#            ind - (optional) a name
#    OUTPUT: the row echelon form of A. T is assigned to the left
#            transformation matrix. If ind is passed as argument, it is
#            assigned to the column numbers of the "steps" of the echelon form.
#            If A contains no RootOfs, the linalg[gausselim]-function is used
#
rowechelon := proc(A, T, ind)
    local ext, m, n, R, B, TT, i, ind1, j, ind0, r, c, b, ii, jj, h;

    m := linalg['rowdim'](A);
    n := linalg['coldim'](A);
    ext := indets(convert(A, 'set'), 'RootOf');
    if ext = {} then
        R := linalg['gausselim'](linalg['augment'](A, Id(m)));
        B := linalg['submatrix'](R, 1..m, 1..n);
        TT := linalg['submatrix'](R, 1..m, n+1..n+m);
        if nargs >= 2 then assign(T, TT) fi;
        if nargs = 3 then
            i := 1;
            ind1 := NULL;
            for j from 1 to n do
                if B[i, j] <> 0 then
                    ind1 := ind1, j;
                    i := i+1
                fi;
                if i > m then break fi;
            od;
            ind := [ind1]
        fi;
        op(B)
    else
        # version with evala
        c := linalg['coldim'](A);
        r := linalg['rowdim'](A);
        B := copy(A);
        TT := Id(r);
        ind0 := [];
        i := 1;
        for j from 1 to c while i<=r do
            ii := i;
            while ii <= r and evala(B[ii, j]) = 0 do ii := ii+1 od;
            if ii = r+1 then next fi;
            ind0 := [j, op(ind0)];
            if ii > i then
                B := linalg['swaprow'](B, ii, i);
                TT := linalg['swaprow'](TT, ii, i)
            fi;

            for h from ii+1 to r do ###print(`row is`, h, `column is`, j);
                b := evala(Normal(B[h, j]/B[i, j]));
                B[h, j] := evala(Normal(B[h, j]-b*B[i, j]));
                for jj from j+1 to c do
                    B[h, jj] := B[h, jj]-b*B[i, jj];
                od;
                for jj from 1 to r do
                    TT[h, jj] := evala(Normal(TT[h, jj]-b*TT[i, jj]))
                od
            od;
            i := i+1
        od;
        if nargs = 3 then ind := ind0 fi;
        if nargs >= 2 then assign(T, TT) fi;
        op(B)
    fi
end:

# complement(A, [, ind])
#    INPUT:  A   - a matrix containing rationals or RootOfs
#            ind - (optional) a list of integers
#    OUTPUT: the complement of the linear space generated by the columns
#            of the matrix A. If ind is passed as argument, the matrix A
#            is supposed to be in column echelon form and the elements of
#            ind must correspond to the consecutive row numbers of the
#            'steps' of A
#
complement := proc(A, ind)
    local B, T, ind1, C, n, h, j, i;

    if nargs = 1 then
        B := colechelon(A, 'T', 'ind1')
    else
        B := evalm(A);
        ind1 := ind
    fi;
    n := linalg['rowdim'](A);
    C := linalg['matrix'](n, n-nops(ind1), 0);
    h := 1;
    j := 1;
    for i from 1 to n do
        if ind1 <> [] and i = ind1[1] then
            ind1 := subsop(1 = NULL, ind1);
        else
            C[i, j] := 1;
            j := j+1;
        fi
    od;
    evalm(C)
end:

# backsubs(A [, c])
#    INPUT:  A - a matrix in row echelon form
#            c - (optional) a name
#    OUTPUT:
#
backsubs := proc(A, c)
    local x, k, e, S, i, j, m, n, vars, leadvars, leadcols, count;

    if not type(A,matrix) then ERROR(`matrix expected`) fi;
    m := (linalg['rowdim'])(A);
    n := (linalg['coldim'])(A);
    count := 0;
    leadcols := (linalg['vector'])(n,0);
    leadvars := (linalg['vector'])(n,0);
    for i to m do
        for j to n do
            if A[i,j] <> 0 then
                leadcols[j] := i;
                leadvars[i] := j;
                count := count+1;
                break
            fi
        od
    od;
    for i to n-1 do
        if leadvars[i+1] <= leadvars[i] and leadvars[i+1] <> 0 or
           leadvars[i+1] <> 0 and leadvars[i] = 0 then
           ERROR(`matrix must be in echelon form, zero rows at the bottom`)
        fi
    od;
    if leadcols[n] <> 0 then
        ERROR(`inconsistent system`)
    fi;
    vars := (linalg['vector'])(n);
    for i to n-1 do
        if leadcols[i] <> 0 then
            vars[i] := x[i]
        else
            if nargs = 2 then vars[i] := c[i] else vars[i] := t.i fi
        fi
    od;
    vars[n] := 0;
    userinfo(1,linalg,`vars =`, vars);
    e := linalg['multiply'](A,vars);
    k := linalg['col'](A,n);
    S := linalg['vector'](n-1,0);
    for i from count by -1 to 1 do
        x[leadvars[i]] :=
            rhs(op(`solve/linear`({e[i] = k[i]}, {x[leadvars[i]]}))) ;
    od;
    for i to n-1 do
        if leadcols[i] <> 0 then
            S[i] := x[i]
        else
            if nargs = 2 then S[i] := c[i] else S[i] := t.i fi
        fi
    od;
    op(S)
end:

# sylvester(A, B, C [, X, CC, c, h])
#   INPUT:  A  - a m x m constant matrix
#           B  - a n x n constant matrix
#           C  - a m x n constant matrix
#           X  - (optional) a name or a mn x mn constant matrix
#           CC - (optional) a name or a mn x mn constant matrix
#           c  - (optional) a name
#           h  - (optional) an integer
#   OUTPUT: The m x n matrix solution X of the sylvester equation AX-XB = C.
#
sylvester := proc(A, B, C, X, CC, c, h)
    local m, n, AA, CC1, i, j, X1, xis, eqs, s;
    global Eq;
    if not type(A, 'matrix'(anything, 'square')) or
       not type(B, 'matrix'(anything, 'square')) then
        ERROR(`square matrices expected`)
    fi;
    m := linalg['rowdim'](A);
    n := linalg['rowdim'](B);
    X1 := linalg['matrix'](m, n);
    CC1 := linalg['matrix'](m, n);
    if nargs = 3 or (nargs > 3 and not type(args[4], 'array')) then
        xis := {seq(seq(X1[i, j], i = 1..m), j = 1..n)};
        if nargs = 7 then AA := evalm(A-c*Id(m)) else AA := A fi;
        eqs := {seq(seq(convert([seq(AA[i, k]*X1[k, j], k = 1..m)],`+`) -
                     convert([seq(X1[i, k]*B[k, j], k=1..n)],`+`) = CC1[i,j],
                        i = 1..m), j = 1..n)};
        Eq:=eqs;
        #assign(`solve/linear`(eqs, xis));
        assign(SolveTools['Linear'](eqs, xis));
        if nargs >= 5 then X := X1; CC := CC1 fi;
    else
        X1 := X;
        CC1 := CC
    fi;
    if nargs = 7 then s := c= h else s := NULL fi;
    map(evala, subs(s, seq(seq(CC1[i, j] = C[i, j], i=1..m), j = 1..n),
                    eval(X1)));
end:

# gen_sylvester(A, B, C, D, E [, K, L, c])
#   INPUT:  A - a m x m constant matrix
#           B - a n x n constant matrix
#           C - a m x m constant matrix
#           D - a n x n constant matrix
#           K - (optional) a name or a mn x mn constant matrix
#           L - (optional) a name or a mn x mn constant matrix
#           c - (optional) a name
#   OUTPUT: The m x n matrix solution X of the generalized sylvester equation
#           AXD-BXC = E.
#           If K is passed as argument, it will be assigned to a row-echelon
#           form of the tensor-product matrix. If the solution is not unique,
#           it contains parameters of the form t.i unless c is given as
#           argument, in which case the parameters are of the form c[i].
#
gen_sylvester := proc(A, B, C, D, E, K, L, c)
    local m, n, ext, AA, T, K1, L1, EE, i, j, X;

    if not type(A, 'matrix'(anything, 'square')) or
       not type(B, 'matrix'(anything, 'square')) then
        ERROR(`square matrices expected`)
    fi;
    m := linalg['rowdim'](A);
    n := linalg['rowdim'](B);
    if nargs = 5 or (nargs > 5 and not type(args[6], 'array')) then
        if nargs = 8 then AA := evalm(A-c*B) else AA := A fi;
        T := evalm(tensor_prod(linalg['transpose'](B), A) -
                   tensor_prod(linalg['transpose'](D), C));
        K1 := rowechelon(T, 'L1');
        if nargs >= 7 then K := evalm(K1); L := evalm(L1) fi;
    else
        K1 := K;
        L1 := L
    fi;
    EE := linalg['vector']([seq(seq(E[i, j], i=1..m), j=1..n)]);
    if nargs = 8 then
        X := backsubs(linalg['augment'](K1, evalm(L1 &* EE)), args[8])
    else
        X := backsubs(linalg['augment'](K1, evalm(L1 &* EE)))
    fi;
    linalg['matrix']([seq([seq(X[i+j*m], j=0..n-1)], i=1..m)])
end:

# sim_sylvester(A, B, C, D, E, F [, XY, EF, c])
#   INPUT:  A - a m x m constant matrix
#           B - a m x m constant matrix
#           C - a n x n constant matrix
#           D - a n x n constant matrix
#           E - a m x n constant matrix
#           F - a m x n constant matrix
#           XY - (optional) a name or a 2mn x 2mn constant matrix
#           EF - (optional) a name or a 2mn x 2mn constant matrix
#           c - (optional) a name
#           h - (optional) a name
#   OUTPUT: The list [X, Y] where X, Y are  m x n matrices solution of the
#           simultaneous sylvester equation
#                     (A*q^h-lambda B)X - Y(C-lambda D) = E -lambda F.
#           If K is passed as argument, it will be assigned to a row-echelon
#           form of the tensor-product matrix. If the solution is not unique,
#           it contains parameters of the form t.i unless c is given as
#           argument, in which case the parameters are of the form c[i].
#
sim_sylvester := proc(A, B, C, D, E, F, XY, EF, c, h)
    local m, n, ext, AA, i, j, xyis, eqs, XY1, EF1, s;

    if not type(A, 'matrix'(anything, 'square')) or
       not type(B, 'matrix'(anything, 'square')) or
       not type(C, 'matrix'(anything, 'square')) or
       not type(D, 'matrix'(anything, 'square'))
    then
        ERROR(`square matrices expected`)
    fi;
    m := linalg['rowdim'](A);
    n := linalg['rowdim'](C);

    XY1 := linalg['matrix'](m, n*2);
    EF1 := linalg['matrix'](m, n*2);
    if nargs = 6 or (nargs > 6 and not type(args[7], 'array')) then
        xyis := {seq(seq(XY1[i, j], i = 1..m), j = 1..2*n)};
        if nargs = 10 then AA := evalm(A-c*B) else AA := A fi;
        eqs := {seq(seq(convert([seq(AA[i, k]*XY1[k, j], k = 1..m)],`+`) -
                    convert([seq(XY1[i, k+n]*C[k, j], k=1..n)],`+`) = EF1[i,j],
                        i = 1..m), j = 1..n),
                seq(seq(convert([seq(B[i, k]*XY1[k, j], k = 1..m)],`+`) -
                    convert([seq(XY1[i, k+n]*D[k, j], k=1..n)],`+`) =
                    EF1[i,j+n],
                        i = 1..m), j = 1..n)};
        #assign(`solve/linear`(eqs, xyis));
        assign((SolveTools['Linear'](eqs, xyis)));
        if nargs >= 8 then XY := XY1; EF := EF1 fi;
    else
        XY1 := XY;
        EF1 := EF
    fi;
    if nargs = 10 then s := c= h else s := NULL fi;
    [map(evala, subs(s, seq(seq(EF1[i, j] = E[i, j], i=1..m), j = 1..n),
                     seq(seq(EF1[i, j+n] = F[i, j], i=1..m), j = 1..n),
                     linalg['submatrix'](XY1, 1..m, 1..n))),
     map(evala, subs(s, seq(seq(EF1[i, j] = E[i, j], i=1..m), j = 1..n),
                     seq(seq(EF1[i, j+n] = F[i, j], i=1..m), j = 1..n),
                     linalg['submatrix'](XY1, 1..m, n+1..2*n)))]
end:

# pencil_block_reduce(A, B, lambda, P [, S, T])
#   INPUT:  A      - a square matrix containing rationals or RootOfs
#           B      - a square matrix containing rationals or RootOfs
#           lambda - a name
#           P      - a polynomial in lambda
#   OUTPUT: matrices S,T such that
#                                   [ C-lambda D      0       ]
#                  S(A+lambda B)T = [                         ]
#                                   [ E-lambda F  G-lambda H  ]
#
#           and det(G+lambda H) = P. The function supposes that P divides
#           the relative characteristic polynomial det(A-lambda B)
#
pencil_block_reduce := proc(A, B, P, lambda, S, T)
    local n, c, dummy, C, D, PP, K, F, iF, k, m, SS, TT, ind;

    n := linalg['rowdim'](A);

    userinfo(5, 'Utilities', `pencil_block_reduce -> n, degree(P, lambda):`, n, degree(P, lambda));

    if degree(P, lambda) = 0 then
        m := n - degree(linalg['det'](evalm(A-lambda*B)), lambda);
        pencil_block_reduce(B, A, lambda^m, lambda, 'SS', 'TT');
        if nargs = 6 then assign(S, SS); assign(T, TT) fi;
        RETURN(map(evala, mult(mult(SS, evalm(A-lambda*B)), TT)))
    fi;

    if evala(linalg['det'](B)) <> 0 then
        # easier case if B is invertible
        userinfo(5, 'Utilities', `pencil_block_reduce B is invertible`);
        C := linalg['inverse'](B);
        K := evalm(subs(lambda = evalm(C&*A), P));
        if not type(K, 'matrix') then K := evalm(Id(n)*K) fi;
        F := linalg['transpose'](convert([seq(convert(k, 'list'),
                                 k = linalg['kernel'](K))], 'matrix'));
        F := colechelon(F, dummy, ind);
        TT := linalg['augment'](complement(F, ind), F);
        SS := map(evala, mult(linalg['inverse'](TT), C));
        #print("SS", SS, "TT", TT);
    else
        # more complicated case: B is singular
        userinfo(5, 'Utilities', `pencil_block_reduce B is singluar`);
        c := 1;
        while evala(linalg['det'](evalm(A-c*B))) = 0
        do c := c+1 od;
        userinfo(5, 'Utilities', `pencil_block_reduce c=`, c);

        C := linalg['inverse'](evalm(A-c*B));
        D := evalm(B &* C);
        PP := numer(subs(lambda = c+1/lambda, P));
        #print("D, PP", D, PP);
        K := evalm(subs(lambda = D, PP));
        if not type(K, 'matrix') then
            K := evalm(Id(n)*K)
        fi;
        #print("K", K);
        F := linalg['transpose'](convert([seq(convert(k, 'list'),
                                 k = linalg['kernel'](K))], 'matrix'));
        F := colechelon(F, dummy, ind);
        F := linalg['augment'](complement(F, ind), F);
        iF := evalm(1/F);
        #print("C, D, F, iF", C,D,F,iF);
        D := evalm(iF &* D &* F);
        #print("D",D);
        m := n-degree(P, lambda);
        SS := map(evala, evalm(linalg['diag'](Id(m), linalg['inverse'](
                         linalg['submatrix'](D, m+1..n, m+1..n))) &* iF));
        TT := map(evala, evalm(C &* F));
        #print("SS", SS, "TT", TT);
    fi;
    if nargs = 6 then assign(S, SS); assign(T, TT) fi;
    map(evala, mult(SS, mult(evalm(A-lambda*B), TT)))
end:

# This function is actually not used!
pencil_solve := proc(A, B, C, c, invABc)
    local cc, X, Y, T;

    if (nargs >= 4 and not type(c, 'name')) then
        cc := c
    else
        cc := 0;
        while evala(linalg['det'](evalm(A-cc*B))) = 0 do cc := cc+1 od;
    fi;
    if nargs = 5 and type(invABc, 'matrix') then
        T := invABc
    else
        T := linalg['inverse'](evalm(A-cc*B))
    fi;
    if nargs = 5 and type(invABc, 'name') and type(c, 'name') then
        c := cc;
        invABc := evalm(T)
    fi;
    X := evalm(T &* C);
    [eval(X), linalg['scalarmul'](X, cc)]
end:

# randomPoly(x [, d])
#   INPUT:  x - a name
#           d - (optional) an integer
#   OUTPUT: a random polynomial in x with at most d terms (3 terms, if d not
#           specified), coefficients of range [-10..10], exponents 1..3,
#
randomPoly := proc(x, d)
    local d1;

    if nargs = 1 then d1 := 4 else d1 := d fi;
    RETURN(randpoly(x, 'coeffs'=rand(-10..10), 'expons' = rand(1..d),
                       'terms' = d1));
end:

# randomRat(x, denoms, poles)
#   INPUT:  x      - a name
#           denoms - a list of polynomials
#           poles  - a list of positive integers
#   OUTPUT: a random rational function which has a pole at demons[i] of
#           order at most poles[i].
#
randomRat := proc(x, denoms, poles)
    local p, d, i, r, inf, m;

    d := 1;
    inf := 0;
    for i from 1 to nops(denoms) do
        p := rand(0..poles[i]);
        if denoms[i] = infinity then
            inf := poles[i]
        else
            d := d*denoms[i]^p()
        fi;
    od;
    m := max(0, degree(d, x)+inf);
    r := randpoly(x, 'coeffs'=rand(-5..5), 'expons' = rand(0..m),
                     'terms' = 2);
    r/d
end:

# randomMatrix(x, m, denoms, poles, ra)
#   INPUT:  m      - a positive integer
#           x      - a name
#           denoms - a list of polynomials
#           poles  -
#           ra     - (optional)
#   OUTPUT: a mxm matrix with entries from the procedure randomRat(x,d,p)
#
randomMatrix := proc(m, x, denoms, poles, ra)
    local M, i, j, r, rr, nr;

    do
        M := linalg[matrix](m, m, 0);
        if nargs = 5 then r := rand(0..ra) else r := rand(0..2) fi;
        if denoms = [] then
            nr := rand(-3..3);
            for i from 1 to m do
                for j from 1 to m do
                    rr := r();
                    if rr = 0 then M[i, j] := nr() fi;
                od
            od
        else
            for i from 1 to m do
                for j from 1 to m do
                    rr := r();
                    if rr = 0 then M[i, j] := randomRat(x, denoms, poles)
                    fi
                od
            od
        fi;
        if m > 4 then break fi;
        if simplify(linalg[det](M)) <> 0 then break fi
    od;
    evalm(M)
end:

# intDiff(fs, X, which)
#   INPUT:  fs - a list of couples [polynomial, integer]
#           X  - a name
#           which - the names 'min' or 'max'
#   OUTPUT: a sublist of fs such that if a is a root of the first part of
#           an element of fs, there is no other element such that a+n is a
#           root of the first part for all n>0 in case of 'min', n<0 otherwise.
#
intDiff := proc(fs, X, which)
    local reps, fs1, tmp, f, f1, n, n1, d, m, idiffs;

    reps := {};
    tmp := convert(fs, 'set');
    fs1 := tmp;
    while fs1 <> {} do
        f := fs1[1];
        m := 0;
        idiffs := NULL;
        for f1 in fs1 do
            n := degree(f[1], X);
            n1 := degree(f1[1], X);
            if n = n1 then
                d := evala(Normal((coeff(f[1], X, n-1)-coeff(f1[1], X, n-1))
                                  /coeff(f1[1], X, n)/n));
                if type(d, 'integer') then
                    if evala(Normal(f[1]-subs(X = X+d, f1[1]))) = 0
                    then
                        idiffs := idiffs, d;
                        tmp := tmp minus {f1};
                        m := m+f1[2];
                        if which = `min` then
                            if d < 0 then f := f1 fi
                        elif which = `max` then if d > 0 then f := f1 fi
                        else ERROR(`wrong argument`) fi;
                    fi;

                fi;
            fi
        od;
        if nargs = 3 then
            reps := reps union {[f[1], m]}
        else
            idiffs := sort([idiffs]);
            idiffs := map((i,j)->i-j, idiffs, idiffs[1]);
            reps := reps union {[f[1], idiffs]}
        fi;
        fs1 := tmp
    od;
    reps
end:

# int_diff_split(fs, X)
#   INPUT:  fs - a list of couples [polynomial, integer]
#           X  - a name
#   OUTPUT: a list of the same form such that the polynomials whose roots
#           differ by integers are grouped together.
#
int_diff_split := proc(fs, X)
    local reps, fs1, tmp, f, f1, n, n1, d, m;

    reps := {};
    tmp := convert(fs, 'set');
    fs1 := tmp;
    while fs1 <> {} do
        f := fs1[1];
        m := 1;
        for f1 in fs1 do
            n := degree(f[1], X);
            n1 := degree(f1[1], X);
            if n = n1 then
                d := evala(Normal((coeff(f[1], X, n-1)-coeff(f1[1], X, n-1))
                                  /coeff(f1[1], X, n)/n));
                if type(d, 'integer') and
                   evala(Normal(f[1]-subs(X = X+d, f1[1]))) = 0
                then
                    tmp := tmp minus {f1};
                    m := m*f1[1]^f1[2]
                fi
            fi
        od;
        reps := reps union {[m, degree(m, X)]};
        fs1 := tmp
    od;
    reps
end:


# ************************************************************************* #
#                                                                           #
#                   Utilities for handling scalar ordinary                  #
#                        linear differential equations                      #
#                                                                           #
#                            Last update:  07.09.97                         #
#                                                                           #
# ************************************************************************* #

# getOde(equ, y)
#   INPUT:  equ - an expression
#           y   - an expression of the form f(x) (the unknown)
#   OUTPUT: if ode is a linear differential equation in f(x) with
#           rational coefficients, the list of coefficients of the equation
#           and the rhs member are returned.
#
getOde := proc(equ, y)
    local x, dvars, equ1, v, b;

    if not type(equ, 'equation') and not type(equ, 'algebraic') then
        ERROR(`1st argument must be a linear homogeneous ordinary differential equation`)
    fi;
    if not type(y, 'anyfunc(name)') then
        ERROR(`2nd argument must be a function`)
    fi;
    x := op(1, y);
    if type(equ, `=`) then equ1 := lhs(equ)-rhs(equ) else equ1 := equ fi;
    dvars := map(isDerivative, indets(equ1), y);
    if not (type(equ1, linear(dvars))) then
        ERROR(`not a linear ode`)
    fi;
    b := -subs({seq(v = 0, v = dvars)}, equ1);
    equ1 := equ1 + b;
    ode2lode(equ1, y), x, b;
end:

# getLodo(L, y)
#   INPUT:  L - an element of an Lodo-ring (syntax of the Lodo-package)
#   OUTPUT: list of coefficients of the operator.
#
getLodo := proc(L, y)
    local x, z;

    if not type(y, 'anyfunc(name)') then
        ERROR(`2nd argument must be a function`)
    fi;
    x := op(1, y);
    # Cheat!
    if type(L, 'list') and not type(L[1], 'list') then RETURN(L, x) fi;
    if not ((nops(L)=4) and (type(L[1], 'list')) and (type(L[2], 'string'))
            and (type(L[3], 'procedure')) and (type(L[4], 'procedure'))) then
        ERROR(`1st argument must be a Lodo-polynomial`)
    fi;
    if type(`Lodo/apply`, 'name') then ERROR(`Lodo package not found`) fi;
    ode2lode(`Lodo/apply`(L, z(x)), z(x)), x;
end:

# getLODO(f, x, y)
#   INPUT:  f - a polynomial in D and x
#   OUTPUT: list of coefficients of the operator.
#
getLODO := proc(f, y, x)
    local g, i, j;

    g := numer(f);
    if not type(g, 'polynom'(anything, [x, y])) then
        ERROR(`1st argument must be a polynomial`)
    fi;
    coeff(g, y, 0)*y(x) +
    convert([seq(diff(y(x), seq(x, j=1..i))*coeff(g, y, i),
             i = 1..degree(g, y))], `+`), y(x)
end:

# ode2lode(ode, y)
#   INPUT:  ode - a expression homogeneous and linear in y and its derivatives
#           y   - an expression of the form f(x)
#   OUTPUT: list of coefficients of the i-th derivative of y (i=0,..,n) in ode
#
ode2lode := proc(ode, y)
    local ode1, c, dvars, terms, dy, res, i, v, x;

    ode1 := numer(ode);
    dvars := map(isDerivative, indets(ode1), y);
    c := [coeffs(collect(ode1, dvars), dvars, terms)];
    terms := [terms];
    dy := y;
    x := op(1, y);
    res := NULL;
    for i from 0 to nops(dvars)-1 do
        if member(dy, terms, 'v') then res := res, c[v] else res := res,0 fi;
        dy := diff(dy, x);
    od;
    makeSimple([res], x)
end:

# isDerivative(expr, y)
#   INPUT: expr - an expression
#          y    - an expression of the form f(x)
#
isDerivative := proc(expr, y)
    if (expr = y) or (type(expr, function) and (op(0, expr) = 'diff')) then
        expr
    fi
end:

# makeSimple(L, x)
#   INPUT:  L - list of coefficients of an linear differential operator
#               with rational function coefficients
#           x - a name
#   OUTPUT: list of coefficients of corresponding simple linear differential
#           operator with polynomial coefficients
#
makeSimple := proc(L, x)
    local g, i, L1;

    L1 := clearDenom(L);
    g := L1[1];
    for i from 2 to nops(L1) do g := evala(Gcd(g, L1[i])) od;
    map(quo, L1, g, x);
end:

# clearDenom(L)
#   INPUT:  L - list of coefficients of an linear differential operator
#               with rational function coefficients
#   OUTPUT: List of coefficients of corresponding operator with polynomial
#           coefficients
#
clearDenom := proc(L)
    local d, s, t, i, X;

    d := denom(convert([seq(L[i]*X^i, i=1..nops(L))], `+`));
    map(normal, map(((s,t)->s*t), L, d))
end:

# normalize(L, x [, k] [, opts])
#   INPUT:  L    - list of coefficients of an linear differential operator
#                  with polynomial function coefficients
#           x    - a name
#           k    - (optional) an integer
#           opts - a set of options
#   OUTPUT: List of coefficients of corresponding operator with height zero
#           If k is passed as arguments, the coefficients are truncated
#           If 'derivation = euler' is an element of opt, the operator is
#           supposed having the derivation x d/dx
#
normalize := proc(L, x, moreargs)
    local d, k, h, i;

    k := infinity;
    d := 1;
    if nargs = 3 then
        if type(args[3], 'set') then
            if member('derivation' = 'euler', args[3]) then d := 0 fi;
        elif type(args[3], 'integer') then
            k := args[3]
        fi
    elif nargs = 4 then
        if type(args[3], 'integer') and (type(args[4], 'set')) then
            k := args[3];
            if member('derivation' = 'euler', args[4]) then d := 0 fi
        fi
    fi;
    h := min(seq(ldegree(L[i], x)-d*(i+1), i=1..nops(L)));
    [seq(truncate(x^(-h)*L[i], x, simplify(k+d*(i-1))),
         i=1..nops(L))];
end:

# Apply(L, x, expr [, k] [, opt])
#   INPUT:  L    - list of coefficients of an linear differential operator
#                  with rational function coefficients
#           x    - a name
#           expr - an algebraic expression
#           k    - (optional) an integer
#           opt  - (optional) a set of options
#   OUTPUT: the expression resulted from the application of L to expr
#           if k is passed as argument, the computations are done modulo x^k
#           If 'derivation = euler' is an elemnt of opts, the derivation
#           x d/dx is assumed.
#
Apply := proc(L, x, expr, moreargs)
    local i, k, res, d, e;

    # EP attention: expr can be a rational function
    k := infinity;
    d := 1;
    if nargs = 4 then
        if type(args[4], 'set') then
            if member('derivation' = 'euler', args[4]) then d := x fi;
        elif type(args[4], 'integer') then
            k := args[4]
        fi
    elif nargs = 5 then
        if type(args[4], 'integer') and (type(args[5], 'set')) then
            k := args[4];
            if member('derivation' = 'euler', args[5]) then d := x fi
        fi
    fi;
    res := 0;
    e := expr;
    for i from 1 to nops(L) do
        res := res + truncate(L[i]*e, x, k);
        e := d*diff(e, x)
    od;
    res
end:

# toEuler(L, x)
#  INPUT:  L    - list of coefficients of linear differential with
#                 derivation d/dx
#          x    - a name
#  OUTPUT: list of coefficients of linear differential operator with
#          derivation x d/dx which is equivalent to L
#
toEuler := proc(L, x)
    local i, j, res, temp, theta;

    temp := convert([seq(L[i]*x^(1-i)*product(theta-j, j=0..i-2),
                         i=1..nops(L))], `+`);
    temp := collect(numer(temp), theta);
    res := seq(coeff(temp, theta, i), i=0..nops(L)-1);
    expand([res])
end:

# newtonPolygon(lode, x, p, u [,m ] [,opts])
#   INPUT:  lode  - list of coefficients of linear differential operator
#           x     - a name
#           p     - a polynomial or 1/x
#           u     - a name
#           m     - (optional) an integer
#           which - (optional) a set of integers
#   OUTPUT: the Newton polygon of L at p represented as list
#           [height, u, list of couples [slope, newton polynomial]]
#           if m is passed as argument, the polygon is constructed only for
#           the first m+1 coefficients of L.
#           if 'which = differential' is an element of opts (this is the
#           default setting), the differential polygon is constructed.
#           if 'which = algebraic' is an element of opts, the algebraic
#           polygon is constructed.
#           if 'derivation = euler' is an element of opts, the differential
#           polygon of L with derivation x d/dx is computed.
#
newtonPolygon := proc(L, x, p, u, moreargs)
    local L1, n, s, yp, lc, lcs, drop, npoly, d, i, j, k,
          which, slope, res, dp, opt1;

    opt1 := {};
    L1 := expand(L);
    n := nops(L);
    which := 'differential';
    if nargs = 5 then
        if type(args[5], 'integer') then
            n := args[5]+1
        else
            opt1 := args[5]
        fi;
    elif nargs = 6 then
        if type(args[5], 'integer') then
            n := args[5]+1 fi;
        opt1 := args[6];
    fi;
    if (p = 1/x) or (p = infinity) then
        s := -1; dp := 1 else s := 1; dp := diff(p, x)
    fi;
    if member('which' = 'differential', opt1) then
        which := 'differential'
    elif member('which' = 'algebraic', opt1) then
        which := 'algebraic'
    fi;
    if member('derivation' = 'euler', opt1) then
        s := 0 # be careful: p=1/x ?? EP
    fi;
    yp := NULL; lcs := NULL;
    for i from 1 to n do
       yp := yp, pval(L1[i], x, p, 'lc')-s*(i-1);
       lcs := lcs, lc
    od;
    yp := [yp]; lcs := [lcs];
    drop := min(op(yp));
    npoly := 0;
    for j from 1 to n do
        if yp[j] = drop then
            if which = 'algebraic' then
                npoly := npoly + lcs[j]*(s*u)^(j-1)
            elif (p <> infinity) and (p <> 1/x) then
                npoly := npoly + rem(dp^(j-1)*lcs[j], p, x)*
                                 product(s*u-k+1, k=1..j-1)
            else
                npoly := npoly + lcs[j]*product(s*u-k+1, k=1..j-1)
            fi
        fi;
    od;
    d := degree(npoly, u);
    if d > 0 then res := [0, collect(primpart(npoly, u), u)]
        else res := NULL
    fi;
    i := d+1;
    while i < n do
        slope := min(seq((yp[j]-yp[i])/(j-i), j=i+1..n));
        npoly := 0;
        for j from i to n do
            if yp[j] -(j-i)*slope = yp[i] then
                npoly := npoly + lcs[j]*(s*u)^((j-i)/denom(slope))
            fi;
        od;
        if (which = 'differential') and (p <> 1/x) then
            npoly := rem(subs(u = diff(p, x)*u, npoly), p, x)
        fi;
        res := res, [slope, collect(primpart(npoly, u), u)];
        i := i+degree(npoly, u)*denom(slope);
    od;
    [drop, u, [res]]
end:


# ************************************************************************* #
#                                                                           #
#                        Utilities for handling systems of                  #
#                     ordinary linear differential equations                #
#                                                                           #
#                           Last update:  08.10.97                          #
#                                                                           #
# ************************************************************************* #


# companion_sys(eq, y)
#   INPUT:  eq - a linear differential operator or equation
#           y  - a function of the form y(x)
#   OUTPUT: an equivalent first order system in companion form
#
companion_sys := proc(eq, y)
    local L, tmp, i, X;

    if nargs <> 2 then ERROR(`wrong number of arguments`)
    elif type(args[1], 'list') then
        tmp := getLodo(args[1], args[2])
    else
        tmp := getOde(args[1], args[2])
    fi;
    L := [seq(normal(tmp[1][i]/tmp[1][nops(tmp[1])]), i=1..nops(tmp[1]))];
    linalg[transpose](linalg[companion]
        (convert([seq(L[i]*X^(i-1), i=1..nops(L))], `+`), X))
end:

# sym_power_sys(A, x, m)
#   INPUT:  A     - a matrix with rational function entries
#           x     - a name
#           m     - a positive integer
#   OUTPUT: the m-th symmetric power system of the system Y' = A(x)Y
#
sym_power_sys := proc(A, x, m)
     local tmp, y, Y, dY, YY, i, j, n, ind, cc, t, p, M;

     userinfo(5, 'Utilities', `computing monomials`);
     n := linalg[rowdim](A);
     tmp := convert(expand(sum(y[i](x), i=1..n)^m), 'list');
     Y := map(m->m/coeffs(m), tmp);
     dY := map(expand, map(diff, Y, x));
     YY := evalm(A &* [seq(y[i](x), i = 1..n)]);
     for i from 1 to n do
         dY := subs(diff(y[i](x), x) = YY[i], dY)
     od;
     dY := map(expand, dY);
     ind := indets(dY) minus {x} minus indets(A); # EP
     userinfo(5, 'Utilities', `computing matrix entries`);
     M := linalg[matrix](nops(Y), nops(Y));
     userinfo(5, 'Utilities', `computing matrix entries`);
     for i from 1 to nops(Y) do
         cc := [coeffs(collect(dY[i], ind, 'distributed'), ind, 't')];
         for j  from 1 to nops(Y) do
             if member(Y[j], [t], 'p') then
                 M[i, j] := cc[p]
             else
                 M[i, j] := 0
             fi;
         od
     od;
     RETURN(Evalm(M, x))
end:

# tensor_sys(A, B, x)
#   INPUT:  A     - a matrix with rational function entries
#           B     - a matrix with rational function entries
#           x     - a name
#   OUTPUT: the matrix S of the tensor product of the system Y' = A(x)Y
#           and Z' = B(x)Z
#
tensor_sys := proc(A, B, x)
    local y, z, W, dW, AY, BZ, i, j, n, m, ind, cc, t, p, M;

    userinfo(5, 'Utilities', `computing monomials`);
    n := linalg[rowdim](A);
    m := linalg[rowdim](B);
    W := [seq(seq(y[i](x)*z[j](x), i = 1..n), j = 1..m)];
    dW := map(expand, map(diff, W, x));
    AY := evalm(A &* [seq(y[i](x), i = 1..n)]);
    BZ := evalm(#-linalg['transpose']
                (B) &* [seq(z[i](x), i = 1..m)]);
    for i from 1 to n do
        dW := subs(diff(y[i](x), x) = AY[i], dW)
    od;
    for j from 1 to m do
        dW := subs(diff(z[j](x), x) = BZ[j], dW)
    od;
    dW := map(expand, dW);
    ind := indets(dW) minus {x};
    userinfo(5, 'Utilities', `computing matrix entries`);
    M := linalg[matrix](n*m, n*m);

    userinfo(5, 'Utilities', `computing matrix entries`);
    for i from 1 to nops(W) do
        cc := [coeffs(collect(dW[i], ind, 'distributed'), ind, 't')];
        for j  from 1 to nops(W) do
            if member(W[j], [t], 'p')
                then M[i, j] := cc[p]
                else M[i, j] := 0
            fi;
        od
    od;
    RETURN(Evalm(M, x))
end:



# ram(A, x, c, r)
#   INPUT:  A - a rational function matrix
#           x - a name
#           c - a name or a number
#           r - an integer
#   OUTPUT: the rational function matrix which corresponds to the linear
#           differential system which results from the ramification x=c*t^r
#           in the system Y' = A(x)Y.
#
ram := proc(A, x, c, r)

    Evalm(c*r*x^(r-1)*subs(x = c*x^r, eval(A)), x)
end:

# newton_polygon(f, x, p, u [, m] [,which])
#   INPUT:  lode  - list of coefficients of linear differential operator
#           x     - a name
#           p     - a polynomial or 1/x
#           u     - a name
#           m     - (optional) an integer
#           which - (optional) the symbol 'differential' or 'algebraic'
#   OUTPUT: the differential or algebraic Newton polygon of lode at the place p
#           represented as list
#           [minimal y-value, u, list of couples [slope, newton polynomial]]
#           the polygon is constructed only for lode[i], i = 0,..,m
#
newton_polygon := proc(f, x, p, u, moreargs)
    local lode1, n, s, yp, lc, lcs, drop, npoly, d, e, i, j, k,
          which, slope, res, dp, f1;


    n := degree(f, u);
    f1 := collect(expand(f), u);
    lode1 := [seq(coeff(f, u, i), i=0..n)];
    which := 'differential';
    if nargs = 5 then
        if type(args[5], 'integer') then
            n := args[5]+1
        elif member(args[5], {'differential', 'algebraic'}) then
            which := args[5]
        fi;
    elif nargs = 6 then
        if type(args[5], 'integer') then
            n := args[5]+1 fi;
        if member(args[6], {'differential', 'algebraic'}) then
            which := args[6]
        fi
    fi;
    if (p = 1/x) or (p = infinity) then
        s := -1; dp := 1 else s := 1; dp := diff(p, x)
    fi;
    yp := NULL; lcs := NULL;
    for i from 1 to n+1 do
       yp := yp, pval(lode1[i], x, p, 'lc')-s*(i-1);
       lcs := lcs, lc
    od;
    yp := [yp]; lcs := [lcs];
    drop := min(op(yp));
    npoly := 0;
    for j from 1 to n+1 do
        if yp[j] = drop then
            if which = 'algebraic' then
                npoly := npoly + lcs[j]*(s*u)^(j-1)
            elif (p <> infinity) and (p <> 1/x) then
                npoly := npoly + rem(dp^(j-1)*lcs[j], p, x)*
                                 product(s*u-k+1, k=1..j-1)
            else
                npoly := npoly + lcs[j]*product(s*u-k+1, k=1..j-1)
            fi
        fi;
    od;
 d := degree(npoly, u);
    if d > 0 then res := [0, collect(primpart(npoly, u), u)]
        else res := NULL
    fi;
    i := d+1;
    while i < n+1 do
        slope := min(seq((yp[j]-yp[i])/(j-i), j=i+1..n+1));
        npoly := 0;
        for j from i to n+1 do
            if yp[j] -(j-i)*slope = yp[i] then
                npoly := npoly + lcs[j]*(s*u)^((j-i)/denom(slope))
            fi;
        od;
        if (which = 'differential') and (p <> 1/x) then
            npoly := rem(subs(u = diff(p, x)*u, npoly), p, x)
        fi;
        res := res, [slope, collect(primpart(npoly, u), u)];
        i := i+degree(npoly, u)*denom(slope);
    od;
    [drop, u, [res]]
end:
