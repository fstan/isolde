# ************************************************************************* #
#                                                                           #
#             FormalReduction of linear differential systems               #
#                                                                           #
#                        Last update:  02.01.12                             #
#                                                                           #
# ************************************************************************* #

### NEW changes from 04/01/2012
##In formal_reuction we call mat_block_reduce
#with the parameters  cc=1 and qq=1.
# Also mat_change_exp_part(K, x, w, phi_w, delta_w) with
# phi_w= phi(w,x) and delta_w= delta(w,x)
# These 2 procedures are in MatTools.

# FormalReduce(A, x, opts)
#   INPUT:  A      - a representation of a local matrix series
#           x      - a name
#           opts   - (optional) a set of options
#   OUTPUT: the formal reduction of the linear differential system represented
#           by A.
FormalReduce := proc(A, x, lambda, phi1, delta1, omega1, opts)
    local q, n, res, opts1, u, invars, rhopoly, i, w, tmp,
    B, f, a, P, ext, A0, phi, delta, omega, mu, r, BB;
    global Valuation_Bound;

    if nargs < 6 then
        phi := Default_phi; #phi as procedure
        delta := Default_delta;
        omega := Default_omega;
    else
        phi := args[4];
        delta := args[5];
        omega := args[6];
    fi;
    u:= lambda;
    res := NULL;
    ##options
    if nargs =7 or nargs = 4 then
        opts1 := opts else opts1 := {}
    fi;

    invars := [];
    if has(opts1, 'invariants') then
        invars := rhs(op(select(has, opts1, 'invariants')));
        opts1 := remove(has, opts1, 'invariants');
    fi;

    rhopoly := [];
    if has(opts1, 'rhopolygon') then
        rhopoly := rhs(op(select(has, opts1, 'rhopolygon')));
        opts1 := remove(has, opts1, 'rhopolygon');
        i := indets(rhopoly, 'name');
        if i <> {} then u := i[1] fi;
    fi;

    # Assign Poincare-rank
    q := - mat_valuation (A, x, omega);
    r := omega + q;

    # Evaluate matrix to speed up execution (these terms are definitely needed)
    mat_eval (A, x, -q, -1);
    n :=  mat_get_dim (A);
    ext :=  mat_get_ext (A);
    userinfo(5, 'FormalReduce', `ext=`,ext);
    res := NULL;
    userinfo(1, 'FormalReduce', `enter formal reduction`);
    userinfo(4, 'FormalReduce', `Poincare-rank is `, r,` valuation is `, -q,
             `, dimension of system is`, n);
    userinfo(5, 'FormalReduce', `q,omega =`,q,omega);

    if q < -omega then
        # system is not singular
        userinfo(4, 'FormalReduce', `system is analytic`);
        RETURN([[0, A]])

    elif q = -omega then
        # If the valuation is = omega, the system is of the 1st kind,
        # hence regular singular.
        userinfo(4, 'FormalReduce', `system is of the 1st kind`);
        #RETURN([[0,A]]); ## this was a temporay solution
        A0 := map(coeff,  mat_eval (A, x, -1, -1), x, -1);
        userinfo(4, 'FormalReduce',
                 `computing characteristic polynomial of leading matrix`);
        for f in int_diff_split(factors(linalg['charpoly'](A0, u), ext)[2], u)
        do
            userinfo(4, 'FormalReduce', `factor is:`, f[1]);
            if degree(f[1], u)*f[2] < n then
                userinfo(4, 'FormalReduce', `block-reducing with`, [1], f[1]);
                B :=  mat_block_reduce (A, x, [1], f[1], u, phi1, delta1, omega1, 1, 1);
            else
                B := A
            fi;
            w := RootOf(intDiff(factors(f[1], ext)[2], u, 'min')[1][1]);
            userinfo(4, 'FormalReduce', `minimal eigenvalue`, w);
            res := res, [w/x,  mat_change_exp_part (B, x, w/x)];
        od;
        RETURN([res])

    elif  (mat_get_dim (A) = 1) and (phi(x,x) = x) then
        # system is a scalar linear differential equation of order 1
        userinfo(4, 'FormalReduce', `reducing first order scalar equation`);
        w :=  mat_eval (A, x, -q, -1);
        w := collect(w[1, 1], x, evala);
        userinfo(4, 'FormalReduce', `exponential part found:`, w);
        RETURN([[w,  mat_change_exp_part (A, x, w)]])

    elif rhopoly = [] then

	# Call Moser-reduction in order to determine parts of the rho-polygon
        userinfo(4, 'FormalReduce', `calling Moser-reduction`);
        tmp := LocalSuperReduce(A, x, u,  phi, delta, omega, opts1 union{'step' = 1});
        userinfo(4, 'FormalReduce', `partial rho-polygon`, tmp[2]);
        RETURN( FormalReduce (tmp[1], x, u, phi, delta, omega, opts1 union
                             {'invariants' = tmp[3], 'rhopolygon' = tmp[2]}));

    else

    	# The system is now Moser-reduced
        userinfo(4, 'FormalReduce', `system is Moser-reduced`);

        # Compute the characteristic equation P
        P := rhopoly[nops(rhopoly)][2];
        userinfo(4, 'FormalReduce',
                 `characteristic polynomial of leading matrix: `, P);
        userinfo(5, 'FormalReduce', `P=`,P);
        for f in factors(P, ext)[2] do
            userinfo(4, 'FormalReduce', `irreducible factor is`, f[1],
                     `multiplicity is`, f[2]);
            if evala(subs(u = 0, f[1])) <> 0 then
                # current factor is not of the form u^l (l >= 1)
                if degree(f[1], u) = 1 then
                    # factor is power of a linear polynomial: (x-mu)^l (l >= 1)
                    userinfo(4, 'FormalReduce',
                             `irreducible factor is linear`);
                    #print(f[2],n);
                    if (f[2] > 1) or (n = 1) then
                        # Multiple root case or we have a scalar difference or q-difference equation
                        if n = 1 then
                            userinfo(4, 'FormalReduce', `scalar non-differential case`);
                        else
                        	userinfo(4, 'FormalReduce', `factor is power of linear polynomial`);
                        	userinfo(4, 'FormalReduce', `multiple root, m = `,f[2]);
                        fi;
                        mu := RootOf(f[1], u);
                        #print(f[1],q);
                        w := mu/x^q;
                        userinfo(4, 'FormalReduce',
                                 `changing hyper-exponential part:`, w);
                        B :=  mat_term_transform(A, x, mu, r, phi, delta, omega);
                        # Update the value of q, since Poincare rank can change
                        q := - mat_valuation (B, x, omega);
                        #print(`new q=`, q);

						# Block-reduction, if required
						if f[2] < n then
							userinfo(4, 'FormalReduce', `block-reducing with !`,
                                 [q], u^f[2]);userinfo(4, 'FormalReduce', `B=`, B);
							BB := mat_block_reduce (B, x, [q], u^f[2], u, phi, delta, omega, 1, 1)
   					else
							BB := B;
						fi;
						tmp := FormalReduce(BB, x, u, phi, delta, omega, opts1);
                        res := res, seq([ mat_get_exp_part (i[2], x), i[2]],
                                        i = tmp);
                    else
                        # single root case
                        userinfo(4, 'FormalReduce', `block-reducing with !!`,
                                 [q], f[1]);
                        B :=  mat_block_reduce (A, x, [q], f[1], u, phi, delta, omega, 1, 1);
                        userinfo(4, 'FormalReduce', `*B=`, B);
                        res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1))
                    fi
                else
                    # factor is irreducible polynomial of degree > 1
                    if n > 4 and n-f[2]*degree(f[1], u) > 0 then
                        userinfo(4, 'FormalReduce',
                                 `isolating algebraic extension`);
                        userinfo(4, 'FormalReduce', `block-reducing with !!!`,
                                 [q], f[1]^f[2]);
                        B :=  mat_block_reduce (A, x, [q], f[1]^f[2], u, phi, delta, omega, 1, 1);
                        res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1))
                    else
                        userinfo(4, 'FormalReduce',
                                 `constant field extension necessary`);
                        mu := RootOf(f[1], u);
                        userinfo(4, 'FormalReduce', `root is`, a,
                                 `multiplicity is:`, f[2]);
                        w := mu/x^q;
                        userinfo(4, 'FormalReduce',
                                 `changing hyper-exponential part:`, w);
                        B :=  mat_term_transform(A, x, mu, r, phi, delta, omega);
                        # Update the value of q, since Poincare rank can change
                        q := - mat_valuation (B, x, omega);
                        #print(`1 new q=`, q);

                        userinfo(4, 'FormalReduce', `block-reducing with !!!!`,
                                 [q], u^f[2]);
                        tmp :=  FormalReduce(
                            mat_block_reduce(B, x, [q], u^f[2], u, phi, delta, omega, 1, 1),
                            x, u, phi, delta, omega, opts1);
                        res := res, seq([ mat_get_exp_part (i[2], x), i[2]],
                                        i = tmp);
                    fi
                fi
            else
                # treated factor is of the form u^f[2]
                if f[2] = n then
                    userinfo(4, 'FormalReduce', `nilpotent case`);
                    P := rhopoly[1][2];
                    userinfo(4, 'FormalReduce', `next k-polynomial:`, P);
                    userinfo(5, 'FormalReduce', `nilpotent case P=`,P);
                    if degree(P, u) > 0 then
                        userinfo(4, 'FormalReduce',
                                 `separating ramified slopes`);
                        B :=  mat_block_reduce (A, x, invars, 1, u, phi, delta, omega, 1, 1);
                        res := res, op(ramified_case(B, x, u, phi, delta, omega, invars));
                        userinfo(4, 'FormalReduce', `applying recursion`);
                        B :=  mat_block_reduce (A, x, invars, P, u, phi, delta, omega, 1, 1);
                        res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1))
                    else
                        userinfo(4, 'FormalReduce', `no decoupling possible`, A, x, invars);
                        res := res, op(ramified_case(A, x, u, phi, delta, omega, invars));
                    fi
                else
                    userinfo(4, 'FormalReduce', `block-reducing with !*`,
                             [q], u^f[2]);
                    B :=  mat_block_reduce (A, x, [q], u^f[2], u, phi, delta, omega, 1, 1);
                    res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1))
                fi
            fi
        od;
        RETURN([res])
    fi
end:


# ramified_case(A, x, invars)
#   INPUT:  A      - a representation of a local matrix series
#           x      - a name
#           invars - a list of integers
#   OUTPUT: the formal reduction of A. This procedure supposes that
#           A is rho-irreducible with nilpotent leading matrix.
# PROBLEM with documentation (c is not assigned to a value, r=
# Poincare rank)
ramified_case := proc(A, x, lambda, phi1, delta1, omega1, opts)
    local q, n, B, C, R, r, f, u, np, k, c, d, res, tmp, i, phi, delta, omega;

    if nargs < 6 then
        phi := Default_phi; #phi as procedure
        delta := Default_delta;
        omega := Default_omega;
    else
        phi := args[4];
        delta := args[5];
        omega := args[6];
    fi;
    u:= lambda;

    userinfo(1, 'FormalReduce', `entering ramified case`);
    q := - mat_valuation (A, x, -1);
    n :=  mat_get_dim (A);
    mat_eval (A, x, -q, -1);
    if n <= 3 then
        R := {n};
        userinfo(4, 'FormalReduce', `ramification detected:`, R)
    else
        userinfo(3, 'FormalReduce', `applying heuristic`);
        B :=  mat_eval (A, x, -q, -q+2);
        userinfo(4, 'FormalReduce', `computing characteristic polynomial`);
        f := linalg['charpoly'](B, u);

        np := newton_polygon(f, x, x, u);
        userinfo(4, 'FormalReduce', `algebraic newton polynomial:`, np[3]);
        R := {seq(denom(r[1]), r = np[3])} minus {1};
        userinfo(4, 'FormalReduce', `after computing denominators`,R);
        R := [op(R), op({seq(k, k = 2..n-2), n} minus R)];
        userinfo(4, 'FormalReduce', `candidates for ramification:`, R);
    fi;
    d := n;
    res := NULL;
    userinfo(4, 'FormalReduce', `ramifications to try`, R);
    while d > 0 do
        r := R[1];
        userinfo(4, 'FormalReduce', `remaining ramified exponential parts:`, d);
        userinfo(4, 'FormalReduce', `doing ramification r = `, r);
        ##TODO variable c is not initialised before used here!
        C :=  mat_ramification (A, x, c, r);

        userinfo(4, 'FormalReduce', `looking for slopes:`, {seq(l, l=r*(q-2)+1..r*(q-1)-1)});
        userinfo(4, 'FormalReduce', `ramified_reduction`, C, x, u,  phi, delta, omega, r*(q-2)+1, r*(q-1)-1, c, r);

        tmp := ramified_reduction(C, x, u,  phi, delta, omega, r*(q-2)+1, r*(q-1)-1, c, r);
        d := d-tmp[2];

        userinfo(4, 'FormalReduce', `ramified exponential parts found:`, tmp[2], tmp[1]);

        res := res, seq([ mat_get_exp_part (i[2], x), i[2]], i=tmp[1]);
        userinfo(5, 'FormalReduce', `res=`,res);
# EP                   ext := ext union {seq(op(readlib('mat_get_ext')(l)), l = tmp[1])};
#                    r_tested := r_tested union {r};
#                  rlist := [op(sort({seq(l, l = 2..d-2),d} minus r_tested))];

        R := subsop(1 = NULL, R);
        userinfo(4, 'FormalReduce', `remaining possible ramifications:`, R);
    od;
    [res]
end:

# ramified_reduction(A, x, k1, k2, c, r, u, opts)
#   INPUT:  A      - a representation of a local matrix series
#           x      - a name
#           opts   - (optional) a set of options
#   OUTPUT:
ramified_reduction := proc(A, x, u, phi1, delta1, omega1, k1, k2, c, r, opts)
    local q, n, res, opts1, invars, rhopoly, i, w, tmp, B, f, a, P, R, ext, A0,
          U, V, g, b, cc, AA, found, phi, delta, omega;

    phi := args[4];
    delta := args[5];
    omega := args[6];
    res := NULL;
    if nargs > 10 then opts1 := opts else opts1 := {} fi;

    invars := [];
    if has(opts1, 'invariants') then
        invars := rhs(op(select(has, opts1, 'invariants')));
        opts1 := remove(has, opts1, 'invariants');
    fi;

    rhopoly := [];
    if has(opts1, 'rhopolygon') then
        rhopoly := rhs(op(select(has, opts1, 'rhopolygon')));
        opts1 := remove(has, opts1, 'rhopolygon');
    fi;

    q := - mat_valuation (A, x, omega);
    if q-1 < k1 then RETURN([[], 0]) fi;
    mat_eval (A, x, -q, -1);
    n :=  mat_get_dim (A);
    ext :=  mat_get_ext (A, x);
    res := NULL;
    found := 0;
    userinfo(1, 'testing', `enter formal reduction after ramification`);
    userinfo(4, 'FormalReduce', `pole order is`, q , `, dimension of system is`, n);

    if rhopoly = [] then

        userinfo(4, 'FormalReduce', `calling Moser-reduction`);
        userinfo(4, 'FormalReduce', `LocalSuperReduce`, A, x, u, phi, delta, omega, opts1 union {'step' = 1});
        tmp := LocalSuperReduce(A, x, u, phi, delta, omega, opts1 union {'step' = 1});
        userinfo(4, 'FormalReduce', `partial rho-polygon`, tmp[2]);
        RETURN(ramified_reduction(tmp[1], x, u, phi, delta, omega, k1, k2, c, r, opts1 union
                             {'invariants' = tmp[3], 'rhopolygon' = tmp[2]}));

    else
        # system is Moser-irreducible
        userinfo(4, 'FormalReduce', `system is Moser-reduced`);
        P := rhopoly[nops(rhopoly)][2];
        userinfo(4, 'FormalReduce', `characteristic polynomial of leading matrix: `, P);
        userinfo(5, 'FormalReduce', `ig=`, q-1,r, igcd(q-1, r));
        if igcd(q-1, r) = 1 then
            # treat non zero roots, if existing and we have picked a "good"
            # slope
            for f in factors(normal(P/u^ldegree(P, u)), ext)[2] do
                # We proceed exactly as in the
                # corresponding case in the formal reduction, but we first have
                # to find the value for the smart ramification c

                 userinfo(4, 'FormalReduce', `irreducible factor is`, f[1],
                         `multiplicity is`, f[2]);
                #if ldegree(f[1], u) < degree(f[1], u) and
                    # treated factor has nonzero roots
                    R := normal(subs(u = u^(1/r), subs(u = r*u, f[1])));
                    userinfo(4, 'FormalReduce', `rational slope k =`,(q-1)/r);
                    igcdex(q-1, r, 'U', 'V');
                    b := RootOf(subs(c = 1, R), u);
                    cc := b^U;
                    userinfo(4, 'FormalReduce', `smart ramification, c =`,cc);
                    R := evala(subs(c = cc, R));
                    userinfo(4, 'FormalReduce', `reduced factor:`, R);
                    #g[1] := subs(u = u/(r*cc), R);
                    g[1] := u - b^V*r;
                    g[2] := f[2];
                    AA := mat_subs(c = cc, A, x, phi, delta, omega);
                    found := found+degree(R, u)*g[2]*r;
                    #print(f[1], g);
                    if degree(g[1], u) = 1 then
                        # factor is linear
                        userinfo(4, 'FormalReduce',
                                 `irreducible factor is linear`);
                        if g[2] > 1 then
                            # multiple root case
                            userinfo(4, 'FormalReduce', `multiple root, m = `,
                                     g[2]);
                            a := RootOf(g[1], u);
                            w := a/x^q;
                            userinfo(4, 'FormalReduce',
                                     `changing exponential part:`, w);
                            B :=  mat_change_exp_part (AA, x, w, phi(w, x), delta1(w,x));
                            userinfo(4, 'FormalReduce', `block-reducing with *!`,
                                     [q], u^g[2]);
                            tmp :=  FormalReduce(
                                mat_block_reduce(B, x, [q], u^g[2], u, phi, delta, omega, 1, 1),
                                x, u, phi, delta, omega, opts1);
                            res := res, op(tmp);
                                     # seq([i[2]], i = tmp);
                        else
                            # single root case
                            userinfo(4, 'FormalReduce', `block-reducing with **!`,
                                     [q], g[1]);
                            B :=  mat_block_reduce (AA, x, [q], g[1], u, phi, delta, omega, 1, 1);
                            res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1));
                        fi
                    else
                        # factor is irreducible polynomial of degree > 1
                        if n > 4 and n-g[2]*degree(g[1], u) > 0 then
                            userinfo(4, 'FormalReduce',
                                     `isolating algebraic extension`);
                            userinfo(4, 'FormalReduce', `block-reducing with !!**`,
                                     [q], g[1]^g[2]);
                            B :=  mat_block_reduce (AA, x, [q], g[1]^g[2], u, phi, delta, omega, 1, 1);
                            res := res, op( FormalReduce (B, x, u, phi, delta, omega, opts1))
                        else
                            userinfo(4, 'FormalReduce',
                                     `constant field extension necessary`);
                            a := RootOf(g[1], u);
                            userinfo(4, 'FormalReduce', `root is`, a,
                                     `multiplicity is:`, g[2]);
                            w := a/x^q;
                            userinfo(4, 'FormalReduce',
                                     `changing exponential part:`, w);
                            B :=  mat_change_exp_part (AA, x, w, phi(w, x), delta(w,x));
                            userinfo(4, 'FormalReduce', `block-reducing with *!!*`,
                                     [q], u^g[2]);
                            tmp :=  FormalReduce(
                                mat_block_reduce(B, x, [q], u^g[2], u, phi, delta, omega, 1, 1),
                                x, u, phi, delta, omega, opts1);
                            res := res, seq([ mat_get_exp_part (i[2], x), i[2]],
                                            i = tmp);
                        fi
                    fi
            od
        else
        	userinfo(4, 'FormalReduce', `ramification is not minimal, no hyper-exponential parts computed`);
        fi;
        print(k1,k2,q,found);
        if degree(P, u) > 0  then
            # we have to apply recursion when nothing has been found so far
            if ldegree(P, u) = degree(P, u) then
                userinfo(4, 'FormalReduce', `nilpotent case`);
                P := rhopoly[1][2];
                userinfo(4, 'FormalReduce', `next k-polynomial:`, P);
                if degree(P, u) > 0 then
                    userinfo(4, 'FormalReduce', `applying recursion`);
                    B :=  mat_block_reduce (A, x, invars, P, u, phi, delta, omega, 1, 1);
                    tmp := ramified_reduction(B, x, u, phi, delta, omega, k1, k2, c, r, opts1);

                    found := found + tmp[2];
                    res := res, op(tmp[1]);
                else
                    userinfo(4, 'FormalReduce', `no decoupling possible`);
                    RETURN([[], 0])
                fi
            elif ldegree(P, u) > 0 then
                userinfo(4, 'FormalReduce', `block-reducing with **!!*`,
                                             [q], u^ldegree(P, u));
                B :=  mat_block_reduce (A, x, [q], u^ldegree(P, u), u, phi, delta, omega, 1, 1);
                tmp := ramified_reduction(B, x, u, phi, delta, omega, k1, k2, c, r, opts1);
                found := found + tmp[2];
                res := res, op(tmp[1])
            fi;
        fi;
        RETURN([[res], found])
    fi
end:
