# Package Definition File for package ISOLDE
# Created: Mon Dec 19 2011

ISOLDE := module()

description "A package for computing symbolic solutions of systems of linear functional equations";

##are there other global data str?
global
    Default_phi, Default_delta, Default_omega,
    Mat_Description, Valuation_Bound;

export #only procedures
    MatTools, LocalReduction, Utilities, #temporary exported

#from the submodule GlobalSuperReduction
   GlobalSuperReduce,

#from the submodule FormalReduction
    FormalReduce,

#from the submodule Utilities
    val, eval_block_reduced,
    randomMatrix,
    matrix_series,

#from the submodule LocalReduction
    LocalSuperReduce,
    LocalMoserReduce,

#from the submodule MatTools
    lc_evala,
    mat_val,
    mat_convert,
    mat_block_reduce,
    mat_valuation,
    mat_eval,
    mat_ramification,
	mat_get_dim,
    mat_get_exp_part,
    mat_get_ext,
    mat_get_inv_transformation,
    mat_get_point,
    mat_get_ramification,
    mat_get_transformation,
	mat_term_transform,
	mat_change_exp_part,
	mat_get_terms,
	mat_pseudo_transform,
    mat_get_order,
	mat_check;

local
    #initialize, #TODO procedure to initialize global structures

    #submodules are all local
    GlobalSuperReduction, FormalReduction,

    G_matrix0,
    G_matrix,
    l_column_rank,
    l_qtcd,
    l_colechelon,

    #Utilities,#module
    pval,
    Id,
    normalm,
    evalam,
    #val,
    sym_power_sys,
    seq_evala,
    #lc_evala,
    lc_series,
	truncate,
	#matrix_series,
	mult,
	my_linsolve,
	val_ldegree,
	generic_val,
	generic_series,
	poly_val,
	poly_pval,
	col_pval,
	#mat_val,
	mat_pval,
    lc_evalam,
	localize,
	transform,
	pseudo_transform,
	colechelon,
	rowechelon,
	complement,
	backsubs,
	sylvester,
	gen_sylvester,
	sim_sylvester,
	pencil_block_reduce,
	pencil_solve,
	tensor_prod,
	randomPoly,
	randomRat,
	#randomMatrix,
	intDiff,
	int_diff_split,
	getOde,
	getLodo,
	getLODO,
	ode2lode,
	isDerivative,
	makeSimple,
	clearDenom,
	normalize,
	Apply,
	toEuler,
	newtonPolygon,
	companion_sys,
	tensor_sys,
	Evalm,
	ram,
	newton_polygon,

    ramified_case,
    ramified_reduction,

    #MatTools, #module
    #mat_block_reduce,
    #mat_change_exp_part,
    #mat_convert,
    mat_direct_block_reduce,
    mat_elementary_transformation,
    #mat_eval,
    #mat_get_dim,
    #mat_get_exp_part,
    #mat_get_ext,
    #mat_get_inv_transformation,
    #mat_get_point,
    #mat_get_ramification,
    #mat_get_transformation,
    #mat_pseudo_transform,
    #mat_ramification,
    mat_scalar_mult,
    mat_swap,
    mat_copy,
    mat_transform,
    #mat_valuation,
    #mat_term_transform,
    new_system,
    new_matrix,
    mat_subs,
    mat_to_simple,
    #eval_block_reduced,
    convert_back,
    simple_eval,
    #mat_get_terms,
    #mat_get_order,
    mat_get_indicial_polynomial,
    mat_colvaluation,
    mat_lead_mon,
    mat_lc,
    mat_coeff,
    #mat_check,
    mat_sum,
    mat_difference,
    mat_product;

option
	package; #load = initialize;

Utilities := module()
export
    pval,
    Id,
    normalm,
    evalam,
    val,
    sym_power_sys,
    seq_evala,
    lc_evala,
    lc_series,
	truncate,
	matrix_series,
	mult,
	my_linsolve,
	val_ldegree,
	generic_val,
	generic_series,
	poly_val,
	poly_pval,
	col_pval,
	mat_val,
	mat_pval,
    lc_evalam,
	localize,
	transform,
	pseudo_transform,
	colechelon,
	rowechelon,
	complement,
	backsubs,
	sylvester,
	gen_sylvester,
	sim_sylvester,
	pencil_block_reduce,
	pencil_solve,
	tensor_prod,
	randomPoly,
	randomRat,
	randomMatrix,
	intDiff,
	int_diff_split,
	getOde,
	getLodo,
	getLODO,
	ode2lode,
	isDerivative,
	makeSimple,
	clearDenom,
	normalize,
	Apply,
	toEuler,
	newtonPolygon,
	companion_sys,
	tensor_sys,
	Evalm,
	ram,
	newton_polygon;

$include <Utilities.mpl>;
end module:

# functions from Utilities
pval := Utilities:-pval;
Id := Utilities:-Id;
normalm := Utilities:-normalm;
evalam := Utilities:-evalam;
lc_evala:=Utilities:-lc_evala;
val:=Utilities:-val;
sym_power_sys := Utilities:- sym_power_sys;
seq_evala := Utilities:- seq_evala;
lc_evala := Utilities:- lc_evala;
lc_series := Utilities:- lc_series;
truncate := Utilities:-truncate;
matrix_series := Utilities:-matrix_series;
mult := Utilities:- mult;
my_linsolve := Utilities:- my_linsolve;
val_ldegree := Utilities:- val_ldegree;
generic_val := Utilities:- generic_val;
generic_series := Utilities:- generic_series;
poly_val := Utilities:- poly_val;
poly_pval := Utilities:- poly_pval;
col_pval := Utilities:- col_pval;
mat_val := Utilities:- mat_val;
mat_pval := Utilities:- mat_pval;
lc_evalam := Utilities:-lc_evalam;
localize := Utilities:- localize;
transform := Utilities:- transform;
pseudo_transform := Utilities:- pseudo_transform;
colechelon := Utilities:- colechelon;
rowechelon := Utilities:- rowechelon;
complement := Utilities:- complement;
backsubs := Utilities:- backsubs;
sylvester := Utilities:- sylvester;
gen_sylvester := Utilities:- gen_sylvester;
sim_sylvester := Utilities:- sim_sylvester;
pencil_block_reduce := Utilities:- pencil_block_reduce;
pencil_solve := Utilities:- pencil_solve;
tensor_prod := Utilities:- tensor_prod;
randomPoly := Utilities:- randomPoly;
randomRat := Utilities:- randomRat;
randomMatrix := Utilities:- randomMatrix;
intDiff := Utilities:- intDiff;
int_diff_split := Utilities:- int_diff_split;
getOde := Utilities:- getOde;
getLodo := Utilities:- getLodo;
getLODO := Utilities:- getLODO;
ode2lode := Utilities:- ode2lode;
isDerivative := Utilities:- isDerivative;
makeSimple := Utilities:- makeSimple;
clearDenom := Utilities:- clearDenom;
normalize := Utilities:- normalize;
Apply := Utilities:- Apply;
toEuler := Utilities:- toEuler;
newtonPolygon := Utilities:- newtonPolygon;
companion_sys := Utilities:- companion_sys;
tensor_sys := Utilities:- tensor_sys;
Evalm := Utilities:- Evalm;
ram := Utilities:- ram;
newton_polygon := Utilities:- newton_polygon;

GlobalSuperReduction := module()
	export
		GlobalSuperReduce;
	local
		g_sort_cols,
		g_column_rank,
		g_qtcd,
		pgauss,
		prank,
		pcolechelon;


$include <GlobalSuperReduction.mpl>;
end module:

GlobalSuperReduce := GlobalSuperReduction:-GlobalSuperReduce;


MatTools := module()

export
		mat_block_reduce,
		mat_change_exp_part,
		mat_convert,
		mat_direct_block_reduce,
		mat_elementary_transformation,
		mat_eval,
		mat_get_dim,
		mat_get_exp_part,
		mat_get_ext,
		mat_get_inv_transformation,
		mat_get_point,
		mat_get_ramification,
		mat_get_transformation,
		mat_pseudo_transform,
		mat_ramification,
		mat_scalar_mult,
		mat_swap,
		mat_transform,
		mat_valuation,
        mat_copy,
		mat_term_transform,
        new_system,
        new_matrix,
        mat_subs,
        mat_to_simple,
        eval_block_reduced,
        convert_back,
        simple_eval,
        mat_get_terms,
        mat_get_order,
        mat_get_indicial_polynomial,
        mat_colvaluation,
        mat_lead_mon,
        mat_lc,
        mat_coeff,
        mat_check,
        mat_sum,
        mat_difference,
        mat_product;


$include <MatTools.mpl>; #label
end module:

# functions from MatTools
mat_block_reduce:=MatTools:-mat_block_reduce;
mat_change_exp_part:=MatTools:-mat_change_exp_part;
mat_convert:=MatTools:-mat_convert;
mat_direct_block_reduce:=MatTools:-mat_direct_block_reduce;
mat_elementary_transformation:=MatTools:-mat_elementary_transformation;
mat_eval:=MatTools:-mat_eval;
mat_get_dim:=MatTools:-mat_get_dim;
mat_get_exp_part:=MatTools:-mat_get_exp_part;
mat_get_ext:=MatTools:-mat_get_ext;
mat_get_inv_transformation:=MatTools:-mat_get_inv_transformation;
mat_get_point:=MatTools:-mat_get_point;
mat_get_ramification:=MatTools:-mat_get_ramification;
mat_get_transformation:=MatTools:-mat_get_transformation;
mat_pseudo_transform:=MatTools:-mat_pseudo_transform;
mat_ramification:=MatTools:-mat_ramification;
mat_scalar_mult:=MatTools:-mat_scalar_mult;
mat_swap:=MatTools:-mat_swap;
mat_transform:=MatTools:-mat_transform;
mat_valuation:=MatTools:-mat_valuation;
mat_term_transform:=MatTools:-mat_term_transform;
mat_copy:=MatTools:-mat_copy;
new_system := MatTools:- new_system;
new_matrix:=MatTools:- new_matrix;
mat_subs:=MatTools:-mat_subs;
mat_to_simple:=MatTools:-mat_to_simple;
eval_block_reduced:=MatTools:-eval_block_reduced;
convert_back:=MatTools:-convert_back;
simple_eval:=MatTools:-simple_eval;
mat_get_terms:=MatTools:-mat_get_terms;
mat_get_order:=MatTools:-mat_get_order;
mat_get_indicial_polynomial:=MatTools:-mat_get_indicial_polynomial;
mat_colvaluation:=MatTools:-mat_colvaluation;
mat_lead_mon:=MatTools:-mat_lead_mon;
mat_lc:=MatTools:-mat_lc;
mat_coeff:=MatTools:-mat_coeff;
mat_check:=MatTools:-mat_check;
mat_sum:=MatTools:-mat_sum;
mat_difference:=MatTools:-mat_difference;
mat_product:=MatTools:-mat_product;


LocalReduction := module()
export
	    LocalSuperReduce,
        LocalMoserReduce,
        G_matrix0,
        G_matrix,
        l_column_rank,
        l_qtcd,
        l_colechelon;


$include <LocalSuperReduction.mpl>;
end module:

# functions from LocalSuperReduce
LocalSuperReduce:=LocalReduction:-LocalSuperReduce;
LocalMoserReduce:=LocalReduction:-LocalMoserReduce;
G_matrix0:=LocalReduction:-G_matrix0;
G_matrix:=LocalReduction:-G_matrix;
l_column_rank:=LocalReduction:-l_column_rank;
l_qtcd:=LocalReduction:-l_qtcd;
l_colechelon:=LocalReduction:-l_colechelon;


FormalReduction := module()
export
	  FormalReduce,
      ramified_case,
      ramified_reduction;

$include <FormalReduction.mpl>;

end module:

# functions from FormalReduce
FormalReduce:=FormalReduction:-FormalReduce;
ramified_case:=FormalReduction:-ramified_case;
ramified_reduction:=FormalReduction:-ramified_reduction;

#default values for phi, delta, omega from the differential case
Default_phi := proc(f, x) f end proc;
Default_delta := proc(f, x) diff(f, x) end proc;
Default_omega := -1;

end module:

savelibname := ".": libname := savelibname, libname:
savelib('ISOLDE',"ISOLDE.mla");
