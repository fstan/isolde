# ************************************************************************* #
#                                                                           #
# local super-reduction algorithm for systems with power series coefficients#
#                                                                           #
#                                                                           #
#             Last update:  $Date: 2010/10/25 08:51:50 $                    #
#                                                                           #
# ************************************************************************* #


# LocalSuperReduce(B, x, lambda, phi, delta, omega, c, q, opts)
#   INPUT:  B      - a key of a representation of a local matrix series B
#           x      - a name
#           lambda - a name
#	    phi	   - an automorphism
#	    delta  - a pseudo-derivative
#	    omega  - an integer
#	    c	   - an integer
#	    q	   - a parameter
#           opts   - (optional) a set of options
#   OUTPUT: The list of a super-irreducible form of A and the rho-polygon of
#           the computed super-irreducible form.
#
#           If the option 'invars' = [q,n0,..,nk] where q and the ni are
#           integers is given as argument, the matrix A is assumed to be
#           (k+1)-irreducible. The value of q is used as pole order and the
#           ni for the first k+1 invariants. In this case, the returned
#           rho-polygon contains only the new computed Newton polynomials.
#
#           The option 'step' = k2 where k2 is a positive integer computes
#           instead of a super-irreducible form a k2-irreducible form.
#
LocalSuperReduce := proc(B, x, lambda, phi1, delta1, omega1, opts2)
    local  t0, i, s, T, invT, k, k2, n, mk, K, opts1,
           invars, A, G, theta, theta0, rhopoly, ext, ind, phi, delta, omega, opts, c, q;
    global Valuation_Bound, Default_delta, Default_phi, Default_omega;

    if nargs < 6 then
        phi := Default_phi;
        delta := Default_delta;
        omega := Default_omega;
    else
        phi := args[4];
        delta := args[5];
        omega := args[6];
    fi;

    if not type(Valuation_Bound, 'integer')
    then
        Valuation_Bound := 200
    fi;

    t0 := time();

    if nargs = 7 then
    	opts := args[7];
    else
    	opts := {};
    fi;

    k2 := infinity;

    A := mat_copy(B);
    n := mat_get_dim(A);
    #M := copy(A[1]);
    T := Id(n);
    invT := Id(n);
    k := 1;
    s := infinity;
    invars := [];
    if nargs = 7 then
        if has(opts, 'invariants') then
            invars := rhs(op(select(has, opts, 'invariants')));
            if invars <> [] then
                #s := invars[1];
                s := - mat_valuation(A, x); # better: use invars
                k := nops(invars);
            fi
        fi;
        if has(opts, 'step') then
            k2 := rhs(op(select(has, opts, 'step')));
        fi;
        opts1 := opts
    else
        opts1 := {}
    fi;
    t0:=time();
    s := - mat_valuation(A, x);
    # mat_eval(A, x, -s, -1);
    ext :=  mat_get_ext(A);

    userinfo(1, 'super_reduce', `reduction step = `, k);
    rhopoly := NULL;
    mk := 0;
    theta := 0;
    if invars = [] then s := infinity fi;
    while (k <= min(s+omega, k2)) and (mk < n) do
        while (theta = 0) and (s > -omega) do
            if k = 1 then
                # Moser reduction
                if invars = [] then
                    s := -mat_valuation(A, x);
                    userinfo(4, 'super_reduce', `valuation is`, -s);
                fi;
                if s <= -omega then break fi;
                G := G_matrix0(A, x, k, [s]);#, phi, delta);
                l_colechelon(G, ext, 'T', 'ind');
                mk := nops(ind);
                invT := map(evala, evalm(1/T));
                userinfo(1, 'super_reduce', `Moser reduction: r = `, mk);
                A := mat_pseudo_transform(A, x, T, invT, 0, phi, delta, omega);
                s := -mat_valuation(A, x);
                G := G_matrix(A, x, k, lambda, [s]);#, phi, delta);
                # compute criterion for k-irreducibility
                theta := evala(primpart(linalg['det'](G), lambda));
                #theta := evala(Normal(
                #              theta/lambda^ldegree(theta, lambda)));
                userinfo(5, 'super_reduce', `theta:`, theta);
                if theta <> 0 then break fi;
                A := l_qtcd(A, x, k, mk, [s], lambda, G, ext, phi, delta, omega);
            else
                # k-reduction
                G := G_matrix0(A, x, k, invars);#, phi, delta);
                K := linalg['kernel'](G);
                if K <> {} then
                    userinfo(4, 'super_reduce',
                             `columns are linearly dependent`);
                    userinfo(5, 'super_reduce', `kernel dim = `, nops(K));
                    A := l_column_rank(A, x, k, invars, K, 'mk', ext, phi, delta, omega, opts1);

                else
                    mk := n;
                    userinfo(4, 'super_reduce',
                             `columns are linearly independent`);
                fi;
                G := G_matrix(A, x, k, lambda, invars);#, phi, delta);

                # compute criterion for k-irreducibility
                theta := evala(Normal(primpart(linalg['det'](G), lambda)));
                #theta := evala(Normal(
                #              theta/lambda^ldegree(theta, lambda)));
                userinfo(5, 'super_reduce', `theta:`, theta);
                if theta <> 0 then break fi;
                A := l_qtcd(A, x, k, mk, invars, lambda, G, ext, phi, delta, omega);
            fi;
        od;
        # matrix is now k-irreducible
#Flavia #if k < s+omega then # to verify EP
	#            theta := normal(theta/lambda^ldegree(theta, lambda))
        #fi;
        if k = 1 then
            # Moser irreducible
            if s <= -omega then
                # compute the characteristic polynomial of the residue matrix
                #theta0 := evala(primpart(linalg['charpoly'](
                #                 c*(mat_coeff(A, x, omega)), lambda), q*lambda));
                ##print("mat_coeff=",mat_coeff(A, x, omega));
                if q <> 1 then
                	#theta0:=linalg['det']((mat_coeff(A, x, omega)*q^(lambda))-(c*((1-q^lambda)/(1-q))*linalg['diag'](seq(1,i=1..n))));
                	theta0:=linalg['det']((mat_coeff(A, x, omega))-lambda*(linalg['diag'](seq(1,i=1..n))));
                	theta0:=normal(theta0);
                	#theta0:=evala(primpart(linalg['charpoly'](mat_coeff(A, x, omega), lambda)));
                else
                	theta0:=linalg['det']((mat_coeff(A, x, omega))-lambda*(linalg['diag'](seq(1,i=1..n))));
                	theta0:=normal(theta0);
                fi;
                userinfo(1, 'super_reduce', `system is regular singular`);
                rhopoly := [0, theta0];
                userinfo(1, 'super_reduce', `slope:`, 0,
                            `indicial equation:`, theta0);
                invars := [-omega];
                #break
            else
                # compute the characteristic polynomial of the leading matrix
                #theta0 := evala(primpart(linalg['charpoly'](
                #                c*(mat_coeff(A, x, -s)), lambda), q*lambda));
                if q <> 1 then
                	theta0:=linalg['det']((mat_coeff(A, x, -s))-lambda*(linalg['diag'](seq(1,i=1..n))));
                	theta0:=normal(theta0);
                	#theta0:=evala(primpart(linalg['charpoly'](mat_coeff(A, x, -s), lambda)));
                else
                	theta0:=linalg['det']((mat_coeff(A, x, -s+omega))-lambda*(linalg['diag'](seq(1,i=1..n))));
                	theta0:=normal(theta0);
                fi;
                #theta := evala(Normal(
                #              theta/lambda^ldegree(theta, lambda)));
                rhopoly := [s+omega-1, evala(primpart(subs(lambda = -lambda, theta),
                                                lambda))],
                           [s+omega, evala(primpart(theta0, lambda))];
                userinfo(1, 'super_reduce', `slope:`, s+omega,
                                        `invariant:`, op(2, [rhopoly])[2]);
                userinfo(1, 'super_reduce', `slope `, s+omega-1,
                                        `invariant:`, op(1, [rhopoly])[2]);
                invars := [s, mk];

            fi;
        else
            #theta := normal(theta/lamdba^(ldegree(theta,lambda)));
            rhopoly := [s-k+omega, evala(primpart(subs(lambda = -lambda, theta),
                                              lambda))], rhopoly;
            userinfo(1, 'super_reduce', `slope `, s-k+omega,
                     `invariant:`, op(1, [rhopoly])[2]);
            invars := [op(invars),
                       mk-convert([seq(invars[i], i=2..nops(invars))], `+`)];
        fi;
        if s > -omega then
            #if has(opts1, 'truncate') then
            #    userinfo(1, 'super_reduce', `truncating system`);
            #    A[1] := subs(x=x-A[2], readlib('mat_eval')(A, x, -s, -s+n*s))
            #fi;
            k:= k+1;
            theta := 0;
            userinfo(1, 'super_reduce', `increase reduction step to: `, k)
        fi
    od;
    if mk = n then
        for i from k to s+omega do
            userinfo(1, 'super_reduce', `increase reduction step to: `, i);
            rhopoly := [s-i+omega, 1], rhopoly;
            userinfo(1, 'super_reduce', `slope:`, s-i+omega,
                     `invariant:`, op(1, [rhopoly])[2]);
        od
    fi;

    #if q <> 1 then
    #	rhopoly := simplify(subs(q^lambda = lambda, rhopoly));
    #fi;

    userinfo(1, 'super_reduce', `time: `,time()-t0);
    if s < -omega then
        userinfo(1, 'super_reduce', `system is non-singular`);
        RETURN([A, [[0, lambda^n]], [0]]);
    fi;

    userinfo(5, 'super_reduce', `rho-polygon:`, [rhopoly]);
    [A, [rhopoly], invars]
end:

# LocalMoserReduce(B, x, lambda, opts)
#   INPUT:  B      - a key of a representation of a local matrix series
#           x      - a name
#           lambda - a name
#           opts   - (optional) a set of options
#   OUTPUT: The list of a Moser-irreducible form of A and a part of
#           rho-polygon of the computed Moser-irreducible form.
#
LocalMoserReduce := proc(B, x, lambda, phi1, delta1, omega1, opts1)
    local opts, phi, delta, omega;

    if nargs < 6 then
        phi := Default_phi;
        delta := Default_delta;
        omega := Default_omega;
    else
        phi := phi1;
        delta := delta1;
        omega := omega1;
    fi;

    ##TODO rethink this nargs argument-> at the moment we have opts only
    # if we specify phi, delta, omega
    if nargs >= 7 then opts := opts1 else opts := {} fi;

    LocalSuperReduce(B, x, lambda, phi, delta, omega, opts union {'step' = 1})
end:

# G_matrix0(K, x, k, invars)
#   INPUT:  K      - a key of a matrix series in internal representation
#           x      - a name
#           k      - a positive integer (the reduction step)
#           invars - list of integers
#   OUTPUT: The matrix G_k(0)
#
G_matrix0 := proc(K, x, k, invars)
    local s, nis, n, G, i, j, jj, h, M;
    global Index;


    s := invars[1];
    nis := subsop(1 = NULL, invars);
    n :=  mat_get_dim(K);
    G := linalg['matrix'](n, n, 0);
    M :=  mat_eval(K, x, -s, -s+k-1);
    # already minimal number of columns
    jj := 1;
    for h from 1 to k-1 do
        for j from jj to jj+nis[h]-1 do
            for i from 1 to n do
                G[i, j] := coeff(M[i, j], x, -s+h-1);
            od;
        od;
        jj := j;
    od;

    # remaining columns
    for j from jj to n do
        for i from 1 to n do
            G[i, j] := coeff(M[i, j], x, -s+k-1);
        od;
    od;
    h := [seq(seq(-s+j-1, i = 1..nis[j]), j = 1..k-1)];
    userinfo(1, 'super_reduce', `valuation:`, -s);
    userinfo(1, 'super_reduce', `reduced column valuations:`, h);
    evalm(G);
end:

# G_matrix(K, x, k, lambda, invars)
#   INPUT:  K      - a key of a matrix series in internal representation
#           x      - a name
#           k      - a positive integer (the reduction step)
#           lambda - a name
#           invars - list of integers
#   OUTPUT: the matrix G_k(lambda)
#
G_matrix := proc(K, x, k, lambda, invars)
    local s, nis, n, G, cc, z, zcols, i, j, jj, h, M;
    global Index;

    s := invars[1];
    nis := subsop(1 = NULL, invars);
    n := mat_get_dim(K);
    G := linalg['matrix'](n, n, 0);
#	if K='A1' then
#		error "stoping now";
#	fi;
    M := mat_eval(K, x, -s, -s+k);

    # already minimal number of columns
    jj := 1;
    for h from 1 to k-1 do
        for j from jj to jj+nis[h]-1 do
            for i from 1 to n do
                G[i, j] := coeff(M[i, j], x, -s+h-1);
            od;
        od;
        jj := j;
    od;
    zcols := NULL;

    # resting columns without lambda
    for j from jj to n do
        z := true;
        for i from 1 to n do
            cc := coeff(M[i, j], x, -s+k-1);
            if cc <> 0 then
                z := false;
                G[i, j] := cc
            fi;
        od;
        if z then zcols := zcols, j fi;
    od;

    # resting columns with lambda
    for j in [zcols] do
        for i from 1 to n do
            G[i, j] := coeff(M[i, j], x, -s+k);
        od;
        G[j, j] := G[j, j] + lambda
    od;
    h := [seq(seq(-s+j-1, i = 1..nis[j]), j = 1..k-1)];
    #userinfo(1, 'super_reduce', `pole order:`, s);
    userinfo(1, 'super_reduce', `valuations:`, h);
    evalm(G)
end:

# l_column_rank(A, x, k, invars, K, mk_new, ext, phi, delta, omega)
#   INPUT:  A      - a key of a matrix series in internal representation
#           x      - a name  of the sausage
#           k      - a positive integer (the reduction step)
#           invars - a list of integers
#           K      - a list of vectors
#           mk_new - a name
#           ext    - an algebraic extension as a set of RootOfs
#	    phi	   - an automorphism
#	    delta  - a pseudo-derivative
#           T      - a matrix
#           invT   - a matrix
#   OUTPUT: A matrix M' with mu_(k)(M') < mu_(k)(M)
#
l_column_rank := proc(A, x, k, invars, K, mk_new, ext, phi, delta, omega)
    local nis, K1, n, P, SS, TT, invTT, i, j, mk, t, ind;

    nis := subsop(1 = NULL, invars);
    n :=  mat_get_dim(A);
    userinfo(2, 'super_reduce', `reducing column rank`);
    K1 := map(convert, K, 'list');
    P := l_colechelon(linalg['transpose'](convert([op(K1)], 'matrix')),
                      ext, 'SS', 'ind');
    mk := n-nops(ind);
    t := 1;
    for i from 1 to k-1 do
        for j from t to t+nis[i]-1 do
            P := linalg['mulrow'](P, j, x^(k-i))
        od;
        t := j;
    od;

    # generate transformation which reduces row rank
    TT := linalg[matrix](n, n, 0);
    j := 1;
    for i from 1 to n do
        if ind <> [] and i = ind[1] then
            ind := subsop(1 = NULL, ind);
        else
            TT[i, j] := 1;
            j := j+1;
        fi
    od;
    for i from 1 to n do
        for j from 1 to n-mk do
            TT[i, j+mk] := P[i, n-mk+1-j]
        od
    od;
    invTT := linalg['inverse'](TT);
    mk_new := mk;
     mat_pseudo_transform(A, x, TT, invTT, 0, phi, delta, omega);
end:

# l_qtcd(A, x, k, mk, invars, lambda, G, ext, phi, delta, omega)
#   INPUT:  A    - a key of a matrix series in internal representation
#           x    - a name
#           k    - a positive integer (the reduction step)
#           ntab - an array of integers
#           mk   - an integer
#           invars - a list of integers
#           lambda - a name
#           G    - a matrix (the G-matrix of M)
#	    phi  - an automorphism
#	    delta - a pseudo-derivative
#   OUTPUT: A matrix M' whose G-matrix G' has reduced row rank
#
l_qtcd := proc(B, x, k, mk, invars, lambda, G, ext, phi, delta, omega)
    local n, GG, q, mk1, Gq, r, c, i, j, K, P, D, invD, U, invU, ind, A;


    A := mat_copy(B);

    userinfo(2, 'super_reduce', `enter qtcd-algorithm`);
    n :=  mat_get_dim(A);
    q := 0;
    mk1 := copy(mk);
    do
        GG := subs(lambda = 0, Evalm(G, x));
        userinfo(5, 'super_reduce', `q =`, q);
        Gq := linalg['submatrix'](GG, 1..n-q, 1..n-q);
        if ext = {} then
            r := linalg[rank](linalg['submatrix'](Gq, 1..mk1, 1..n-q))
        else
            l_colechelon(linalg[transpose](
                linalg['submatrix'](Gq, 1..mk1, 1..n-q)), ext, 'dummy', 'ind');
            r := nops(ind)
        fi;
        if (r < mk1) then
            userinfo(5, 'super_reduce', `rows are linearly dependent`);
            break
        fi;
        K := map(convert, linalg[kernel](linalg['transpose'](Gq)), 'list');
        P := l_colechelon(linalg[transpose](convert([op(K)], 'matrix')), ext,
                                          'dummy', 'ind');
        P := linalg[transpose](P);
        c := linalg['rowdim'](P);
        userinfo(5, 'super_reduce', `kernel dim = `, c);
        for i from c to 1 by -1 do
            # eventual permutation on P, M, T and invT
            if P[i, n-q-c+i] = 0 then
                j := n-q-c+i;
                while P[i, j] = 0 do j := j-1 od;
                userinfo(5, 'super_reduce', `permute rows`, n-q-c+i, j);
                P := linalg['swapcol'](P, n-q-c+i, j);
                A:=mat_swap(A, x, n-q-c+i, j);
            fi
        od;
        invU := Id(n);
        invU := linalg['copyinto'](P, invU, n-q-c+1, 1);
        U := linalg['inverse'](invU);
        A :=  mat_pseudo_transform(A, x, U, invU, 0, phi, delta, omega);
        if q = n-mk1 then break fi;
        #print("n=",n,"mk1=",mk1);
        userinfo(3, 'super_reduce', `recomputing G-matrix`);
        G := G_matrix(A, x, k, lambda, invars);#, phi, delta);

        #print("G=", G);
        q := q+c
    od;
    GG := subs(lambda = 0, Evalm(G, x));
    userinfo(5, 'super_reduce', `exit qtcd with q = `, q);
    userinfo(3, 'super_reduce', `applying diagonal transformation:`,
             seq(x, i=1..mk), seq(1, i=1..n-mk-q), seq(x, i=1..q));
    D := linalg['diag'](seq(x, i=1..mk), seq(1, i=1..n-mk-q), seq(x, i=1..q));
    ##print("Diagaonal trans:",eval(D));
    invD := linalg['diag'](seq(1/x, i=1..mk), seq(1, i=1..n-mk-q),
                           seq(1/x, i=1..q));
    #MMM:=A;if invars[1]=4 then ERROR(`s`) fi;
     mat_pseudo_transform(A, x, D, invD, 1, phi, delta, omega);
end:

# l_colechelon(A, ext, T [, ind])
#    INPUT:  A   - a matrix over the field Q(ext)
#            ext - a set of RootOfs describing an algebraic extension
#            T   - a name
#            ind - (optional) a name
#    OUTPUT: the column echelon form of A. Computations are done in the
#            algebraic extension given by ext. T is assigned to the
#            transformation matrix. If ind is passed as argument, it is
#            assigned to the row numbers of the "steps" of the echelon form.
#
l_colechelon := proc(A, ext, T, ind)
    local B, ind0, r, c, b, i, ii, j, jj, h;

    c := linalg[coldim](A);
    r := linalg[rowdim](A);
    B := copy(A);
    T := Id(c);
    ind0 := [];
    j := 1;
    if ext = {} then
        # faster version if no algebraic extension
        for i from r by -1 to 1 while j<=c do
            jj := j;
            while jj <= c and B[i, jj] = 0 do jj := jj+1 od;
            if jj = c+1 then next fi;
            ind0 := [i, op(ind0)];
            if jj > j then
                B := linalg['swapcol'](B, jj, j);
                T := linalg['swapcol'](T, jj, j)
            fi;
            for h from jj+1 to c do
                b := B[i, h]/B[i, j];
                for ii to i do
                    B[ii, h] := normal(B[ii, h]-b*B[ii, j])
                od;
                for ii to c do
                    T[ii, h] := normal(T[ii, h]-b*T[ii, j])
                od
            od;
            j := j+1
        od;
    else
        # version with evala
        for i from r by -1 to 1 while j<=c do
            jj := j;
            while jj <= c and B[i, jj] = 0 do jj := jj+1 od;
            if jj = c+1 then next fi;
            ind0 := [i, op(ind0)];
            if jj > j then
                B := linalg['swapcol'](B, jj, j);
                T := linalg['swapcol'](T, jj, j)
            fi;
            for h from jj+1 to c do
                b := evala(Normal(B[i, h]/B[i, j]));
                for ii to i do
                    B[ii, h] := evala(Normal(B[ii, h]-b*B[ii, j]))
                od;
                for ii to c do
                    T[ii, h] := evala(Normal(T[ii, h]-b*T[ii, j]))
                od
            od;
            j := j+1
        od
    fi;
    if nargs = 4 then ind := ind0 fi;
    op(B)
end:
