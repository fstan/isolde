SHELL=/bin/bash
ifeq (X$(MAPLEAPP), X)
export MAPLEAPP=maple
endif

MLA =ISOLDE.mla


# Delete the default suffixes, then set up our own
.SUFFIXES: 
.SUFFIXES: .mpl .date .tst .out .ok

.PHONY: all clean test

all: $(MLA)

clean:
	rm -f  tests/*.out tests/*.ok tests/failed_tests.txt

### MLA
### here we need to move the commands from the end of ISOLDE.mpl
##savelibname := ".": libname := savelibname, libname:
##savelib('ISOLDE',"ISOLDE.mla");

### Tests
TESTS = $(wildcard tests/*.tst)

### later create a src directory with all the code files
### and put the lib file at top level
%.out: %.tst  $(MLA)
	ulimit -v 1048576 && \
	    ${MAPLEAPP} -s -t -q -e2 -A2 -b . -B < $< > $@

%.out.ok: %.out
	grep -q -e "TEST FAILED" $< || touch $@

test: $(TESTS:.tst=.out.ok) $(TESTS:.tst=.out)
	for n in $(TESTS:.tst=.out.ok) ; \
	    do \
	    	[ -e $$n ] || echo $${n/out.ok/tst} ; \
	    done > tests/failed_tests.txt

###runs tests in parallel
testing:
	$(MAKE) -j 4 test
